<script id="o-grid" type="template">
    {{set "header" true}}
    <div class="row-fluid">
        {{#if header}}
    	<div class="span12 pillbar">
    	<div class="row-fluid">
        <div class="span7">
            {{#if filters}}
    	      <ul class="nav nav-pills">
                  {{#each filters}}
    		      <li class="filter {{#if active}}active{{/if}}" data-event="filter" data-filter="{{name}}"><a href="#">{{#if label}}{{label}}{{else}}{{camel name}}{{/if}}</a></li>
                  {{/each}}
    	      </ul>
            {{/if}}
        </div>
        <div class="span5">
            {{#each buttons}}
            {{set "event" "select"}}
    	  	<a class="btn pull-right" data-event="{{event}}" data-name="{{name}}" href="{{url}}">{{#if label}}{{label}}{{else}}{{camel name}}{{/if}}</a>
            {{/each}}
        </div>
        </div>
        </div>
        {{/if}}
    </div><!--end.row-fluid -->
    	<ul class="thumbnails" location="grid">

    	</ul>


    <div class="pagination">
    </div>

    <span _template_="_template_" data-selector-format="location" data-selector-location="grid"></span>
</script>
<script id="o-grid-cell" type="template">

      <a class="thumbnail" href="#">
        {{{name}}}
      </a>
</script>
<script id="o-grid-service" type="template">
        <div class="thumbnail">

        {{{name}}} - {{camel category}} - {{id}}
        </div>
</script>

<script id="o-grid-apps" type="template">
  <a href="#" class="app-card-link">
  <div class="app-card-contain">
  <div class="app-card">
    <img src="/img/app_large.png" class="app-icon"/>
    <h5 class="app-title">{{{name}}}</h5>
    
   
    <!--<i class="icon-star{{#ifNot favorite}}-empty{{/ifNot}}"></i>-->

      
  </div>
  </div>
  </a>
</script>
