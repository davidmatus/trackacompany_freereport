<script type="template" id="o-register">
    <div class="container register">
      <div class="row-fluid">
        <div location="core" class="span8 offset2">
          <h2>Register</h2>
          <div>
            <button class="social facebook">facebook connect</button>
            <button class="social github">github connect</button>
            <button class="social twitter">twitter connect</button>
          </div>

          <fieldset data-view-cid="view39" data-view-name="core_main" class="view row">

            <div class="span4 names">
              <span location="first_name">
                <div data-view-cid="view41" data-view-name="first_name" class="view">
                  <input type="text" placeholder="First Name *" value="" class="text " id="first_name">
                </div>
              </span>
              <span location="last_name">
                <div data-view-cid="view43" data-view-name="last_name" class="view">
                  <input type="text" placeholder="Last Name *" value="" class="text " id="last_name">
                </div>
              </span>
            </div>

            <div class="span4">
              <span location="email">
                <div data-view-cid="view45" data-view-name="email" class="view">
                  <input type="text" placeholder="Email *" value="" class="text " id="email">
                </div>
              </span>
            </div>

            <div class="span4">
              <span location="password">
                <div data-view-cid="view47" data-view-name="password" class="view">
                  <input type="password" placeholder="Password *" value="" class="password " id="password">
                </div>
              </span>
            </div>


            <div class="span4">
              <span location="password_confirm">
                <div data-view-cid="view49" data-view-name="password_confirm" class="view">
                 <input type="password" placeholder="Repeat Password *" value="" class="password " id="password_confirm">
                </div>
              </span>
            </div>


            <div class="">
              <div class="span8 submit">
                <button class="" type="submit">Sign up</button>
              </div>
            </div>

          </fieldset>
        </div>
      </div>
    </div>
    <span data-selector-format="name" _template_="_template_"></span>
</script>
<script type="template" id="o-registration-divider">
  <div class="or">
    <span>{{#if text}}{{{text}}}{{else}}or{{/if}}</span>
  </div>
</script>
<script type="template" id="o-registration-fieldset">
    <div class="row-fluid">
        <div class="span12">
          <span location="account_name">
          </span>
        </div>

    </div>
    <div class="row-fluid">
        <div class="span6 name">
          <span location="first_name">

          </span>
          <span location="last_name">

          </span>
        </div>

        <div class="span6">
          <span location="email">

          </span>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">
          <span location="password">

          </span>
        </div>

        <div class="span6">
          <span location="password_confirm">

          </span>
        </div>
    </div>



    <span data-selector-format="name" _template_="_template_"></span>
</script>
<script type="template" id="o-registration-logins">

    {{#each logins}}

            {{#set "colIndex" true}}
               {{getCol cols="4" order="ltr"}}
            {{/set}}

            {{#ifEq colIndex "0"}}
            <div class="row-fluid">
            {{/ifEq}}
            <div class="span3">
                <a href="{{url}}" class="login btn">{{{label}}}</a>
            </div>
            {{#ifEq colIndex 2}}
            </div>
            {{/ifEq}}

    {{/each}}
</script>