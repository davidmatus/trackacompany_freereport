<script id="o-empty" type="template">
</script>
<script id="o-iframe" type="template">
    {{set "height" "400px"}}
    <iframe src="{{url}}" width="100%" height="{{height}}"></iframe>
</script>
<script id="o-raw" type="template">
    {{{html}}}
</script>
<script id="o-table-raw" type="template">
    {{{value}}}
</script>

<script id="o-table-numbered" type="template">
    {{_tableRowNum}}
</script>
<script id="o-progressBar" type="template">
    {{#if size}}
    <div class="row-fluid">
    {{#if label}}
    <label  class="form-label span{{math 12 "-" size}}">
        {{{label}}}
    </label>
    {{/if}}
    <div class="span{{size}}">
    {{/if}}
    {{#ifAny msg showPercent}}
        <div class="row-fluid">
        <div class="span10">
            {{{msg}}}
        </div>
        <div class="span2">
            {{#if showPercent}}<span class="pull-right">{{percent}}%</span>{{/if}}
        </div>
        </div>
    {{/ifAny}}
    <div class="progress {{#if level}}progress-{{level}}{{/if}} {{#ifAny stripe striped}}progress-striped{{/ifAny}} {{#if active}}active{{/if}}">
        <div style="width: {{percent}}%;" class="bar"></div>
    </div>
    {{#if size}}
    </div>
    </div>
    {{/if}}
</script>
<script id="o-buttonGrid" type="template">
    {{set "size" "12"}}
    <div class="row-fluid">
    <div class="span{{size}}">
    <div class="centerContent">
        <ul class="bigBtnIcon" style="text-align:left;">
            {{#each buttons}}
            <li>
                <a class="tipB" href="{{url}}" oldtitle="{{desc}}" title="{{desc}}" style="width: {{../boxWidth}}px; height: {{../boxHeight}}px;" {{data-fields data}} >
                    {{#ifAny icon img}}<span class="icon icon-{{icon}}" style="font-size: {{../iconSize}}px;">{{#if img}}<img src="{{img}}" width="{{../../imgWidth}}" height="{{../../imgHeight}}"></img>{{/if}}</span>{{/ifAny}}

                    <span class="txt" style="{{../titleStyle}}">{{title}}</span>
                    {{#if notification}}<span class="notification">{{{notification}}}</span>{{/if}}
                </a>
            </li>
            {{/each}}
        </ul>
    </div>
    </div>
    </div>
</script>
<script id="o-blockGrid" type="template">
    {{#ifAny title subtitle buttons}}
    <div class="row-fluid">
        <div class="span12">
            {{set "titleStyle" "float:left;"}}
            <div style="{{titleStyle}}">
                <h2><span>{{{title}}}</span></h2><h6><span>{{{subtitle}}}</span></h6>
            </div>
            {{#if buttons}}
            <div style="float:{{#if buttonsLeft}}left{{else}}right{{/if}}; padding-bottom:10px;">
                {{{buttons}}}
            </div>
            {{/if}}
        </div>
    </div>
    {{/ifAny}}
    {{#set "numCols" true}}
           {{arraySize blockSizes}}
    {{/set}}
    {{#set "lastColIndex" true}}
               {{math numCols "-" "1"}}
    {{/set}}
    {{#each _hbSubViewCount}}
        {{#set "colIndex" true}}
           {{getCol cols="numCols" order="ltr"}}
        {{/set}}
        {{#set "colSize" true}}
               {{arrayValue "blockSizes" colIndex}}
        {{/set}}


        {{#ifEq colIndex "0"}}
        <div class="row-fluid">
        {{/ifEq}}
        <div class="span{{colSize}}" location="{{_hbParent.cid}}_{{_hbIndex}}"></div>
        {{#ifEq colIndex _hbParent.lastColIndex}}
        </div>
        {{/ifEq}}
    {{/each}}


    <span _template_="_template_" data-selector-format="index"/>
</script>
<script id="o-block-menu" type="partial">
    {{#ifNot hideMenu}}
    {{#with menuData}}
    {{set "pillMargin" 30}}

    <ul class="nav nav-{{navType}} {{#if stacked}}nav-stacked{{/if}} {{#ifEq ../menuLocation "left"}}pull-left{{/ifEq}} {{#ifEq ../menuLocation "right"}}pull-right{{/ifEq}} {{menuClasses}}" style="{{#if minWidth}}min-width: {{minWidth}}px; {{/if}}{{#ifAny ../menuLocation "left" "right"}}{{#ifEq navType "pills" }}margin-{{#ifEq ../../../menuLocation "left"}}right{{else}}left{{/ifEq}}: {{pillMargin}}px; {{/ifEq}} {{/ifAny}}">
        {{#each items}}
            {{#if items}}
                <li class="dropdown {{#if active}}active{{/if}} {{classes}}">
                <a class="dropdown-toggle" href="#">{{label}}</a>
                <ul class="dropdown-menu">
                {{#each items}}
                    {{#if header}}
                        <li class="nav-header">{{#if icon}}<i class="icon-{{icon}} {{#if iconWhite}}icon-white{{/if}}"></i>{{/if}}{{label}}</li>
                    {{else}}
                        {{#if divider}}
                            <li class="divider"></li>
                        {{else}}
                            <li class="{{#if active}}active{{/if}} {{classes}}">{{#ifNot disable}}<a href="{{link}}" data-event="select" data-name="{{_hbKey}}" {{data-fields data}}>{{/ifNot}}{{#if icon}}<i class="icon-{{icon}} {{#if iconWhite}}icon-white{{/if}}"></i>{{/if}}{{{label}}}{{#ifNot disable}}</a>{{/ifNot}}</li>
                        {{/if}}
                    {{/if}}

                {{/each}}
                </ul>
                </li>
            {{else}}
                {{#if header}}
                    <li class="nav-header">{{#if icon}}<i class="icon-{{icon}} {{#if iconWhite}}icon-white{{/if}}"></i>{{/if}}{{label}}</li>
                {{else}}
                    {{#if divider}}
                        <li class="divider"></li>
                    {{else}}
                       <li class="{{#if active}}active{{/if}} {{classes}}">{{#ifNot disable}}<a href="{{link}}" data-event="select" data-name="{{_hbKey}}" {{data-fields data}}>{{/ifNot}}{{#if icon}}<i class="icon-{{icon}} {{#if iconWhite}}icon-white{{/if}}"></i>{{/if}}{{{label}}}{{#ifNot disable}}</a>{{/ifNot}}</li>
                    {{/if}}
                {{/if}}
            {{/if}}
        {{/each}}
    </ul>
    {{/with}}
    {{/ifNot}}

</script>
<script id="o-block" type="template">
    {{set "border" true}}
    {{set "minimize" true}}
    {{set "box" false}}
    {{set "titleTag" "h4"}}
    <div class="{{#if box}}box {{#if closed}}closed{{/if}}{{/if}} {{blockClasses}}">
    {{#if box}}
    <div class="title" style="">
        <{{titleTag}} class="clearfix">

            <span class="left">
                {{#if icon}}<span class="icon16 icon-{{icon}}"></span>{{/if}}
                {{{title}}}&nbsp;
            </span>
            <span class="right" style="">
                {{{topRight}}}
            </span>
        </{{titleTag}}>
        {{#if minimize}}<a class="minimize" href="#" style="display: none;">Minimize</a>{{/if}}
    </div>
    {{else}}
    {{#if title}}
        <{{titleTag}}>
            {{#if icon}}<span class="icon16 icon-{{icon}}"></span>{{/if}}
            <span>{{{title}}}&nbsp;</span>
        </{{titleTag}}>
    {{/if}}
    {{/if}}
        {{set "minHeight" "0"}}
        <div class="{{#if box}}content{{/if}}" style="min-height:{{minHeight}}px;">
    {{#if hasMenu}}
    {{#ifNot hideMenu}}
        {{set "menuLocation" above}}
        {{set "menuFirst" true}}
        {{set "tabbable" true}}

        <div class="{{#ifEq navType "tabs"}}tabbable tabs-{{menuLocation}} {{/ifEq}}">
        {{#if menuFirst}}
            {{> o-block-menu}}
        {{/if}}
        <div class="{{#if border}}tab-content{{/if}}"  style="{{#if minHeight}}min-height:{{minHeight}}px;{{/if}} ">
            {{#each _hbSubViewCount}}
                {{#if _hbIndex}}
                    <div class="" location="{{_hbParent.cid}}_{{_hbIndex}}" style="overflow:hidden; {{_hbParent.contentStyle}}"/>
                {{/if}}
            {{/each}}
        </div>
        {{#ifNot menuFirst}}
            {{> o-block-menu}}
        {{/ifNot}}
        </div>
    {{else}}
        {{#each _hbSubViewCount}}
                {{#ifNotEq _hbIndex "0"}}
                <div location="{{_hbParent.cid}}_{{_hbIndex}}"></div>
                {{/ifNotEq}}
        {{/each}}
    {{/ifNot}}
    {{else}}
        {{#each _hbSubViewCount}}
                <div location="{{_hbParent.cid}}_{{_hbIndex}}"></div>
        {{/each}}
    {{/if}}
        </div>
        </div>

    <span _template_="_template_" data-selector-format="index"/>


</script>
<script id="o-nav-items" type="partial">
        {{#each items}}
            {{#if items}}
                <li class="dropdown {{#if active}}active{{/if}}">
                <a class="dropdown-toggle" href="#">{{label}}</a>
                <ul class="dropdown-menu">
                {{#each items}}
                    {{#if header}}
                        <li class="nav-header">{{#if icon}}<i class="icon-{{icon}} {{#if iconWhite}}icon-white{{/if}}"></i>{{/if}}{{label}}</li>
                    {{else}}
                        {{#if divider}}
                            <li class="divider"></li>
                        {{else}}
                            <li class="{{#if active}}active{{/if}} {{classes}}">{{#ifNot disable}}<a href="{{link}}" data-event="select" data-name="{{_hbKey}}" {{data-fields data}}>{{/ifNot}}{{#if icon}}<i class="icon-{{icon}} {{#if iconWhite}}icon-white{{/if}}"></i>{{/if}}{{{label}}}{{#ifNot disable}}</a>{{/ifNot}}</li>
                        {{/if}}
                    {{/if}}

                {{/each}}
                </ul>
                </li>
            {{else}}
                {{#if header}}
                    <li class="nav-header">{{#if icon}}<i class="icon-{{icon}} {{#if iconWhite}}icon-white{{/if}}"></i>{{/if}}{{label}}</li>
                {{else}}
                    {{#if divider}}
                        <li class="divider"></li>
                    {{else}}
                        <li class="{{#if active}}active{{/if}} {{classes}}">{{#if custom}}{{{custom}}}{{else}}{{#ifNot disable}}<a href="{{link}}" data-event="select" data-name="{{_hbKey}}" {{data-fields data}}>{{/ifNot}}{{#if icon}}<i class="icon-{{icon}} {{#if iconWhite}}icon-white{{/if}}"></i>{{/if}}{{{label}}}{{#ifNot disable}}</a>{{/ifNot}}{{/if}}</li>
                    {{/if}}
                {{/if}}
            {{/if}}
        {{/each}}
</script>
<script id="o-menu" type="template">
    {{set "visible" true}}
    {{#if visible}}
    {{#if title}}
    <h1 class="page-title">{{title}}</h1>
    {{/if}}
    {{#if items}}
    {{#if ../tabbable}}
    <div class="tabbable tabs-{{tabLocation}}">
    {{/if}}
    {{set "menuFullWidth" true}}
    <ul class="nav nav-{{navType}} {{#if stacked}}nav-stacked{{/if}} {{classes}}" style="{{#ifEq navType "list"}} {{else}}{{#if menuFullWidth}}width:100%{{/if}}{{/ifEq}}">
        {{> o-nav-items}}
    </ul>
    {{#if ../tabbable}}
    </div>
    {{/if}}
    {{/if}}
    {{/if}}
</script>
<script type="template" id="o-navbar">
    <div class="navbar {{navType}}">
        <div class="navbar-inner">
        {{#if title}}
            <a class="brand" href="{{titleUrl}}">{{{title}}}</a>
        {{/if}}
            <ul class="nav">
                {{> o-nav-items}}
            </ul>
        {{{extra}}}
        </div>
    </div>
</script>
<script id="o-alert" type="template">
{{#if closeButton}}
    <a class="close" data-dismiss="alert">×</a>
{{/if}}
{{#if titleHeading}}
{{#if heading}}
    <h4 class="alert-heading">{{heading}}</h4>
{{/if}}
{{/if}}
{{#if buttons}}
{{#each buttons}}
    <!-- todo: implement button options in alert -->
{{/each}}
{{/if}}
    {{#ifNot titleHeading}}
    <strong>{{heading}}</strong>
    {{/ifNot}}
{{{text}}}
</script>
<script id="o-alert" type="template">
{{#if closeButton}}
    <a class="close" data-dismiss="alert">×</a>
{{/if}}
{{#if heading}}
    <h4 class="alert-heading">{{heading}}</h4>
{{/if}}
{{#if buttons}}
{{#each buttons}}
    <!-- todo: implement button options in alert -->
{{/each}}
{{/if}}
{{{text}}}
</script>
<script id="o-search" type="template">
<div class="searchbox">
<form class="form-search" id="search-form">
    <input type="text" id="search-field" class="input-medium search-query" placeholder="Search" value="">
    <!--<button class="btn" type="submit">Search</button>-->
</form>
</div>
</script>
<script id="o-switch" type="template">
    {{#if value}}
    <a class="enable" href="#">{{onText}}</a>
    {{/if}}
    <a class="switch-switch" href="#">Switch</a>
    {{#ifNot value}}
    <a class="disable" href="#">{{offText}}</a>
    {{/ifNot}}

</script>


<script id="o-modal" type="template">
    <div class="modal hide in row-fluid" style="display: block;">
    <div class="span12">
        {{#ifAny title close}}
        <div class="modal-header">
            {{#if close}}
                <a class="close" href="#">×</a>
            {{/if}}
            <h3>{{#if title}}{{{title}}}{{else}}&nbsp;{{/if}}</h3>
        </div>
        {{/ifAny}}
        <div class="modal-body" style="overflow-x:hidden;">
            {{#if text}}<p>{{{text}}}</p>{{/if}}
            <span location="modal-body"/>
        </div>
        {{#if buttons}}
        <div class="modal-footer">
            {{#each buttons}}
                <a class="btn {{classes}}" {{data-fields data}} href="{{link}}" target="{{target}}">{{text}}</a>
            {{/each}}
        </div>
        {{/if}}
    </div>
    </div>
    <span _template_="_template_" data-selector-format="location" data-selector-location="modal-body"/>
</script>
<script id="o-table" type="template">
    {{{tableHeader}}}
    <table>

        {{#if foot}}
            <tfoot><tr>
            {{#each foot}}
                <td class={{class}}>{{#if label}}{{{label}}}{{else}}{{{value}}}{{/if}}</td>
            {{/each}}
            </tr></tfoot>
        {{/if}}
    </table>
    {{{tableFooter}}}
</script>
<script id="o-table-simple" type="template">
    {{{tableHeader}}}
    <table class="{{classes}}">
    {{#if colGroup}}
        <colgroup>
        {{#each colGroup}}
           <col span="{{span}}" style="{{#if width}}width: {{width}};{{/if}} {{style}}">
        {{/each}}
        </colgroup>
    {{/if}}
    {{#if tHead}}
        <thead><tr>
        {{#each tHead}}
            <th class={{class}}>{{{label}}}</th>
        {{/each}}
        </tr></thead>
    {{/if}}
    {{#if tBody}}
        <tbody>
        {{#each tBody}}
            <tr class="{{class}}">
                {{#each cols}}
                    <td rowspan="{{rowspan}}" colspan="{{colspan}}" class="{{class}}">{{{value}}}</td>
                {{/each}}
            </tr>
        {{/each}}
        </tbody>
    {{/if}}
    {{#if tFoot}}
        <tfoot><tr>
        {{#each tFoot}}
            <td class={{class}}>{{{label}}}</td>
        {{/each}}
        </tr></tfoot>
    {{/if}}
    </table>
    {{{tableFooter}}}
</script>
