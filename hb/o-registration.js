(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-register'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "      <div class=\"container register\">        <div class=\"row-fluid\">          <div location=\"core\" class=\"span8 offset2\">            <h2>Register</h2>            <div>              <button class=\"social facebook\">facebook connect</button>              <button class=\"social github\">github connect</button>              <button class=\"social twitter\">twitter connect</button>            </div>              <fieldset data-view-cid=\"view39\" data-view-name=\"core_main\" class=\"view row\">                <div class=\"span4 names\">                <span location=\"first_name\">                  <div data-view-cid=\"view41\" data-view-name=\"first_name\" class=\"view\">                    <input type=\"text\" placeholder=\"First Name *\" value=\"\" class=\"text \" id=\"first_name\">                  </div>                </span>                <span location=\"last_name\">                  <div data-view-cid=\"view43\" data-view-name=\"last_name\" class=\"view\">                    <input type=\"text\" placeholder=\"Last Name *\" value=\"\" class=\"text \" id=\"last_name\">                  </div>                </span>              </div>                <div class=\"span4\">                <span location=\"email\">                  <div data-view-cid=\"view45\" data-view-name=\"email\" class=\"view\">                    <input type=\"text\" placeholder=\"Email *\" value=\"\" class=\"text \" id=\"email\">                  </div>                </span>              </div>                <div class=\"span4\">                <span location=\"password\">                  <div data-view-cid=\"view47\" data-view-name=\"password\" class=\"view\">                    <input type=\"password\" placeholder=\"Password *\" value=\"\" class=\"password \" id=\"password\">                  </div>                </span>              </div>                  <div class=\"span4\">                <span location=\"password_confirm\">                  <div data-view-cid=\"view49\" data-view-name=\"password_confirm\" class=\"view\">                   <input type=\"password\" placeholder=\"Repeat Password *\" value=\"\" class=\"password \" id=\"password_confirm\">                  </div>                </span>              </div>                  <div class=\"\">                <div class=\"span8 submit\">                  <button class=\"\" type=\"submit\">Sign up</button>                </div>              </div>              </fieldset>          </div>        </div>      </div>      <span data-selector-format=\"name\" _template_=\"_template_\"></span>  ";
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['o-register'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-registration-divider'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", self=this;

function program1(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.text) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.text; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }

function program3(depth0,data) {
  
  
  return "or";
  }

  buffer += "    <div class=\"or\">      <span>";
  stack1 = helpers['if'].call(depth0, depth0.text, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span>    </div>  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-registration-fieldset'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "      <div class=\"row-fluid\">          <div class=\"span12\">            <span location=\"account_name\">            </span>          </div>        </div>      <div class=\"row-fluid\">          <div class=\"span6 name\">            <span location=\"first_name\">              </span>            <span location=\"last_name\">              </span>          </div>            <div class=\"span6\">            <span location=\"email\">              </span>          </div>      </div>      <div class=\"row-fluid\">          <div class=\"span6\">            <span location=\"password\">              </span>          </div>            <div class=\"span6\">            <span location=\"password_confirm\">              </span>          </div>      </div>            <span data-selector-format=\"name\" _template_=\"_template_\"></span>  ";
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['o-registration-fieldset'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-registration-logins'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this, functionType="function";

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "                ";
  options = {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data};
  stack2 = ((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "colIndex", true, options) : helperMissing.call(depth0, "set", "colIndex", true, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "                ";
  options = {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0.colIndex, "0", options) : helperMissing.call(depth0, "ifEq", depth0.colIndex, "0", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "              <div class=\"span3\">                  <a href=\"";
  if (stack2 = helpers.url) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.url; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" class=\"login btn\">";
  if (stack2 = helpers.label) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.label; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</a>              </div>              ";
  options = {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0.colIndex, 2, options) : helperMissing.call(depth0, "ifEq", depth0.colIndex, 2, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "        ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "                 ";
  options = {hash:{
    'cols': ("4"),
    'order': ("ltr")
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.getCol || depth0.getCol),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "getCol", options)))
    + "              ";
  return buffer;
  }

function program4(depth0,data) {
  
  
  return "              <div class=\"row-fluid\">              ";
  }

function program6(depth0,data) {
  
  
  return "              </div>              ";
  }

  buffer += "        ";
  stack1 = helpers.each.call(depth0, depth0.logins, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  ";
  return buffer;
  });
})();
