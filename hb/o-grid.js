(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-grid'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "     	<div class=\"span12 pillbar\">     	<div class=\"row-fluid\">         <div class=\"span7\">             ";
  stack1 = helpers['if'].call(depth0, depth0.filters, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "         </div>         <div class=\"span5\">             ";
  stack1 = helpers.each.call(depth0, depth0.buttons, {hash:{},inverse:self.noop,fn:self.program(10, program10, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "         </div>         </div>         </div>         ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "     	      <ul class=\"nav nav-pills\">                   ";
  stack1 = helpers.each.call(depth0, depth0.filters, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "     	      </ul>             ";
  return buffer;
  }
function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "     		      <li class=\"filter ";
  stack1 = helpers['if'].call(depth0, depth0.active, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" data-event=\"filter\" data-filter=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"><a href=\"#\">";
  stack1 = helpers['if'].call(depth0, depth0.label, {hash:{},inverse:self.program(8, program8, data),fn:self.program(6, program6, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a></li>                   ";
  return buffer;
  }
function program4(depth0,data) {
  
  
  return "active";
  }

function program6(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.label) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.label; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program8(depth0,data) {
  
  var stack1, options;
  options = {hash:{},data:data};
  return escapeExpression(((stack1 = helpers.camel || depth0.camel),stack1 ? stack1.call(depth0, depth0.name, options) : helperMissing.call(depth0, "camel", depth0.name, options)));
  }

function program10(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "             ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "event", "select", options) : helperMissing.call(depth0, "set", "event", "select", options)))
    + "     	  	<a class=\"btn pull-right\" data-event=\"";
  if (stack2 = helpers.event) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.event; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" data-name=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" href=\"";
  if (stack2 = helpers.url) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.url; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">";
  stack2 = helpers['if'].call(depth0, depth0.label, {hash:{},inverse:self.program(8, program8, data),fn:self.program(6, program6, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</a>             ";
  return buffer;
  }

  buffer += "     ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "header", true, options) : helperMissing.call(depth0, "set", "header", true, options)))
    + "     <div class=\"row-fluid\">         ";
  stack2 = helpers['if'].call(depth0, depth0.header, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "     </div><!--end.row-fluid -->     	<ul class=\"thumbnails\" location=\"grid\">      	</ul>       <div class=\"pagination\">     </div>      <span _template_=\"_template_\" data-selector-format=\"location\" data-selector-location=\"grid\"></span> ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['o-grid'] = {"selectorFormat":"location","selectorLocation":"grid"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-grid-cell'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function";


  buffer += "        <a class=\"thumbnail\" href=\"#\">         ";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "       </a> ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-grid-service'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;


  buffer += "         <div class=\"thumbnail\">          ";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " - ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.camel || depth0.camel),stack1 ? stack1.call(depth0, depth0.category, options) : helperMissing.call(depth0, "camel", depth0.category, options)))
    + " - ";
  if (stack2 = helpers.id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "         </div> ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-grid-apps'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  
  return "-empty";
  }

  buffer += "   <a href=\"#\" class=\"app-card-link\">   <div class=\"app-card-contain\">   <div class=\"app-card\">     <img src=\"/img/app_large.png\" class=\"app-icon\"/>     <h5 class=\"app-title\">";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h5>              <!--<i class=\"icon-star";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  stack2 = ((stack1 = helpers.ifNot || depth0.ifNot),stack1 ? stack1.call(depth0, depth0.favorite, options) : helperMissing.call(depth0, "ifNot", depth0.favorite, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"></i>-->           </div>   </div>   </a> ";
  return buffer;
  });
})();
