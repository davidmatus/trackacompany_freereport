(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-view'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          ";
  stack1 = helpers['with'].call(depth0, depth0.img, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          <img src=\"";
  if (stack1 = helpers.src) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.src; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" style=\"border:1px solid #333; display: block; position: absolute; top: 0px; left: 20px;\">          ";
  return buffer;
  }

function program4(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          <fieldset>              ";
  stack1 = helpers['if'].call(depth0, depth0.label, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "                ";
  stack1 = helpers.each.call(depth0, depth0.fields, {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          </fieldset>          ";
  return buffer;
  }
function program5(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              <legend>";
  if (stack1 = helpers.label) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.label; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</legend>              ";
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "              ";
  options = {hash:{},inverse:self.program(10, program10, data),fn:self.program(8, program8, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0.type, "hidden", options) : helperMissing.call(depth0, "ifEq", depth0.type, "hidden", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "              ";
  return buffer;
  }
function program8(depth0,data) {
  
  
  return "              ";
  }

function program10(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              ";
  stack1 = helpers['if'].call(depth0, depth0.label, {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              ";
  return buffer;
  }
function program11(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              <div class=\"control-group row-fuild\">                  <label class=\"control-label \">";
  if (stack1 = helpers.label) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.label; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + ":</label>                  <div class=\"controls\">                      <span class=\"field-data ";
  if (stack1 = helpers.valueClasses) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.valueClasses; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span>                  </div>              </div>              ";
  return buffer;
  }

  buffer += "      <form id=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"form-horizontal ";
  if (stack1 = helpers.classes) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.classes; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" style=\"";
  if (stack1 = helpers.style) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.style; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">          ";
  stack1 = helpers['if'].call(depth0, depth0.img, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          ";
  stack1 = helpers.each.call(depth0, depth0.fieldSets, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      </form>  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-buttons'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "        ";
  options = {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data};
  stack2 = ((stack1 = helpers.ifNot || depth0.ifNot),stack1 ? stack1.call(depth0, depth0.disableActionBorder, options) : helperMissing.call(depth0, "ifNot", depth0.disableActionBorder, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "buttonOffset", "3", options) : helperMissing.call(depth0, "set", "buttonOffset", "3", options)))
    + "      ";
  options = {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data};
  stack2 = ((stack1 = helpers.ifNotEq || depth0.ifNotEq),stack1 ? stack1.call(depth0, depth0.formLayout, "inline", options) : helperMissing.call(depth0, "ifNotEq", depth0.formLayout, "inline", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = helpers['if'].call(depth0, depth0.prevButton, {hash:{},inverse:self.noop,fn:self.program(10, program10, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = helpers['if'].call(depth0, depth0.nextButton, {hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = helpers['if'].call(depth0, depth0.submitButton, {hash:{},inverse:self.noop,fn:self.program(16, program16, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = helpers['if'].call(depth0, depth0.cancelButton, {hash:{},inverse:self.noop,fn:self.program(19, program19, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = helpers.each.call(depth0, depth0.moreButtons, {hash:{},inverse:self.noop,fn:self.programWithDepth(22, program22, data, depth0),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  options = {hash:{},inverse:self.noop,fn:self.program(25, program25, data),data:data};
  stack2 = ((stack1 = helpers.ifNotEq || depth0.ifNotEq),stack1 ? stack1.call(depth0, depth0.formLayout, "inline", options) : helperMissing.call(depth0, "ifNotEq", depth0.formLayout, "inline", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  options = {hash:{},inverse:self.noop,fn:self.program(25, program25, data),data:data};
  stack2 = ((stack1 = helpers.ifNot || depth0.ifNot),stack1 ? stack1.call(depth0, depth0.disableActionBorder, options) : helperMissing.call(depth0, "ifNot", depth0.disableActionBorder, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<div class=\"form-actions\" style=\"margin-top: 0px; ";
  stack1 = helpers['if'].call(depth0, depth0.actionOffset, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">";
  return buffer;
  }
function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "margin-left: ";
  if (stack1 = helpers.actionOffset) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.actionOffset; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "px;";
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<div class=\"";
  options = {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data};
  stack2 = ((stack1 = helpers.ifNot || depth0.ifNot),stack1 ? stack1.call(depth0, depth0.disableButtonOffset, options) : helperMissing.call(depth0, "ifNot", depth0.disableButtonOffset, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\" style=\"";
  stack2 = helpers['if'].call(depth0, depth0.disableActionBorder, {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">";
  return buffer;
  }
function program6(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "offset";
  if (stack1 = helpers.buttonOffset) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.buttonOffset; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " span";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.math || depth0.math),stack1 ? stack1.call(depth0, "12-", depth0.buttonOffset, options) : helperMissing.call(depth0, "math", "12-", depth0.buttonOffset, options)));
  return buffer;
  }

function program8(depth0,data) {
  
  
  return "margin-top: 20px; ";
  }

function program10(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "      ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "prevButtonText", "Prev", options) : helperMissing.call(depth0, "set", "prevButtonText", "Prev", options)))
    + "      <button ";
  stack2 = helpers['if'].call(depth0, depth0.prevButtonTabIndex, {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " type=\"button\" class=\"btn prev ";
  if (stack2 = helpers.buttonClasses) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.buttonClasses; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + " ";
  if (stack2 = helpers.prevButtonClasses) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.prevButtonClasses; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">";
  if (stack2 = helpers.prevButtonText) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.prevButtonText; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</button>      ";
  return buffer;
  }
function program11(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "tabindex=\"";
  if (stack1 = helpers.prevButtonTabIndex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.prevButtonTabIndex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"";
  return buffer;
  }

function program13(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "      ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "nextButtonText", "Next", options) : helperMissing.call(depth0, "set", "nextButtonText", "Next", options)))
    + "      <button ";
  stack2 = helpers['if'].call(depth0, depth0.nextButtonTabIndex, {hash:{},inverse:self.noop,fn:self.program(14, program14, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " type=\"submit\" class=\"btn btn-primary next ";
  if (stack2 = helpers.buttonClasses) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.buttonClasses; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + " ";
  if (stack2 = helpers.nextButtonClasses) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.nextButtonClasses; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">";
  if (stack2 = helpers.nextButtonText) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.nextButtonText; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</button>      ";
  return buffer;
  }
function program14(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "tabindex=\"";
  if (stack1 = helpers.nextButtonTabIndex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.nextButtonTabIndex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"";
  return buffer;
  }

function program16(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "      ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "submitButtonText", "Submit", options) : helperMissing.call(depth0, "set", "submitButtonText", "Submit", options)))
    + "      <button ";
  stack2 = helpers['if'].call(depth0, depth0.submitButtonTabIndex, {hash:{},inverse:self.noop,fn:self.program(17, program17, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " type=\"submit\" class=\"btn btn-primary submit ";
  if (stack2 = helpers.buttonClasses) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.buttonClasses; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + " ";
  if (stack2 = helpers.submitButtonClasses) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.submitButtonClasses; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">";
  if (stack2 = helpers.submitButtonText) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.submitButtonText; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</button>      ";
  return buffer;
  }
function program17(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "tabindex=\"";
  if (stack1 = helpers.submitButtonTabIndex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.submitButtonTabIndex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"";
  return buffer;
  }

function program19(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "      ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "cancelButtonText", "Cancel", options) : helperMissing.call(depth0, "set", "cancelButtonText", "Cancel", options)))
    + "      <button ";
  stack2 = helpers['if'].call(depth0, depth0.cancelButtonTabIndex, {hash:{},inverse:self.noop,fn:self.program(20, program20, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " type=\"button\" class=\"btn cancel ";
  if (stack2 = helpers.buttonClasses) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.buttonClasses; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + " ";
  if (stack2 = helpers.cancelButtonClasses) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.cancelButtonClasses; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">";
  if (stack2 = helpers.cancelButtonText) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.cancelButtonText; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</button>      ";
  return buffer;
  }
function program20(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "tabindex=\"";
  if (stack1 = helpers.cancelButtonTabIndex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.cancelButtonTabIndex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"";
  return buffer;
  }

function program22(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "      <a ";
  stack1 = helpers['if'].call(depth0, depth0.tabindex, {hash:{},inverse:self.noop,fn:self.program(23, program23, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " class=\"btn "
    + escapeExpression(((stack1 = depth1.buttonClasses),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " ";
  if (stack2 = helpers.classes) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.classes; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers['data-fields'] || depth0['data-fields']),stack1 ? stack1.call(depth0, depth0.data, options) : helperMissing.call(depth0, "data-fields", depth0.data, options)))
    + " href=\"";
  if (stack2 = helpers.link) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.link; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" target=\"";
  if (stack2 = helpers.target) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.target; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">";
  if (stack2 = helpers.text) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.text; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</a>      ";
  return buffer;
  }
function program23(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "tabindex=\"";
  if (stack1 = helpers.tabindex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.tabindex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"";
  return buffer;
  }

function program25(depth0,data) {
  
  
  return "</div>";
  }

  buffer += "      ";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  stack2 = ((stack1 = helpers.ifAny || depth0.ifAny),stack1 ? stack1.call(depth0, depth0.prevButton, depth0.nextButton, depth0.submitButton, depth0.cancelButton, depth0.moreButtons, options) : helperMissing.call(depth0, "ifAny", depth0.prevButton, depth0.nextButton, depth0.submitButton, depth0.cancelButton, depth0.moreButtons, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "    ";
  return buffer;
  });
})();
(function() {
	Handlebars.registerPartial('o-form-buttons', Handlebars.templates['o-form-buttons']); 
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-errors'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "      <span class=\"help-inline ";
  if (stack1 = helpers['error-classes']) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0['error-classes']; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (stack1 = helpers.errMsg) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.errMsg; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span>      ";
  return buffer;
  }

  buffer += "      ";
  stack1 = helpers['if'].call(depth0, depth0.errMsg, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
	Handlebars.registerPartial('o-form-errors', Handlebars.templates['o-form-errors']); 
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-help'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "      <!-- <a href=\"#\" class=\"help-toggle icon-question-sign\">&nbsp;</a> -->      <span class=\" help-block blue ";
  if (stack1 = helpers['help-classes']) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0['help-classes']; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" style=\"";
  stack1 = helpers['if'].call(depth0, depth0.helpFloat, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"><span class=\"msg\">";
  if (stack1 = helpers.help) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.help; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span></span>      ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "float: ";
  if (stack1 = helpers.helpFloat) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.helpFloat; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + ";";
  return buffer;
  }

  buffer += "      ";
  stack1 = helpers['if'].call(depth0, depth0.help, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
	Handlebars.registerPartial('o-form-help', Handlebars.templates['o-form-help']); 
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-steps'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "tabbable tabs-";
  if (stack1 = helpers.menuLocation) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.menuLocation; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " ";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "        ";
  options = {hash:{},inverse:self.noop,fn:self.programWithDepth(4, program4, data, depth0),data:data};
  stack2 = ((stack1 = helpers.ifNotEq || depth0.ifNotEq),stack1 ? stack1.call(depth0, depth0.menuLocation, "below", options) : helperMissing.call(depth0, "ifNotEq", depth0.menuLocation, "below", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "        ";
  return buffer;
  }
function program4(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "      ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "pillMargin", 30, options) : helperMissing.call(depth0, "set", "pillMargin", 30, options)))
    + "      <ul class=\"nav nav-";
  if (stack2 = helpers.navType) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.navType; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + " ";
  stack2 = helpers['if'].call(depth0, depth0.stacked, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  options = {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth1.ifEq),stack1 ? stack1.call(depth0, depth1.menuLocation, "left", options) : helperMissing.call(depth0, "ifEq", depth1.menuLocation, "left", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  options = {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth1.ifEq),stack1 ? stack1.call(depth0, depth1.menuLocation, "right", options) : helperMissing.call(depth0, "ifEq", depth1.menuLocation, "right", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " \" style=\"";
  stack2 = helpers['if'].call(depth0, depth0.minWidth, {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{},inverse:self.noop,fn:self.programWithDepth(13, program13, data, depth1),data:data};
  stack2 = ((stack1 = helpers.ifAny || depth1.ifAny),stack1 ? stack1.call(depth0, depth1.menuLocation, "left", "right", options) : helperMissing.call(depth0, "ifAny", depth1.menuLocation, "left", "right", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  stack2 = helpers['if'].call(depth0, depth0.hideStepsMenu, {hash:{},inverse:self.noop,fn:self.program(19, program19, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">      ";
  stack2 = helpers.each.call(depth0, depth0.steps, {hash:{},inverse:self.noop,fn:self.program(21, program21, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "        </ul>      ";
  return buffer;
  }
function program5(depth0,data) {
  
  
  return "nav-stacked";
  }

function program7(depth0,data) {
  
  
  return "pull-left";
  }

function program9(depth0,data) {
  
  
  return "pull-right";
  }

function program11(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "min-width: ";
  if (stack1 = helpers.minWidth) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.minWidth; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "px; ";
  return buffer;
  }

function program13(depth0,data,depth2) {
  
  var buffer = "", stack1, stack2, options;
  options = {hash:{},inverse:self.noop,fn:self.programWithDepth(14, program14, data, depth2),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0.navType, "pills", options) : helperMissing.call(depth0, "ifEq", depth0.navType, "pills", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  return buffer;
  }
function program14(depth0,data,depth3) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "margin-";
  options = {hash:{},inverse:self.program(17, program17, data),fn:self.program(15, program15, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth3.ifEq),stack1 ? stack1.call(depth0, depth3.menuLocation, "left", options) : helperMissing.call(depth0, "ifEq", depth3.menuLocation, "left", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += ": ";
  if (stack2 = helpers.pillMargin) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.pillMargin; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "px; ";
  return buffer;
  }
function program15(depth0,data) {
  
  
  return "right";
  }

function program17(depth0,data) {
  
  
  return "left";
  }

function program19(depth0,data) {
  
  
  return "display:none;";
  }

function program21(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "        <li class=\"";
  options = {hash:{},inverse:self.noop,fn:self.program(22, program22, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0._hbKey, ((stack1 = depth0._hbParent),stack1 == null || stack1 === false ? stack1 : stack1.activeStep), options) : helperMissing.call(depth0, "ifEq", depth0._hbKey, ((stack1 = depth0._hbParent),stack1 == null || stack1 === false ? stack1 : stack1.activeStep), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"><a href=\"#\" class=\"form-step\" data-step=\"";
  if (stack2 = helpers._hbKey) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0._hbKey; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" data-toggle=\"tab\">";
  stack2 = helpers['if'].call(depth0, depth0.label, {hash:{},inverse:self.program(26, program26, data),fn:self.program(24, program24, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</a></li>      ";
  return buffer;
  }
function program22(depth0,data) {
  
  
  return "active";
  }

function program24(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.label) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.label; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }

function program26(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program28(depth0,data) {
  
  
  return "tab-content";
  }

function program30(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "min-height:";
  if (stack1 = helpers.minHeight) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.minHeight; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "px;";
  return buffer;
  }

function program32(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "          <div class=\"tab-pane ";
  options = {hash:{},inverse:self.noop,fn:self.program(22, program22, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0._hbKey, ((stack1 = depth0._hbParent),stack1 == null || stack1 === false ? stack1 : stack1.activeStep), options) : helperMissing.call(depth0, "ifEq", depth0._hbKey, ((stack1 = depth0._hbParent),stack1 == null || stack1 === false ? stack1 : stack1.activeStep), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\" data-step=\"";
  if (stack2 = helpers._hbKey) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0._hbKey; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">              <div class=\"";
  if (stack2 = helpers.fieldSetSize) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.fieldSetSize; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" location=\"";
  if (stack2 = helpers._hbKey) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0._hbKey; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" ";
  options = {hash:{},inverse:self.noop,fn:self.program(33, program33, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0.formLayout, "inline", options) : helperMissing.call(depth0, "ifEq", depth0.formLayout, "inline", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "></div>      </div>      ";
  return buffer;
  }
function program33(depth0,data) {
  
  
  return "style=\"display:inline\"";
  }

function program35(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "      ";
  options = {hash:{},inverse:self.noop,fn:self.programWithDepth(36, program36, data, depth0),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0.menuLocation, "below", options) : helperMissing.call(depth0, "ifEq", depth0.menuLocation, "below", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  return buffer;
  }
function program36(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "      <ul class=\"nav nav-";
  if (stack1 = helpers.navType) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.navType; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " ";
  stack1 = helpers['if'].call(depth0, depth0.stacked, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ";
  options = {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth1.ifEq),stack1 ? stack1.call(depth0, depth1.menuLocation, "left", options) : helperMissing.call(depth0, "ifEq", depth1.menuLocation, "left", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  options = {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth1.ifEq),stack1 ? stack1.call(depth0, depth1.menuLocation, "right", options) : helperMissing.call(depth0, "ifEq", depth1.menuLocation, "right", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " \" style=\"";
  stack2 = helpers['if'].call(depth0, depth0.minWidth, {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  options = {hash:{},inverse:self.noop,fn:self.programWithDepth(37, program37, data, depth1),data:data};
  stack2 = ((stack1 = helpers.ifAny || depth1.ifAny),stack1 ? stack1.call(depth0, depth1.menuLocation, "left", "right", options) : helperMissing.call(depth0, "ifAny", depth1.menuLocation, "left", "right", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">      ";
  stack2 = helpers.each.call(depth0, depth0.steps, {hash:{},inverse:self.noop,fn:self.program(40, program40, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "        </ul>      ";
  return buffer;
  }
function program37(depth0,data,depth2) {
  
  var buffer = "", stack1, stack2, options;
  options = {hash:{},inverse:self.noop,fn:self.programWithDepth(38, program38, data, depth2),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0.navType, "pills", options) : helperMissing.call(depth0, "ifEq", depth0.navType, "pills", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  return buffer;
  }
function program38(depth0,data,depth3) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "margin-";
  options = {hash:{},inverse:self.program(17, program17, data),fn:self.program(15, program15, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth3.ifEq),stack1 ? stack1.call(depth0, depth3.menuLocation, "left", options) : helperMissing.call(depth0, "ifEq", depth3.menuLocation, "left", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += ": 30px; ";
  return buffer;
  }

function program40(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "      <li class=\"";
  options = {hash:{},inverse:self.noop,fn:self.program(22, program22, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0._hbKey, ((stack1 = depth0._hbParent),stack1 == null || stack1 === false ? stack1 : stack1.activeStep), options) : helperMissing.call(depth0, "ifEq", depth0._hbKey, ((stack1 = depth0._hbParent),stack1 == null || stack1 === false ? stack1 : stack1.activeStep), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"><a href=\"#\" class=\"form-step\" data-step=\"";
  if (stack2 = helpers._hbKey) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0._hbKey; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" data-toggle=\"tab\">";
  stack2 = helpers['if'].call(depth0, depth0.label, {hash:{},inverse:self.program(26, program26, data),fn:self.program(41, program41, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</a></li>      ";
  return buffer;
  }
function program41(depth0,data) {
  
  
  return "label";
  }

  buffer += "      ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "navType", "tabs", options) : helperMissing.call(depth0, "set", "navType", "tabs", options)))
    + "      ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "menuLocation", "above", options) : helperMissing.call(depth0, "set", "menuLocation", "above", options)))
    + "        <div class=\"";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0.navType, "tabs", options) : helperMissing.call(depth0, "ifEq", depth0.navType, "tabs", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">      ";
  options = {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data};
  stack2 = ((stack1 = helpers.ifNot || depth0.ifNot),stack1 ? stack1.call(depth0, depth0.hideSteps, options) : helperMissing.call(depth0, "ifNot", depth0.hideSteps, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      <div class=\"";
  options = {hash:{},inverse:self.noop,fn:self.program(28, program28, data),data:data};
  stack2 = ((stack1 = helpers.ifNot || depth0.ifNot),stack1 ? stack1.call(depth0, depth0.hideSteps, options) : helperMissing.call(depth0, "ifNot", depth0.hideSteps, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\" style=\"";
  stack2 = helpers['if'].call(depth0, depth0.minHeight, {hash:{},inverse:self.noop,fn:self.program(30, program30, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">          ";
  stack2 = helpers.each.call(depth0, depth0.steps, {hash:{},inverse:self.noop,fn:self.program(32, program32, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      </div>      ";
  options = {hash:{},inverse:self.noop,fn:self.program(35, program35, data),data:data};
  stack2 = ((stack1 = helpers.ifNot || depth0.ifNot),stack1 ? stack1.call(depth0, depth0.hideSteps, options) : helperMissing.call(depth0, "ifNot", depth0.hideSteps, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "        </div>  ";
  return buffer;
  });
})();
(function() {
	Handlebars.registerPartial('o-form-steps', Handlebars.templates['o-form-steps']); 
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.formSize) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.formSize; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program3(depth0,data) {
  
  
  return "span12";
  }

function program5(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "form-";
  options = {hash:{},inverse:self.program(8, program8, data),fn:self.program(6, program6, data),data:data};
  stack2 = ((stack1 = helpers.ifAll || depth0.ifAll),stack1 ? stack1.call(depth0, depth0.embeded, depth0.formLayoutEmbeded, options) : helperMissing.call(depth0, "ifAll", depth0.embeded, depth0.formLayoutEmbeded, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  return buffer;
  }
function program6(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.formLayoutEmbeded) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.formLayoutEmbeded; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program8(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.formLayout) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.formLayout; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program10(depth0,data) {
  
  
  return "margin: 0;";
  }

function program12(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              <h2>";
  if (stack1 = helpers.title) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.title; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h2>              ";
  return buffer;
  }

function program14(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              ";
  stack1 = self.invokePartial(partials['o-form-steps'], 'o-form-steps', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              ";
  return buffer;
  }

function program16(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              ";
  stack1 = helpers['if'].call(depth0, depth0.fieldSets, {hash:{},inverse:self.noop,fn:self.program(17, program17, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          ";
  return buffer;
  }
function program17(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "              <div class=\"";
  if (stack1 = helpers.fieldSetSize) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.fieldSetSize; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" location=\"defaultStep\" ";
  options = {hash:{},inverse:self.noop,fn:self.program(18, program18, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0.formLayout, "inline", options) : helperMissing.call(depth0, "ifEq", depth0.formLayout, "inline", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "></div>          ";
  return buffer;
  }
function program18(depth0,data) {
  
  
  return "style=\"display:inline\"";
  }

  buffer += "      <div class=\"row-fluid\">          <div class=\"";
  stack1 = helpers['if'].call(depth0, depth0.formSize, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" style=\"";
  if (stack1 = helpers.style) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.style; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">              <form id=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"";
  options = {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data};
  stack2 = ((stack1 = helpers.ifAny || depth0.ifAny),stack1 ? stack1.call(depth0, depth0.formLayout, depth0.formLayoutEmbeded, options) : helperMissing.call(depth0, "ifAny", depth0.formLayout, depth0.formLayoutEmbeded, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  if (stack2 = helpers.formClasses) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.formClasses; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" style=\"";
  if (stack2 = helpers.formStyle) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.formStyle; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + " ";
  options = {hash:{},inverse:self.noop,fn:self.program(10, program10, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0.formLayout, "inline", options) : helperMissing.call(depth0, "ifEq", depth0.formLayout, "inline", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">              ";
  stack2 = helpers['if'].call(depth0, depth0.title, {hash:{},inverse:self.noop,fn:self.program(12, program12, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "              ";
  stack2 = helpers['if'].call(depth0, depth0.steps, {hash:{},inverse:self.program(16, program16, data),fn:self.program(14, program14, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "          ";
  stack2 = self.invokePartial(partials['o-form-buttons'], 'o-form-buttons', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "          </form>      </div>      </div>      <span _template_=\"_template_\" data-selector-format=\"field\" data-selector-field=\"step\" data-selector-default-value=\"defaultStep\"/>    ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['o-form'] = {"selectorFormat":"field","selectorField":"step","selectorDefaultValue":"defaultStep"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-fieldset'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<legend>";
  if (stack1 = helpers.label) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.label; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</legend>";
  return buffer;
  }

function program3(depth0,data) {
  
  
  return "<div class=\"row-fluid\">";
  }

function program5(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "      ";
  options = {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data};
  stack2 = ((stack1 = helpers.ifNotEq || depth1.ifNotEq),stack1 ? stack1.call(depth0, depth1.layoutCols, "1", options) : helperMissing.call(depth0, "ifNotEq", depth1.layoutCols, "1", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = helpers.each.call(depth0, depth1.fields, {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  options = {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data};
  stack2 = ((stack1 = helpers.ifNotEq || depth1.ifNotEq),stack1 ? stack1.call(depth0, depth1.layoutCols, "1", options) : helperMissing.call(depth0, "ifNotEq", depth1.layoutCols, "1", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  return buffer;
  }
function program6(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "<div class=\"span";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.math || depth0.math),stack1 ? stack1.call(depth0, "12/", depth0._hbCount, options) : helperMissing.call(depth0, "math", "12/", depth0._hbCount, options)))
    + "\">";
  return buffer;
  }

function program8(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "      ";
  options = {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data};
  if (stack1 = helpers.ifCol) { stack1 = stack1.call(depth0, options); }
  else { stack1 = depth0.ifCol; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if (!helpers.ifCol) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  return buffer;
  }
function program9(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "      <span location=\"";
  if (stack1 = helpers._hbKey) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0._hbKey; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"/>      ";
  return buffer;
  }

function program11(depth0,data) {
  
  
  return "</div>";
  }

  buffer += "      ";
  stack1 = helpers['if'].call(depth0, depth0.label, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "layoutCols", 1, options) : helperMissing.call(depth0, "set", "layoutCols", 1, options)))
    + "      ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "layoutOrder", "down", options) : helperMissing.call(depth0, "set", "layoutOrder", "down", options)))
    + "      ";
  options = {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data};
  stack2 = ((stack1 = helpers.ifNotEq || depth0.ifNotEq),stack1 ? stack1.call(depth0, depth0.layoutCols, "1", options) : helperMissing.call(depth0, "ifNotEq", depth0.layoutCols, "1", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = helpers.each.call(depth0, depth0.layoutCols, {hash:{},inverse:self.noop,fn:self.programWithDepth(5, program5, data, depth0),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  options = {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data};
  stack2 = ((stack1 = helpers.ifNotEq || depth0.ifNotEq),stack1 ? stack1.call(depth0, depth0.layoutCols, "1", options) : helperMissing.call(depth0, "ifNotEq", depth0.layoutCols, "1", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['o-form-fieldset'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-label'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<label for=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\" control-label  ";
  if (stack1 = helpers['label-classes']) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0['label-classes']; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  stack1 = helpers['if'].call(depth0, depth0.required, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  if (stack1 = helpers.label) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.label; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</label>";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "* ";
  }

  buffer += "      ";
  stack1 = helpers['if'].call(depth0, depth0.label, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
	Handlebars.registerPartial('o-form-label', Handlebars.templates['o-form-label']); 
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-hidden'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " tabindex=\"";
  if (stack1 = helpers.tabindex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.tabindex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ";
  return buffer;
  }

  buffer += "      <input type=\"hidden\" id=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"";
  if (stack1 = helpers.classes) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.classes; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" value=\"";
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ";
  stack1 = helpers['if'].call(depth0, depth0.tabindex, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " />  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-control-start'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var stack1, stack2, options;
  options = {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data};
  stack2 = ((stack1 = helpers.ifNotEq || depth0.ifNotEq),stack1 ? stack1.call(depth0, depth0.fieldLayout, "inline", options) : helperMissing.call(depth0, "ifNotEq", depth0.fieldLayout, "inline", options));
  if(stack2 || stack2 === 0) { return stack2; }
  else { return ''; }
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<div class=\" controls  ";
  if (stack1 = helpers['control-classes']) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0['control-classes']; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " \">";
  return buffer;
  }

  buffer += "      ";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0.formLayout, "horizontal", options) : helperMissing.call(depth0, "ifEq", depth0.formLayout, "horizontal", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
	Handlebars.registerPartial('o-form-control-start', Handlebars.templates['o-form-control-start']); 
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-control-end'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var stack1, stack2, options;
  options = {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data};
  stack2 = ((stack1 = helpers.ifNotEq || depth0.ifNotEq),stack1 ? stack1.call(depth0, depth0.fieldLayout, "inline", options) : helperMissing.call(depth0, "ifNotEq", depth0.fieldLayout, "inline", options));
  if(stack2 || stack2 === 0) { return stack2; }
  else { return ''; }
  }
function program2(depth0,data) {
  
  
  return "</div>";
  }

  buffer += "      ";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0.formLayout, "horizontal", options) : helperMissing.call(depth0, "ifEq", depth0.formLayout, "horizontal", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
	Handlebars.registerPartial('o-form-control-end', Handlebars.templates['o-form-control-end']); 
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-input'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<div class=\"";
  if (stack1 = helpers.size) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.size; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " ";
  if (stack1 = helpers.inputClasses) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.inputClasses; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " ";
  stack1 = helpers['if'].call(depth0, depth0.before, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ";
  stack1 = helpers['if'].call(depth0, depth0.after, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "input-prepend";
  }

function program4(depth0,data) {
  
  
  return "input-append";
  }

function program6(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<span class=\"add-on\">";
  if (stack1 = helpers.before) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.before; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span>";
  return buffer;
  }

function program8(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " tabindex=\"";
  if (stack1 = helpers.tabindex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.tabindex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ";
  return buffer;
  }

function program10(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<span class=\"add-on\">";
  if (stack1 = helpers.after) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.after; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span>";
  return buffer;
  }

function program12(depth0,data) {
  
  
  return "</div>";
  }

  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  stack2 = ((stack1 = helpers.ifAny || depth0.ifAny),stack1 ? stack1.call(depth0, depth0.before, depth0.after, options) : helperMissing.call(depth0, "ifAny", depth0.before, depth0.after, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = helpers['if'].call(depth0, depth0.before, {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "<input type=\"";
  if (stack2 = helpers.inputType) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.inputType; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" id=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" class=\"";
  if (stack2 = helpers.prependSize) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.prependSize; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + " ";
  if (stack2 = helpers.classes) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.classes; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" value=\"";
  if (stack2 = helpers.value) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.value; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" placeholder=\"";
  if (stack2 = helpers.placeholder) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.placeholder; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" ";
  if (stack2 = helpers.attributes) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.attributes; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  stack2 = helpers['if'].call(depth0, depth0.tabindex, {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " />";
  stack2 = helpers['if'].call(depth0, depth0.after, {hash:{},inverse:self.noop,fn:self.program(10, program10, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  options = {hash:{},inverse:self.noop,fn:self.program(12, program12, data),data:data};
  stack2 = ((stack1 = helpers.ifAny || depth0.ifAny),stack1 ? stack1.call(depth0, depth0.before, depth0.after, options) : helperMissing.call(depth0, "ifAny", depth0.before, depth0.after, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-encrypted'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  
  return "input-append";
  }

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          <span class=\"add-on\"><i class=\"icon-lock\"></i></span><input type=\"button\" id=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"nostyle ";
  stack1 = helpers['if'].call(depth0, depth0.after, {hash:{},inverse:self.program(6, program6, data),fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ";
  if (stack1 = helpers.classes) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.classes; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" value=\"Click to Change\" placeholder=\"";
  if (stack1 = helpers.placeholder) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.placeholder; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ";
  if (stack1 = helpers.attributes) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.attributes; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " />";
  stack1 = helpers['if'].call(depth0, depth0.after, {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          ";
  return buffer;
  }
function program4(depth0,data) {
  
  
  return "span10";
  }

function program6(depth0,data) {
  
  
  return "span11";
  }

function program8(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<span class=\"add-on\">";
  if (stack1 = helpers.after) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.after; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span>";
  return buffer;
  }

function program10(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          <span class=\"add-on\"><i class=\"icon-key\"></i></span><input type=\"password\" id=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"";
  stack1 = helpers['if'].call(depth0, depth0.after, {hash:{},inverse:self.program(6, program6, data),fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ";
  if (stack1 = helpers.classes) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.classes; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" value=\"\" placeholder=\"";
  if (stack1 = helpers.placeholder) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.placeholder; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ";
  if (stack1 = helpers.attributes) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.attributes; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ";
  stack1 = helpers['if'].call(depth0, depth0.tabindex, {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " />";
  stack1 = helpers['if'].call(depth0, depth0.after, {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          ";
  return buffer;
  }
function program11(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " tabindex=\"";
  if (stack1 = helpers.tabindex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.tabindex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ";
  return buffer;
  }

function program13(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "      &nbsp;&nbsp;<a ";
  stack1 = helpers['if'].call(depth0, depth0.value, {hash:{},inverse:self.program(16, program16, data),fn:self.program(14, program14, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " href=\"#modal\">Show</a>      ";
  return buffer;
  }
function program14(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "data-title=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" data-text=\"";
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"";
  return buffer;
  }

function program16(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "data-server=\"";
  if (stack1 = helpers.showUrl) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.showUrl; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"";
  return buffer;
  }

  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      <div class=\"input-prepend ";
  if (stack1 = helpers.inputClasses) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.inputClasses; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " ";
  stack1 = helpers['if'].call(depth0, depth0.after, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ";
  if (stack1 = helpers.size) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.size; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">          ";
  stack1 = helpers['if'].call(depth0, depth0.lock, {hash:{},inverse:self.program(10, program10, data),fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      </div>      ";
  stack1 = helpers['if'].call(depth0, depth0.showable, {hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "        ";
  stack1 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-raw'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, stack2, options, self=this, helperMissing=helpers.helperMissing, functionType="function";

function program1(depth0,data) {
  
  
  return "display:inline";
  }

  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      <div class=\"raw\" style=\"padding-top:5px; ";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0.formLayout, "inline", options) : helperMissing.call(depth0, "ifEq", depth0.formLayout, "inline", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">";
  if (stack2 = helpers.html) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.html; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</div>      ";
  stack2 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-customraw'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, self=this, functionType="function";


  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  if (stack1 = helpers.html) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.html; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  if (stack1 = helpers.customExtra) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.customExtra; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-daterange'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  
  return "input-prepend";
  }

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<span class=\"add-on\">";
  if (stack1 = helpers.before) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.before; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span>";
  return buffer;
  }

function program5(depth0,data) {
  
  
  return "span4";
  }

function program7(depth0,data) {
  
  
  return "span5";
  }

function program9(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<span class=\"add-on\">";
  if (stack1 = helpers.before2) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.before2; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span>";
  return buffer;
  }

  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "        <div class=\"";
  if (stack1 = helpers.size) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.size; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " date ";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  stack2 = ((stack1 = helpers.ifAny || depth0.ifAny),stack1 ? stack1.call(depth0, depth0.before, depth0.before2, options) : helperMissing.call(depth0, "ifAny", depth0.before, depth0.before2, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " input-append\">          ";
  stack2 = helpers['if'].call(depth0, depth0.before, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "<input type=\"";
  if (stack2 = helpers.inputType) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.inputType; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" id=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" class=\"";
  stack2 = helpers['if'].call(depth0, depth0.before, {hash:{},inverse:self.program(7, program7, data),fn:self.program(5, program5, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  if (stack2 = helpers.classes) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.classes; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" value=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.value),stack1 == null || stack1 === false ? stack1 : stack1.start)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" placeholder=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.placeholder),stack1 == null || stack1 === false ? stack1 : stack1.start)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" ><span class=\"add-on\"><i class=\"icon icon-th\"></i></span>          ";
  stack2 = helpers['if'].call(depth0, depth0.before2, {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "<input type=\"";
  if (stack2 = helpers.inputType) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.inputType; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" id=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "2\" class=\"";
  stack2 = helpers['if'].call(depth0, depth0.before2, {hash:{},inverse:self.program(7, program7, data),fn:self.program(5, program5, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "  ";
  if (stack2 = helpers.classes) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.classes; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" value=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.value),stack1 == null || stack1 === false ? stack1 : stack1.end)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" placeholder=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.placeholder),stack1 == null || stack1 === false ? stack1 : stack1.end)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" ><span class=\"add-on\"><i class=\"icon icon-th\"></i></span>      </div>      ";
  stack2 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-media-display'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "      <img src=\"/_core/media/media:preview/";
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "/small\"/>  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-media'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "      ";
  stack1 = helpers['if'].call(depth0, depth0.mediaPreview, {hash:{},inverse:self.program(4, program4, data),fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "      ";
  if (stack1 = helpers.mediaPreview) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.mediaPreview; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  return buffer;
  }

function program4(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "      <img src=\"/_core/media/media:preview/";
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "/small\"/>      ";
  return buffer;
  }

function program6(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "      <!-- ";
  if (stack1 = helpers.emptyPreview) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.emptyPreview; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " -->      ";
  return buffer;
  }

function program8(depth0,data) {
  
  
  return "          <button class=\"btn remove-media\" type=\"button\"><i class=\"icon-trash\"></i></button>          ";
  }

  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = helpers['if'].call(depth0, depth0.value, {hash:{},inverse:self.program(6, program6, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      <div class=\"btn-group btn-group-vertical\">          ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "buttonIcon", "picture", options) : helperMissing.call(depth0, "set", "buttonIcon", "picture", options)))
    + "          <button class=\"btn load-media\" type=\"button\"><i class=\"icon-";
  if (stack2 = helpers.buttonIcon) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.buttonIcon; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"></i></button>          ";
  stack2 = helpers['if'].call(depth0, depth0.value, {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      </div>          ";
  stack2 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-file-basic'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<span class=\"add-on\">";
  if (stack1 = helpers.before) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.before; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span>";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<span class=\"add-on\">";
  if (stack1 = helpers.after) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.after; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span>";
  return buffer;
  }

  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "        ";
  stack1 = helpers['if'].call(depth0, depth0.before, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "<input type=\"";
  if (stack1 = helpers.type) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.type; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" id=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"input-file input-";
  if (stack1 = helpers.size) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.size; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " ";
  if (stack1 = helpers.classes) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.classes; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" value=\"";
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" placeholder=\"";
  if (stack1 = helpers.placeholder) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.placeholder; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" >";
  stack1 = helpers['if'].call(depth0, depth0.after, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "    ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-file'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.uploadLabel) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.uploadLabel; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "Upload ";
  if (stack1 = helpers.label) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.label; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "...";
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<div class=\"";
  if (stack1 = helpers.size) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.size; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " ";
  if (stack1 = helpers.inputClasses) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.inputClasses; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " ";
  stack1 = helpers['if'].call(depth0, depth0.before, {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ";
  stack1 = helpers['if'].call(depth0, depth0.after, {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">";
  return buffer;
  }
function program6(depth0,data) {
  
  
  return "input-prepend";
  }

function program8(depth0,data) {
  
  
  return "input-append";
  }

function program10(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<span class=\"add-on\">";
  if (stack1 = helpers.before) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.before; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span>";
  return buffer;
  }

function program12(depth0,data) {
  
  
  return "[]";
  }

function program14(depth0,data) {
  
  
  return "multiple";
  }

function program16(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<span class=\"add-on\">";
  if (stack1 = helpers.after) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.after; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span>";
  return buffer;
  }

function program18(depth0,data) {
  
  
  return "</div>";
  }

  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      <span class=\"files\" />                  <span class=\"btn btn-success fileinput-button\">                      <span><i class=\"icon-plus icon-white\"></i> ";
  stack1 = helpers['if'].call(depth0, depth0.uploadLabel, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span>                      ";
  options = {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data};
  stack2 = ((stack1 = helpers.ifAny || depth0.ifAny),stack1 ? stack1.call(depth0, depth0.before, depth0.after, options) : helperMissing.call(depth0, "ifAny", depth0.before, depth0.after, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "                      ";
  stack2 = helpers['if'].call(depth0, depth0.before, {hash:{},inverse:self.noop,fn:self.program(10, program10, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "                      <input type=\"";
  if (stack2 = helpers.type) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.type; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" id=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" name=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2);
  stack2 = helpers['if'].call(depth0, depth0.multiple, {hash:{},inverse:self.noop,fn:self.program(12, program12, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\" ";
  stack2 = helpers['if'].call(depth0, depth0.multiple, {hash:{},inverse:self.noop,fn:self.program(14, program14, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " class=\"nostyle input-file ";
  if (stack2 = helpers.prependSize) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.prependSize; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + " ";
  if (stack2 = helpers.classes) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.classes; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" value=\"";
  if (stack2 = helpers.value) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.value; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" placeholder=\"";
  if (stack2 = helpers.placeholder) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.placeholder; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" ";
  if (stack2 = helpers.attributes) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.attributes; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " >                      ";
  stack2 = helpers['if'].call(depth0, depth0.after, {hash:{},inverse:self.noop,fn:self.program(16, program16, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "                      ";
  options = {hash:{},inverse:self.noop,fn:self.program(18, program18, data),data:data};
  stack2 = ((stack1 = helpers.ifAny || depth0.ifAny),stack1 ? stack1.call(depth0, depth0.before, depth0.after, options) : helperMissing.call(depth0, "ifAny", depth0.before, depth0.after, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "                  </span>      ";
  stack2 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      <!-- The loading indicator is shown during image processing -->      <div class=\"fileupload-loading\"></div>      <br>        ";
  stack2 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-file-upload-single'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "      ";
  stack1 = helpers['if'].call(depth0, depth0.error, {hash:{},inverse:self.program(4, program4, data),fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "      <span class=\"file-error\">";
  if (stack1 = helpers.error) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.error; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span>      ";
  return buffer;
  }

function program4(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "      ";
  options = {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data};
  stack2 = ((stack1 = helpers.ifAny || depth0.ifAny),stack1 ? stack1.call(depth0, depth0.type, depth0.name, options) : helperMissing.call(depth0, "ifAny", depth0.type, depth0.name, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  return buffer;
  }
function program5(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "      <span class=\"template-upload\">      <span class=\"preview\"><span class=\"fade\"></span></span>          ";
  options = {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data};
  stack2 = ((stack1 = helpers.ifNot || depth0.ifNot),stack1 ? stack1.call(depth0, depth0.type, options) : helperMissing.call(depth0, "ifNot", depth0.type, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "          ";
  stack2 = helpers['if'].call(depth0, depth0.showProgress, {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      <span class=\"cancel\"><button class=\"btn info\">Remove</button></span>      </span>      ";
  return buffer;
  }
function program6(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<span class=\"file-name\">";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span>";
  return buffer;
  }

function program8(depth0,data) {
  
  
  return "           <div class=\"progress\"><div style=\"width:0%;\" class=\"bar\"></div></div>          ";
  }

  buffer += "      ";
  stack1 = helpers.each.call(depth0, depth0.files, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-file-download-single'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "      ";
  stack1 = helpers['if'].call(depth0, depth0.type, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "      <span class=\"template-download fade\">      <span class=\"preview\">";
  stack1 = helpers['if'].call(depth0, depth0.thumbnail_url, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span>      <span class=\"delete\"><button class=\"btn btn-danger\" data-type=\"";
  if (stack1 = helpers.delete_type) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.delete_type; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" data-url=\"";
  if (stack1 = helpers.delete_url) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.delete_url; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">          <i class=\"icon-trash icon-white\"></i> Delete </button></span>      </span>      ";
  return buffer;
  }
function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<a href=\"";
  if (stack1 = helpers.url) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.url; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" target=\"_blank\" title=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" rel=\"gallery\" download=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"><img src=\"";
  if (stack1 = helpers.thumbnail_url) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.thumbnail_url; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"></a>";
  return buffer;
  }

  buffer += "      ";
  stack1 = helpers.each.call(depth0, depth0.files, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-textarea'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " tabindex=\"";
  if (stack1 = helpers.tabindex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.tabindex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ";
  return buffer;
  }

  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      <textarea ";
  stack1 = helpers['if'].call(depth0, depth0.tabindex, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " rows=\"";
  if (stack1 = helpers.rows) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.rows; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" id=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"";
  if (stack1 = helpers.size) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.size; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " ";
  if (stack1 = helpers.classes) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.classes; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" placeholder=\"";
  if (stack1 = helpers.placeholder) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.placeholder; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ";
  if (stack1 = helpers.attributes) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.attributes; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</textarea>      ";
  stack1 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-switch'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, self=this;


  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      <span location=\"switch\"></span>      ";
  stack1 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      <span _template_=\"_template_\" data-selector-format=\"location\" data-selector-location=\"switch\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['o-form-switch'] = {"selectorFormat":"location","selectorLocation":"switch"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-select'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "      <select id=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ";
  stack1 = helpers['if'].call(depth0, depth0.multiple, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " class=\"";
  if (stack1 = helpers.size) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.size; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ";
  if (stack1 = helpers.attributes) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.attributes; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ";
  stack1 = helpers['if'].call(depth0, depth0.tabindex, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " >      ";
  stack1 = helpers.each.call(depth0, depth0.list, {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      </select>      ";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "multiple=\"multiple\"";
  }

function program4(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " tabindex=\"";
  if (stack1 = helpers.tabindex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.tabindex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ";
  return buffer;
  }

function program6(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "      <option value=\"";
  options = {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data};
  stack2 = ((stack1 = helpers.ifExists || depth0.ifExists),stack1 ? stack1.call(depth0, depth0.value, options) : helperMissing.call(depth0, "ifExists", depth0.value, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\" ";
  stack2 = helpers['if'].call(depth0, depth0.selected, {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += ">      ";
  stack2 = helpers['if'].call(depth0, depth0.label, {hash:{},inverse:self.program(13, program13, data),fn:self.program(11, program11, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</option>      ";
  return buffer;
  }
function program7(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program9(depth0,data) {
  
  
  return "selected=\"selected\"";
  }

function program11(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.label) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.label; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program13(depth0,data) {
  
  var stack1, stack2, options;
  options = {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data};
  stack2 = ((stack1 = helpers.ifExists || depth0.ifExists),stack1 ? stack1.call(depth0, depth0.value, options) : helperMissing.call(depth0, "ifExists", depth0.value, options));
  if(stack2 || stack2 === 0) { return stack2; }
  else { return ''; }
  }

function program15(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "      ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "selectStyle", "margin-left:0px", options) : helperMissing.call(depth0, "set", "selectStyle", "margin-left:0px", options)))
    + "      <input type=\"hidden\" id=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" class=\"";
  if (stack2 = helpers.size) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.size; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" style=\"";
  if (stack2 = helpers.selectStyle) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.selectStyle; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" ";
  if (stack2 = helpers.attributes) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.attributes; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  stack2 = helpers['if'].call(depth0, depth0.tabindex, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ></input>      ";
  return buffer;
  }

function program17(depth0,data) {
  
  
  return "<br/><br/>";
  }

  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = helpers['if'].call(depth0, depth0.simple, {hash:{},inverse:self.program(15, program15, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "helpFloat", "left", options) : helperMissing.call(depth0, "set", "helpFloat", "left", options)))
    + "      ";
  stack2 = helpers['if'].call(depth0, depth0.help, {hash:{},inverse:self.noop,fn:self.program(17, program17, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-checkbox'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "      <label class=\"checkbox ";
  stack1 = helpers['if'].call(depth0, depth1.inline, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">          <input ";
  stack1 = helpers['if'].call(depth0, depth1.disabled, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " name=\""
    + escapeExpression(((stack1 = depth1.name),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" type=\"checkbox\" value=\"";
  options = {hash:{},inverse:self.program(8, program8, data),fn:self.program(6, program6, data),data:data};
  stack2 = ((stack1 = helpers.ifExists || depth0.ifExists),stack1 ? stack1.call(depth0, depth0.value, options) : helperMissing.call(depth0, "ifExists", depth0.value, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\" ";
  stack2 = helpers['if'].call(depth0, depth0.selected, {hash:{},inverse:self.noop,fn:self.program(10, program10, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  if (stack2 = helpers.attributes) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.attributes; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  stack2 = helpers['if'].call(depth0, depth0.tabindex, {hash:{},inverse:self.noop,fn:self.program(12, program12, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " />          ";
  stack2 = helpers['if'].call(depth0, depth0.label, {hash:{},inverse:self.program(16, program16, data),fn:self.program(14, program14, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      </label>      ";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "inline";
  }

function program4(depth0,data) {
  
  
  return "disabled";
  }

function program6(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program8(depth0,data) {
  
  
  return escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  }

function program10(depth0,data) {
  
  
  return "checked=\"checked\"";
  }

function program12(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " tabindex=\"";
  if (stack1 = helpers.tabindex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.tabindex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ";
  return buffer;
  }

function program14(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.label) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.label; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }

function program16(depth0,data) {
  
  var stack1, stack2, options;
  options = {hash:{},inverse:self.program(8, program8, data),fn:self.program(6, program6, data),data:data};
  stack2 = ((stack1 = helpers.ifExists || depth0.ifExists),stack1 ? stack1.call(depth0, depth0.value, options) : helperMissing.call(depth0, "ifExists", depth0.value, options));
  if(stack2 || stack2 === 0) { return stack2; }
  else { return ''; }
  }

  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "        ";
  stack1 = helpers.each.call(depth0, depth0.list, {hash:{},inverse:self.noop,fn:self.programWithDepth(1, program1, data, depth0),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "        ";
  stack1 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-radio'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "      <label class=\"radio ";
  stack1 = helpers['if'].call(depth0, depth1.inline, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">          <input ";
  stack1 = helpers['if'].call(depth0, depth1.disabled, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " name=\""
    + escapeExpression(((stack1 = depth1.name),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" type=\"radio\" value=\"";
  options = {hash:{},inverse:self.program(8, program8, data),fn:self.program(6, program6, data),data:data};
  stack2 = ((stack1 = helpers.ifExists || depth0.ifExists),stack1 ? stack1.call(depth0, depth0.value, options) : helperMissing.call(depth0, "ifExists", depth0.value, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\" ";
  stack2 = helpers['if'].call(depth0, depth0.selected, {hash:{},inverse:self.noop,fn:self.program(10, program10, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  if (stack2 = helpers.attributes) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.attributes; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  stack2 = helpers['if'].call(depth0, depth0.tabindex, {hash:{},inverse:self.noop,fn:self.program(12, program12, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " />          ";
  stack2 = helpers['if'].call(depth0, depth0.label, {hash:{},inverse:self.program(16, program16, data),fn:self.program(14, program14, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      </label>      ";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "inline";
  }

function program4(depth0,data) {
  
  
  return "disabled";
  }

function program6(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program8(depth0,data) {
  
  
  return escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  }

function program10(depth0,data) {
  
  
  return "checked=\"checked\"";
  }

function program12(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " tabindex=\"";
  if (stack1 = helpers.tabindex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.tabindex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ";
  return buffer;
  }

function program14(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.label) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.label; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program16(depth0,data) {
  
  var stack1, stack2, options;
  options = {hash:{},inverse:self.program(8, program8, data),fn:self.program(6, program6, data),data:data};
  stack2 = ((stack1 = helpers.ifExists || depth0.ifExists),stack1 ? stack1.call(depth0, depth0.value, options) : helperMissing.call(depth0, "ifExists", depth0.value, options));
  if(stack2 || stack2 === 0) { return stack2; }
  else { return ''; }
  }

  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "        ";
  stack1 = helpers.each.call(depth0, depth0.list, {hash:{},inverse:self.noop,fn:self.programWithDepth(1, program1, data, depth0),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "        ";
  stack1 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-table-source'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  
  return "span12";
  }

function program3(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.size) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.size; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program5(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "              <button type=\"submit\" class=\"btn btn-primary add-from-source\" data-source=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\">Add From ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.camel || depth0.camel),stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "camel", depth0, options)))
    + "</button>              ";
  return buffer;
  }

  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      <div class=\"row-fluid\">          <div class=\"";
  stack1 = helpers['if'].call(depth0, depth0.embeded, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">              ";
  stack1 = helpers.each.call(depth0, depth0.sources, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          </div>          <div class=\"progress span12\" style=\"display: none\"><div style=\"width:0%;\" class=\"bar\"></div></div>          <div class=\"";
  stack1 = helpers['if'].call(depth0, depth0.embeded, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">              <span location=\"object\"></span>          </div>      </div>      ";
  stack1 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "        <span _template_=\"_template_\" data-selector-format=\"location\" data-selector-location=\"object\"></span>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['o-form-table-source'] = {"selectorFormat":"location","selectorLocation":"object"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-slider'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, stack2, options, self=this, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "      <br/>      <div class=\"span";
  if (stack1 = helpers.size) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.size; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " labels\" style=\"margin-left:0;\">          <div class=\"row-fluid\">              ";
  stack1 = helpers.each.call(depth0, depth0.labels, {hash:{},inverse:self.noop,fn:self.programWithDepth(2, program2, data, depth0),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          </div>      </div>      ";
  return buffer;
  }
function program2(depth0,data,depth1) {
  
  var buffer = "", stack1;
  buffer += "              <div class=\"span";
  if (stack1 = helpers.size) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.size; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" style=\"text-align:";
  stack1 = helpers['if'].call(depth0, depth0.last, {hash:{},inverse:self.program(5, program5, data),fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ";\"><span>";
  stack1 = helpers['if'].call(depth0, depth1.labelMarker, {hash:{},inverse:self.noop,fn:self.programWithDepth(10, program10, data, depth1),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  if (stack1 = helpers.label) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.label; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span></div>              ";
  return buffer;
  }
function program3(depth0,data) {
  
  
  return "right";
  }

function program5(depth0,data) {
  
  var stack1;
  stack1 = helpers['if'].call(depth0, depth0.first, {hash:{},inverse:self.program(8, program8, data),fn:self.program(6, program6, data),data:data});
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }
function program6(depth0,data) {
  
  
  return "left";
  }

function program8(depth0,data) {
  
  
  return "center";
  }

function program10(depth0,data,depth2) {
  
  var buffer = "", stack1, stack2;
  stack2 = ((stack1 = depth2.labelMarker),typeof stack1 === functionType ? stack1.apply(depth0) : stack1);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "<br/>";
  return buffer;
  }

function program12(depth0,data) {
  
  
  return "<br/><br/>";
  }

  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "height", 5, options) : helperMissing.call(depth0, "set", "height", 5, options)))
    + "      <div id=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" class=\"slider ";
  if (stack2 = helpers.classes) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.classes; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + " span";
  if (stack2 = helpers.size) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.size; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" style=\"min-height: ";
  if (stack2 = helpers.height) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.height; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "px;\" ";
  if (stack2 = helpers.attributes) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.attributes; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += ">      </div>      ";
  stack2 = helpers['if'].call(depth0, depth0.labels, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "helpFloat", "left", options) : helperMissing.call(depth0, "set", "helpFloat", "left", options)))
    + "      ";
  stack2 = helpers['if'].call(depth0, depth0.help, {hash:{},inverse:self.noop,fn:self.program(12, program12, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-object'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  
  return "span12";
  }

function program3(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.size) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.size; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      <div class=\"row-fluid\">          <div class=\"";
  stack1 = helpers['if'].call(depth0, depth0.embeded, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">              <span location=\"object\"></span>          </div>      </div>      ";
  stack1 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      <span _template_=\"_template_\" data-selector-format=\"location\" data-selector-location=\"object\"></span>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['o-form-object'] = {"selectorFormat":"location","selectorLocation":"object"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-address'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  
  return "span12";
  }

function program3(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.size) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.size; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "        <div class=\"";
  stack1 = helpers['if'].call(depth0, depth0.embeded, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">          <div class=\"row-fluid second-level-elements\">              <div class=\"";
  stack1 = helpers['if'].call(depth0, depth0.embeded, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">                  <div class=\"row-fluid\">                  <span class=\"span6\">  	                <span location=\"type\"/>  	                <span location=\"street\"/>  	                <span location=\"street2\"/>                    </span>                  <span class=\"span6\">                      <span location=\"city\"/>                  	<span location=\"state\"/>                      <span location=\"zip\"/>                      <span location=\"country\"/>                  </span>                  </div></div>              ";
  stack1 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              ";
  stack1 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          </div>      </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['o-form-address'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-list'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "          ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "height", "150px", options) : helperMissing.call(depth0, "set", "height", "150px", options)))
    + "          <tbody class=\"";
  stack2 = helpers['if'].call(depth0, depth0.sortable, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\" style=\"display: block; max-height: ";
  if (stack2 = helpers.height) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.height; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "; overflow-x: hidden; overflow-y: auto;\">          ";
  stack2 = helpers.each.call(depth0, depth0.list, {hash:{},inverse:self.noop,fn:self.programWithDepth(4, program4, data, depth0),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "          </tbody>          ";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "sort-list";
  }

function program4(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "          <tr data-cid=\"";
  if (stack1 = helpers.cid) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.cid; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" data-order=\"";
  if (stack1 = helpers.order) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.order; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" style=\"width:100%\"><td class=\"span12\">              ";
  stack1 = helpers['if'].call(depth0, depth1.sortable, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              <span class=\"";
  if (stack1 = helpers['list-classes']) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0['list-classes']; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" style=\"\" ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers['data-fields'] || depth0['data-fields']),stack1 ? stack1.call(depth0, depth0.data, options) : helperMissing.call(depth0, "data-fields", depth0.data, options)))
    + ">";
  if (stack2 = helpers.label) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.label; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</span>                    <span class=\"list-actions\" style=\"float:right\">                      ";
  stack2 = helpers['if'].call(depth0, depth1.edit, {hash:{},inverse:self.noop,fn:self.programWithDepth(7, program7, data, depth0),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "                      ";
  stack2 = helpers['if'].call(depth0, depth1['delete'], {hash:{},inverse:self.noop,fn:self.programWithDepth(9, program9, data, depth0),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "                  </span>          </td>          </tr>          ";
  return buffer;
  }
function program5(depth0,data) {
  
  
  return "              <span class=\"sort-list-handle\" style=\"cursor:pointer\"><i class=\"icon-move\"></i></span>              ";
  }

function program7(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2;
  buffer += "                      <a class=\"btn "
    + escapeExpression(((stack1 = ((stack1 = depth1.edit),stack1 == null || stack1 === false ? stack1 : stack1.classes)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " list-edit\" data-cid=\"";
  if (stack2 = helpers.cid) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.cid; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" title=\"Edit\">                          <i class=\"icon-edit\"></i>                      </a>                      ";
  return buffer;
  }

function program9(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2;
  buffer += "                      <a class=\"btn "
    + escapeExpression(((stack1 = ((stack1 = depth1['delete']),stack1 == null || stack1 === false ? stack1 : stack1.classes)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " list-delete\" data-cid=\"";
  if (stack2 = helpers.cid) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.cid; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" title=\"Delete\">                          <i class=\"icon-remove\"></i>                      </a>                      ";
  return buffer;
  }

function program11(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          <tfoot><tr><td>              <a class=\"btn "
    + escapeExpression(((stack1 = ((stack1 = depth0.add),stack1 == null || stack1 === false ? stack1 : stack1.classes)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " list-add\" title=\"Add\" style=\"100%\">                  <i class=\"icon-plus\"></i>              </a>            </td></tr></tfoot>          ";
  return buffer;
  }

function program13(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "          ";
  options = {hash:{},inverse:self.program(19, program19, data),fn:self.program(14, program14, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0.limit, 1, options) : helperMissing.call(depth0, "ifEq", depth0.limit, 1, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  return buffer;
  }
function program14(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          ";
  stack1 = helpers['if'].call(depth0, depth0.used, {hash:{},inverse:self.program(17, program17, data),fn:self.program(15, program15, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          ";
  return buffer;
  }
function program15(depth0,data) {
  
  
  return "          <div>to change, delete this one first</div>          ";
  }

function program17(depth0,data) {
  
  
  return "          <div></div>          ";
  }

function program19(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          ";
  stack1 = helpers['if'].call(depth0, depth0.limit, {hash:{},inverse:self.noop,fn:self.program(20, program20, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          ";
  return buffer;
  }
function program20(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          <div>";
  if (stack1 = helpers.used) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.used; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " of ";
  if (stack1 = helpers.limit) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.limit; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " used (";
  if (stack1 = helpers.available) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.available; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " remaining)</div>          ";
  return buffer;
  }

  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      <table class=\"";
  if (stack1 = helpers.size) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.size; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " table table-bordered table-striped table-condensed\" style=\"display: block;\">          ";
  stack1 = helpers['if'].call(depth0, depth0.list, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "available", true, options) : helperMissing.call(depth0, "set", "available", true, options)))
    + "          ";
  options = {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data};
  stack2 = ((stack1 = helpers.ifAll || depth0.ifAll),stack1 ? stack1.call(depth0, depth0.add, depth0.available, options) : helperMissing.call(depth0, "ifAll", depth0.add, depth0.available, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "        </table>        ";
  stack2 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      ";
  stack2 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "        ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "displayMsg", true, options) : helperMissing.call(depth0, "set", "displayMsg", true, options)))
    + "      ";
  stack2 = helpers['if'].call(depth0, depth0.displayMsg, {hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      <span class=\"listEditor\"></span><!-- where modal window will get inserted -->        ";
  stack2 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-json'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, self=this, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-label'], 'o-form-label', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-start'], 'o-form-control-start', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      <div class=\"jsoneditor\" style=\"width: ";
  if (stack1 = helpers.width) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.width; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "px; height: ";
  if (stack1 = helpers.height) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.height; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "px;\" ";
  if (stack1 = helpers.attributes) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.attributes; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "/>      ";
  stack1 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-help'], 'o-form-help', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  stack1 = self.invokePartial(partials['o-form-control-end'], 'o-form-control-end', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-table'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function", self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "          ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || depth0.set),stack1 ? stack1.call(depth0, "titleTag", "h2", options) : helperMissing.call(depth0, "set", "titleTag", "h2", options)))
    + "          <";
  if (stack2 = helpers.titleTag) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.titleTag; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + ">";
  if (stack2 = helpers.title) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.title; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "</";
  if (stack2 = helpers.titleTag) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.titleTag; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + ">      ";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              <colgroup>                  ";
  stack1 = helpers.each.call(depth0, depth0.colgroup, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              </colgroup>              ";
  return buffer;
  }
function program4(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "                     <col span=\"";
  stack1 = helpers['if'].call(depth0, depth0.span, {hash:{},inverse:self.program(7, program7, data),fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" style=\"";
  stack1 = helpers['if'].call(depth0, depth0.width, {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">                  ";
  return buffer;
  }
function program5(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.span) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.span; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program7(depth0,data) {
  
  
  return "1";
  }

function program9(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "width: ";
  if (stack1 = helpers.width) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.width; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + ";";
  return buffer;
  }

function program11(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<th>";
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</th>";
  return buffer;
  }

  buffer += "      <div class=\"row-fluid\">      ";
  stack1 = helpers['if'].call(depth0, depth0.title, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      <form class=\"";
  if (stack1 = helpers.classes) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.classes; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">          <table class=\"table ";
  if (stack1 = helpers.classes) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.classes; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">              ";
  stack1 = helpers['if'].call(depth0, depth0.colgroup, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              <thead><tr>";
  stack1 = helpers.each.call(depth0, depth0.head, {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</tr></thead>              <tbody location=\"fieldsets\"/>          </table>            ";
  stack1 = self.invokePartial(partials['o-form-buttons'], 'o-form-buttons', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      </form>      </div>      <span _template_=\"_template_\" data-selector-format=\"location\" data-selector-location=\"fieldsets\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['o-form-table'] = {"selectorFormat":"location","selectorLocation":"fieldsets"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['o-form-table-fieldset'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "              ";
  options = {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data};
  stack2 = ((stack1 = helpers.ifNotEq || depth0.ifNotEq),stack1 ? stack1.call(depth0, depth0.type, "hidden", options) : helperMissing.call(depth0, "ifNotEq", depth0.type, "hidden", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "          ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "                  <td class=\"";
  if (stack1 = helpers.classes) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.classes; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"><span location=\""
    + escapeExpression(((stack1 = ((stack1 = depth0._hbParent),stack1 == null || stack1 === false ? stack1 : stack1.cid)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "_";
  if (stack2 = helpers._hbIndex) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0._hbIndex; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"></span></td>              ";
  return buffer;
  }

  buffer += "          ";
  stack1 = helpers.each.call(depth0, depth0.fields, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      <span _template_=\"_template_\" data-selector-format=\"index\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['o-form-table-fieldset'] = {"selectorFormat":"index"};
})();
