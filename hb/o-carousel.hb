<script id="o-carousel" type="template">
   <div class="row-fluid">

       <div class="span1">
           <a href="#" class="carousel-prev" data-prev="{{cid}}_prev"><i class="icon icon-caret-left icon-4x"></i></a>
       </div>
       <div class="span10">
           <div class="carousel" style="{{style}}">
               <ul>
               {{#each items}}
                    <li style="{{style}}">{{{item}}}</li>
               {{/each}}
               {{#each _hbSubViewCount}}
                    <li class="" location="{{_hbParent.cid}}_{{_hbIndex}}"></li>
               {{/each}}
               </ul>
           </div>
       </div>
    <div class="span1">
           <a href="#" class="carousel-next" data-next="{{cid}}_next"><i class="icon icon-caret-right  icon-4x"></i></a>
       </div>
   </div>

    <span _template_="_template_" data-selector-format="index"/>
</script>
