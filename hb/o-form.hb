<script id="o-form-view" type="template">
    <form id="{{id}}" class="form-horizontal {{classes}}" style="{{style}}">
        {{#if img}}
        {{#with img}}
        <img src="{{src}}" style="border:1px solid #333; display: block; position: absolute; top: 0px; left: 20px;">
        {{/with}}
        {{/if}}
        {{#each fieldSets}}
        <fieldset>
            {{#if label}}
            <legend>{{label}}</legend>
            {{/if}}

            {{#each fields}}
            {{#ifEq type "hidden"}}
            {{else}}
            {{#if label}}
            <div class="control-group row-fuild">
                <label class="control-label ">{{label}}:</label>
                <div class="controls">
                    <span class="field-data {{valueClasses}}">{{{value}}}</span>
                </div>
            </div>
            {{/if}}
            {{/ifEq}}
            {{/each}}
        </fieldset>
        {{/each}}
    </form>
</script>
<script id="o-form-buttons" type="partial">
    {{#ifAny prevButton nextButton submitButton cancelButton moreButtons}}

    {{#ifNot disableActionBorder}}<div class="form-actions" style="margin-top: 0px; {{#if actionOffset}}margin-left: {{actionOffset}}px;{{/if}}">{{/ifNot}}
    {{set "buttonOffset" "3"}}
    {{#ifNotEq formLayout "inline"}}<div class="{{#ifNot disableButtonOffset}}offset{{buttonOffset}} span{{math "12-" buttonOffset}}{{/ifNot}}" style="{{#if disableActionBorder}}margin-top: 20px; {{/if}}">{{/ifNotEq}}
    {{#if prevButton}}
    {{set "prevButtonText" "Prev"}}
    <button {{#if prevButtonTabIndex}}tabindex="{{prevButtonTabIndex}}"{{/if}} type="button" class="btn prev {{buttonClasses}} {{prevButtonClasses}}">{{{prevButtonText}}}</button>
    {{/if}}
    {{#if nextButton}}
    {{set "nextButtonText" "Next"}}
    <button {{#if nextButtonTabIndex}}tabindex="{{nextButtonTabIndex}}"{{/if}} type="submit" class="btn btn-primary next {{buttonClasses}} {{nextButtonClasses}}">{{{nextButtonText}}}</button>
    {{/if}}
    {{#if submitButton}}
    {{set "submitButtonText" "Submit"}}
    <button {{#if submitButtonTabIndex}}tabindex="{{submitButtonTabIndex}}"{{/if}} type="submit" class="btn btn-primary submit {{buttonClasses}} {{submitButtonClasses}}">{{{submitButtonText}}}</button>
    {{/if}}
    {{#if cancelButton}}
    {{set "cancelButtonText" "Cancel"}}
    <button {{#if cancelButtonTabIndex}}tabindex="{{cancelButtonTabIndex}}"{{/if}} type="button" class="btn cancel {{buttonClasses}} {{cancelButtonClasses}}">{{{cancelButtonText}}}</button>
    {{/if}}
    {{#each moreButtons}}
    <a {{#if tabindex}}tabindex="{{tabindex}}"{{/if}} class="btn {{../buttonClasses}} {{classes}}" {{data-fields data}} href="{{link}}" target="{{target}}">{{{text}}}</a>
    {{/each}}
    {{#ifNotEq formLayout "inline"}}</div>{{/ifNotEq}}
    {{#ifNot disableActionBorder}}</div>{{/ifNot}}
    {{/ifAny}}

</script>
<script id="o-form-errors" type="partial">
    {{#if errMsg}}
    <span class="help-inline {{error-classes}}">{{{errMsg}}}</span>
    {{/if}}
</script>
<script id="o-form-help" type="partial">
    {{#if help}}
    <!-- <a href="#" class="help-toggle icon-question-sign">&nbsp;</a> -->
    <span class=" help-block blue {{help-classes}}" style="{{#if helpFloat}}float: {{helpFloat}};{{/if}}"><span class="msg">{{{help}}}</span></span>
    {{/if}}
</script>

<script id="o-form-steps" type="partial">
    {{set "navType" "tabs"}}
    {{set "menuLocation" "above"}}

    <div class="{{#ifEq navType "tabs"}}tabbable tabs-{{menuLocation}} {{/ifEq}}">
    {{#ifNot hideSteps}}

    {{#ifNotEq menuLocation "below"}}
    {{set "pillMargin" 30}}
    <ul class="nav nav-{{navType}} {{#if stacked}}nav-stacked{{/if}} {{#ifEq ../menuLocation "left"}}pull-left{{/ifEq}} {{#ifEq ../menuLocation "right"}}pull-right{{/ifEq}} " style="{{#if minWidth}}min-width: {{minWidth}}px; {{/if}}{{#ifAny ../menuLocation "left" "right"}}{{#ifEq navType "pills" }}margin-{{#ifEq ../../../menuLocation "left"}}right{{else}}left{{/ifEq}}: {{pillMargin}}px; {{/ifEq}} {{/ifAny}} {{#if hideStepsMenu}}display:none;{{/if}}">
    {{#each steps}}

    <li class="{{#ifEq _hbKey _hbParent.activeStep}}active{{/ifEq}}"><a href="#" class="form-step" data-step="{{_hbKey}}" data-toggle="tab">{{#if label}}{{{label}}}{{else}}{{value}}{{/if}}</a></li>
    {{/each}}

    </ul>
    {{/ifNotEq}}

    {{/ifNot}}
    <div class="{{#ifNot hideSteps}}tab-content{{/ifNot}}" style="{{#if minHeight}}min-height:{{minHeight}}px;{{/if}}">
        {{#each steps}}
        <div class="tab-pane {{#ifEq _hbKey _hbParent.activeStep}}active{{/ifEq}}" data-step="{{_hbKey}}">
            <div class="{{fieldSetSize}}" location="{{_hbKey}}" {{#ifEq formLayout "inline"}}style="display:inline"{{/ifEq}}></div>
    </div>
    {{/each}}
    </div>
    {{#ifNot hideSteps}}
    {{#ifEq menuLocation "below"}}
    <ul class="nav nav-{{navType}} {{#if stacked}}nav-stacked{{/if}} {{#ifEq ../menuLocation "left"}}pull-left{{/ifEq}} {{#ifEq ../menuLocation "right"}}pull-right{{/ifEq}} " style="{{#if minWidth}}min-width: {{minWidth}}px; {{/if}}{{#ifAny ../menuLocation "left" "right"}}{{#ifEq navType "pills" }}margin-{{#ifEq ../../../menuLocation "left"}}right{{else}}left{{/ifEq}}: 30px; {{/ifEq}} {{/ifAny}}">
    {{#each steps}}
    <li class="{{#ifEq _hbKey _hbParent.activeStep}}active{{/ifEq}}"><a href="#" class="form-step" data-step="{{_hbKey}}" data-toggle="tab">{{#if label}}label{{else}}{{value}}{{/if}}</a></li>
    {{/each}}

    </ul>
    {{/ifEq}}
    {{/ifNot}}

    </div>
</script>
<script id="o-form" type="template">
    <div class="row-fluid">
        <div class="{{#if formSize}}{{formSize}}{{else}}span12{{/if}}" style="{{style}}">
            <form id="{{id}}" class="{{#ifAny formLayout formLayoutEmbeded}}form-{{#ifAll embeded formLayoutEmbeded}}{{formLayoutEmbeded}}{{else}}{{formLayout}}{{/ifAll}}{{/ifAny}} {{formClasses}}" style="{{formStyle}} {{#ifEq formLayout "inline"}}margin: 0;{{/ifEq}}">
            {{#if title}}
            <h2>{{title}}</h2>
            {{/if}}
            {{#if steps}}
            {{> o-form-steps}}
            {{else}}
            {{#if fieldSets}}
            <div class="{{fieldSetSize}}" location="defaultStep" {{#ifEq formLayout "inline"}}style="display:inline"{{/ifEq}}></div>
        {{/if}}
        {{/if}}
        {{> o-form-buttons}}
        </form>
    </div>
    </div>
    <span _template_="_template_" data-selector-format="field" data-selector-field="step" data-selector-default-value="defaultStep"/>

</script>
<script id="o-form-fieldset" type="template">
    {{#if label}}<legend>{{label}}</legend>{{/if}}
    {{set "layoutCols" 1}}
    {{set "layoutOrder" "down"}}
    {{#ifNotEq layoutCols "1"}}<div class="row-fluid">{{/ifNotEq}}
    {{#each layoutCols}}
    {{#ifNotEq ../layoutCols "1"}}<div class="span{{math "12/" _hbCount}}">{{/ifNotEq}}
    {{#each ../fields}}
    {{#ifCol}}
    <span location="{{_hbKey}}"/>
    {{/ifCol}}
    {{/each}}
    {{#ifNotEq ../layoutCols "1"}}</div>{{/ifNotEq}}
    {{/each}}
    {{#ifNotEq layoutCols "1"}}</div>{{/ifNotEq}}
    <span _template_="_template_" data-selector-format="name"/>
</script>
<script id="o-form-label" type="partial">
    {{#if label}}<label for="{{name}}" class=" control-label  {{label-classes}}">{{#if required}}* {{/if}}{{{label}}}</label>{{/if}}
</script>
<script id="o-form-hidden" type="template">
    <input type="hidden" id="{{name}}" class="{{classes}}" value="{{value}}" {{#if tabindex}} tabindex="{{tabindex}}" {{/if}} />
</script>
<script id="o-form-control-start" type="partial">
    {{#ifEq formLayout "horizontal"}}{{#ifNotEq fieldLayout "inline"}}<div class=" controls  {{control-classes}} ">{{/ifNotEq}}{{/ifEq}}
</script>
<script id="o-form-control-end" type="partial">
    {{#ifEq formLayout "horizontal"}}{{#ifNotEq fieldLayout "inline"}}</div>{{/ifNotEq}}{{/ifEq}}
</script>

<script id="o-form-input" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}
    {{#ifAny before after}}<div class="{{size}} {{inputClasses}} {{#if before}}input-prepend{{/if}} {{#if after}}input-append{{/if}}">{{/ifAny}}
    {{#if before}}<span class="add-on">{{{before}}}</span>{{/if}}<input type="{{inputType}}" id="{{name}}" class="{{prependSize}} {{classes}}" value="{{value}}" placeholder="{{placeholder}}" {{{attributes}}} {{#if tabindex}} tabindex="{{tabindex}}" {{/if}} />{{#if after}}<span class="add-on">{{{after}}}</span>{{/if}}
    {{#ifAny before after}}</div>{{/ifAny}}
    {{> o-form-errors}}
    {{> o-form-help}}
    {{> o-form-control-end}}
</script>

<script id="o-form-encrypted" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}
    <div class="input-prepend {{inputClasses}} {{#if after}}input-append{{/if}} {{size}}">
        {{#if lock}}
        <span class="add-on"><i class="icon-lock"></i></span><input type="button" id="{{name}}" class="nostyle {{#if after}}span10{{else}}span11{{/if}} {{classes}}" value="Click to Change" placeholder="{{placeholder}}" {{{attributes}}} />{{#if after}}<span class="add-on">{{after}}</span>{{/if}}
        {{else}}
        <span class="add-on"><i class="icon-key"></i></span><input type="password" id="{{name}}" class="{{#if after}}span10{{else}}span11{{/if}} {{classes}}" value="" placeholder="{{placeholder}}" {{{attributes}}} {{#if tabindex}} tabindex="{{tabindex}}" {{/if}} />{{#if after}}<span class="add-on">{{after}}</span>{{/if}}
        {{/if}}
    </div>
    {{#if showable}}
    &nbsp;&nbsp;<a {{#if value}}data-title="{{name}}" data-text="{{value}}"{{else}}data-server="{{showUrl}}"{{/if}} href="#modal">Show</a>
    {{/if}}

    {{> o-form-errors}}
    {{> o-form-help}}
    {{> o-form-control-end}}
</script>
<script id="o-form-raw" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}
    <div class="raw" style="padding-top:5px; {{#ifEq formLayout "inline"}}display:inline{{/ifEq}}">{{{html}}}</div>
    {{> o-form-errors}}
    {{> o-form-help}}
    {{> o-form-control-end}}
</script>
<script id="o-form-customraw" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}
    {{{html}}}
    {{{customExtra}}}
    {{> o-form-errors}}
    {{> o-form-help}}
    {{> o-form-control-end}}
</script>
<script id="o-form-daterange" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}

    <div class="{{size}} date {{#ifAny before before2}}input-prepend{{/ifAny}} input-append">
        {{#if before}}<span class="add-on">{{before}}</span>{{/if}}<input type="{{inputType}}" id="{{name}}" class="{{#if before}}span4{{else}}span5{{/if}} {{classes}}" value="{{value.start}}" placeholder="{{placeholder.start}}" ><span class="add-on"><i class="icon icon-th"></i></span>
        {{#if before2}}<span class="add-on">{{before2}}</span>{{/if}}<input type="{{inputType}}" id="{{name}}2" class="{{#if before2}}span4{{else}}span5{{/if}}  {{classes}}" value="{{value.end}}" placeholder="{{placeholder.end}}" ><span class="add-on"><i class="icon icon-th"></i></span>
    </div>
    {{> o-form-errors}}
    {{> o-form-help}}
    {{> o-form-control-end}}
</script>
<script id="o-form-media-display" type="template">
    <img src="/_core/media/media:preview/{{value}}/small"/>
</script>
<script id="o-form-media" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}
    {{#if value}}
    {{#if mediaPreview}}
    {{{mediaPreview}}}
    {{else}}
    <img src="/_core/media/media:preview/{{value}}/small"/>
    {{/if}}
    {{else}}
    <!-- {{{emptyPreview}}} -->
    {{/if}}
    <div class="btn-group btn-group-vertical">
        {{set "buttonIcon" "picture"}}
        <button class="btn load-media" type="button"><i class="icon-{{buttonIcon}}"></i></button>
        {{#if value}}
        <button class="btn remove-media" type="button"><i class="icon-trash"></i></button>
        {{/if}}
    </div>


    {{> o-form-errors}}
    {{> o-form-help}}
    {{> o-form-control-end}}
</script>
<script id="o-form-file-basic" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}

    {{#if before}}<span class="add-on">{{before}}</span>{{/if}}<input type="{{type}}" id="{{name}}" class="input-file input-{{size}} {{classes}}" value="{{value}}" placeholder="{{placeholder}}" >{{#if after}}<span class="add-on">{{after}}</span>{{/if}}
    {{> o-form-errors}}
    {{> o-form-help}}
    {{> o-form-control-end}}

</script>
<script id="o-form-file" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}
    <span class="files" />
                <span class="btn btn-success fileinput-button">
                    <span><i class="icon-plus icon-white"></i> {{#if uploadLabel}}{{uploadLabel}}{{else}}Upload {{label}}...{{/if}}</span>
                    {{#ifAny before after}}<div class="{{size}} {{inputClasses}} {{#if before}}input-prepend{{/if}} {{#if after}}input-append{{/if}}">{{/ifAny}}
                    {{#if before}}<span class="add-on">{{before}}</span>{{/if}}
                    <input type="{{type}}" id="{{name}}" name="{{name}}{{#if multiple}}[]{{/if}}" {{#if multiple}}multiple{{/if}} class="nostyle input-file {{prependSize}} {{classes}}" value="{{value}}" placeholder="{{placeholder}}" {{{attributes}}} >
                    {{#if after}}<span class="add-on">{{after}}</span>{{/if}}
                    {{#ifAny before after}}</div>{{/ifAny}}
                </span>
    {{> o-form-errors}}
    {{> o-form-help}}
    <!-- The loading indicator is shown during image processing -->
    <div class="fileupload-loading"></div>
    <br>

    {{> o-form-control-end}}
</script>
<script id="o-form-file-upload-single" type="template">
    {{#each files}}
    {{#if error}}
    <span class="file-error">{{{error}}}</span>
    {{else}}
    {{#ifAny type name}}
    <span class="template-upload">
    <span class="preview"><span class="fade"></span></span>
        {{#ifNot type}}<span class="file-name">{{name}}</span>{{/ifNot}}
        {{#if showProgress}}
         <div class="progress"><div style="width:0%;" class="bar"></div></div>
        {{/if}}
    <span class="cancel"><button class="btn info">Remove</button></span>
    </span>
    {{/ifAny}}
    {{/if}}
    {{/each}}
</script>
<script id="o-form-file-download-single" type="template">
    {{#each files}}
    {{#if type}}
    <span class="template-download fade">
    <span class="preview">{{#if thumbnail_url}}<a href="{{url}}" target="_blank" title="{{name}}" rel="gallery" download="{{name}}"><img src="{{thumbnail_url}}"></a>{{/if}}</span>
    <span class="delete"><button class="btn btn-danger" data-type="{{delete_type}}" data-url="{{delete_url}}">
        <i class="icon-trash icon-white"></i> Delete </button></span>
    </span>
    {{/if}}
    {{/each}}
</script>

<script id="o-form-textarea" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}
    <textarea {{#if tabindex}} tabindex="{{tabindex}}" {{/if}} rows="{{rows}}" id="{{name}}" class="{{size}} {{classes}}" placeholder="{{placeholder}}" {{{attributes}}}>{{{value}}}</textarea>
    {{> o-form-errors}}
    {{> o-form-help}}
    {{> o-form-control-end}}
</script>
<script id="o-form-switch" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}
    <span location="switch"></span>
    {{> o-form-errors}}
    {{> o-form-help}}
    {{> o-form-control-end}}
    <span _template_="_template_" data-selector-format="location" data-selector-location="switch"/>
</script>
<script id="o-form-select" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}
    {{#if simple}}
    <select id="{{name}}" {{#if multiple}}multiple="multiple"{{/if}} class="{{size}}" {{{attributes}}} {{#if tabindex}} tabindex="{{tabindex}}" {{/if}} >
    {{#each list}}
    <option value="{{#ifExists value}}{{value}}{{/ifExists}}" {{#if selected}}selected="selected"{{/if}}>
    {{#if label}}{{label}}{{else}}{{#ifExists value}}{{value}}{{/ifExists}}{{/if}}</option>
    {{/each}}
    </select>
    {{else}}
    {{set "selectStyle" "margin-left:0px"}}
    <input type="hidden" id="{{name}}" class="{{size}}" style="{{selectStyle}}" {{{attributes}}} {{#if tabindex}} tabindex="{{tabindex}}" {{/if}} ></input>
    {{/if}}
    {{> o-form-errors}}
    {{set "helpFloat" "left"}}
    {{#if help}}<br/><br/>{{/if}}
    {{> o-form-help}}
    {{> o-form-control-end}}
</script>
<script id="o-form-checkbox" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}

    {{#each list}}
    <label class="checkbox {{#if ../inline}}inline{{/if}}">
        <input {{#if ../disabled}}disabled{{/if}} name="{{../name}}" type="checkbox" value="{{#ifExists value}}{{value}}{{else}}{{this}}{{/ifExists}}" {{#if selected}}checked="checked"{{/if}} {{{attributes}}} {{#if tabindex}} tabindex="{{tabindex}}" {{/if}} />
        {{#if label}}{{{label}}}{{else}}{{#ifExists value}}{{value}}{{else}}{{this}}{{/ifExists}}{{/if}}
    </label>
    {{/each}}

    {{> o-form-errors}}
    {{> o-form-help}}
    {{> o-form-control-end}}
</script>
<script id="o-form-radio" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}

    {{#each list}}
    <label class="radio {{#if ../inline}}inline{{/if}}">
        <input {{#if ../disabled}}disabled{{/if}} name="{{../name}}" type="radio" value="{{#ifExists value}}{{value}}{{else}}{{this}}{{/ifExists}}" {{#if selected}}checked="checked"{{/if}} {{{attributes}}} {{#if tabindex}} tabindex="{{tabindex}}" {{/if}} />
        {{#if label}}{{label}}{{else}}{{#ifExists value}}{{value}}{{else}}{{this}}{{/ifExists}}{{/if}}
    </label>
    {{/each}}

    {{> o-form-errors}}
    {{> o-form-help}}
    {{> o-form-control-end}}
</script>
<script id="o-form-table-source" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}
    <div class="row-fluid">
        <div class="{{#if embeded}}span12{{else}}{{size}}{{/if}}">
            {{#each sources}}
            <button type="submit" class="btn btn-primary add-from-source" data-source="{{this}}">Add From {{camel this}}</button>
            {{/each}}
        </div>
        <div class="progress span12" style="display: none"><div style="width:0%;" class="bar"></div></div>
        <div class="{{#if embeded}}span12{{else}}{{size}}{{/if}}">
            <span location="object"></span>
        </div>
    </div>
    {{> o-form-errors}}
    {{> o-form-help}}
    {{> o-form-control-end}}

    <span _template_="_template_" data-selector-format="location" data-selector-location="object"></span>
</script>
<script id="o-form-slider" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}
    {{set "height" 5}}
    <div id="{{name}}" class="slider {{classes}} span{{size}}" style="min-height: {{height}}px;" {{{attributes}}}>
    </div>
    {{#if labels}}
    <br/>
    <div class="span{{size}} labels" style="margin-left:0;">
        <div class="row-fluid">
            {{#each labels}}
            <div class="span{{size}}" style="text-align:{{#if last}}right{{else}}{{#if first}}left{{else}}center{{/if}}{{/if}};"><span>{{#if ../labelMarker}}{{{../../labelMarker}}}<br/>{{/if}}{{{label}}}</span></div>
            {{/each}}
        </div>
    </div>
    {{/if}}
    {{> o-form-errors}}
    {{set "helpFloat" "left"}}
    {{#if help}}<br/><br/>{{/if}}
    {{> o-form-help}}
    {{> o-form-control-end}}
</script>

<script id="o-form-object" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}
    <div class="row-fluid">
        <div class="{{#if embeded}}span12{{else}}{{size}}{{/if}}">
            <span location="object"></span>
        </div>
    </div>
    {{> o-form-errors}}
    {{> o-form-help}}
    {{> o-form-control-end}}
    <span _template_="_template_" data-selector-format="location" data-selector-location="object"></span>
</script>
<script id="o-form-address" type="template">
    {{> o-form-label}}

    <div class="{{#if embeded}}span12{{/if}}">
        <div class="row-fluid second-level-elements">
            <div class="{{#if embeded}}span12{{else}}{{size}}{{/if}}">
                <div class="row-fluid">
                <span class="span6">
	                <span location="type"/>
	                <span location="street"/>
	                <span location="street2"/>

                </span>
                <span class="span6">
                    <span location="city"/>
                	<span location="state"/>
                    <span location="zip"/>
                    <span location="country"/>
                </span>
                </div></div>
            {{> o-form-errors}}
            {{> o-form-help}}
        </div>
    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>
<script id="o-form-list" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}
    <table class="{{size}} table table-bordered table-striped table-condensed" style="display: block;">
        {{#if list}}
        {{set "height" "150px"}}
        <tbody class="{{#if sortable}}sort-list{{/if}}" style="display: block; max-height: {{height}}; overflow-x: hidden; overflow-y: auto;">
        {{#each list}}
        <tr data-cid="{{cid}}" data-order="{{order}}" style="width:100%"><td class="span12">
            {{#if ../sortable}}
            <span class="sort-list-handle" style="cursor:pointer"><i class="icon-move"></i></span>
            {{/if}}
            <span class="{{list-classes}}" style="" {{data-fields data}}>{{{label}}}</span>

                <span class="list-actions" style="float:right">
                    {{#if ../edit}}
                    <a class="btn {{../edit.classes}} list-edit" data-cid="{{cid}}" title="Edit">
                        <i class="icon-edit"></i>
                    </a>
                    {{/if}}
                    {{#if ../delete}}
                    <a class="btn {{../delete.classes}} list-delete" data-cid="{{cid}}" title="Delete">
                        <i class="icon-remove"></i>
                    </a>
                    {{/if}}
                </span>
        </td>
        </tr>
        {{/each}}
        </tbody>
        {{/if}}
        {{set "available" true}}
        {{#ifAll add available}}
        <tfoot><tr><td>
            <a class="btn {{add.classes}} list-add" title="Add" style="100%">
                <i class="icon-plus"></i>
            </a>

        </td></tr></tfoot>
        {{/ifAll}}

    </table>

    {{> o-form-errors}}
    {{> o-form-help}}

    {{set "displayMsg" true}}
    {{#if displayMsg}}
        {{#ifEq limit 1}}
        {{#if used}}
        <div>to change, delete this one first</div>
        {{else}}
        <div></div>
        {{/if}}
        {{else}}
        {{#if limit}}
        <div>{{used}} of {{limit}} used ({{available}} remaining)</div>
        {{/if}}
        {{/ifEq}}
    {{/if}}
    <span class="listEditor"></span><!-- where modal window will get inserted -->

    {{> o-form-control-end}}
</script>
<script id="o-form-json" type="template">
    {{> o-form-label}}
    {{> o-form-control-start}}
    <div class="jsoneditor" style="width: {{width}}px; height: {{height}}px;" {{{attributes}}}/>
    {{> o-form-errors}}
    {{> o-form-help}}
    {{> o-form-control-end}}
</script>
<script type="template" id="o-form-table">
    <div class="row-fluid">
    {{#if title}}
        {{set "titleTag" "h2"}}
        <{{titleTag}}>{{title}}</{{titleTag}}>
    {{/if}}
    <form class="{{classes}}">
        <table class="table {{classes}}">
            {{#if colgroup}}
            <colgroup>
                {{#each colgroup}}
                   <col span="{{#if span}}{{span}}{{else}}1{{/if}}" style="{{#if width}}width: {{width}};{{/if}}">
                {{/each}}
            </colgroup>
            {{/if}}
            <thead><tr>{{#each head}}<th>{{{value}}}</th>{{/each}}</tr></thead>
            <tbody location="fieldsets"/>
        </table>

        {{> o-form-buttons}}
    </form>
    </div>
    <span _template_="_template_" data-selector-format="location" data-selector-location="fieldsets"/>
</script>
<script type="template" id="o-form-table-fieldset">
        {{#each fields}}
            {{#ifNotEq type "hidden"}}
                <td class="{{classes}}"><span location="{{_hbParent.cid}}_{{_hbIndex}}"></span></td>
            {{/ifNotEq}}
        {{/each}}
    <span _template_="_template_" data-selector-format="index"/>
</script>