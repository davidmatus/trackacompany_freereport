// Calling Wizard
$(function (){
    $("#wizard").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        labels: {
            next: "Next" + ('<i class="fa fa-arrow-right"></i>'),
            previous: ('<i class="fa fa-arrow-left"></i>') + "Previous"
        }
    });
});

$(function (){
    $("#wizard-vertical").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        labels: {
            next: "Next" + ('<i class="fa fa-arrow-right"></i>'),
            previous: ('<i class="fa fa-arrow-left"></i>') + "Previous"
        }
    });
});


// Calling NiceScroll
$(document).ready(function() {
    //$('html').niceScroll({
    //  cursorcolor:"#839DAC",
    //  cursorborderradius:"3px",
    //  cursorwidth: "6px",
    //  scrollspeed: "40"
    //});
    $(".scroll").niceScroll({
      cursorcolor:"#C5D1D8",
      cursorborderradius:"3px",
      cursorwidth: "6px",
      scrollspeed: "40"
    });




});

      

