<script type="template" id="tac-profile-main">
    <div class="tab-pane billing-tab-content" style="min-height:200px">
        <h4>
            <span>{{{userName}}}  <small>{{{changeName}}}</small></span>
            <small>Name</small>
        </h4>
        <h4>
            <span>{{{email}}}  <small>{{{changeEmail}}}</small></span>
            <small>Email</small>
        </h4>
        <h4><span>{{{changePassword}}}</span></h4>
    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>
<script type="template" id="tac-profile-plan">
    <div class="tab-pane billing-tab-content" style="min-height:200px">
      {{#if plan}}
        <h3>Plan Details</h3>
        <div class="row-fluid">
          <div class="span4"><h4>Plan: {{{plan}}} {{{upgrade}}}</h4>{{{base}}}</div>
          <div class="span4"><h4>Companies Tracked: {{{totalCompanies}}}</h4></div>
          <div class="span4"><h4>{{{newCompany}}}</h4></div>
        </div>
        <span location="list"/>
        <span location="summary"/>
       {{else}}
        {{{msg}}}
        <h3>{{{upgrade}}}</h3>
      {{/if}}

    </div>
<span _template_="_template_" data-selector-format="name"/>
</script>
<script type="template" id="tac-profile-history">
    <div class="tab-pane billing-tab-content" style="min-height:200px">
        <span location="history"/>
    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>

<script type="template" id="tac-profile-contact">
    <div class="tab-pane billing-tab-content" style="overflow:hidden; ">
        <span location="contactForm"/>
    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>
<script id="tac-profile-sources" type="template">
    <ul class="selection">
        {{#each list}}
        <li>
            <label>
                <input name="{{../name}}" {{#if ../disabled}}disabled{{/if}} type="checkbox" value="{{value}}" {{#if selected}}checked="checked"{{/if}} {{{attributes}}} {{#if tabindex}} tabindex="{{tabindex}}" {{/if}} />
                <img src="{{icon}}">
                {{{label}}}
            </label>
        </li>
        {{/each}}
    </ul>
    <span _template_="_template_" data-selector-format="name"/>
</script>
