<script id="tac-search-results" type="template">
    <div class="container" style="min-height:400px">

          <section class="search_company_results span12">
            <article>
              <span location="search"/>
            </article>
          </section>
        <section class="results span12">
        {{#if numCompanies}}
         <h2>{{#if resultsMsg}}{{{resultsMsg}}}{{else}}{{numCompanies}} result{{#ifNotEq numCompanies 1}}s{{/ifNotEq}} for "{{companyName}}" on {{source}}{{/if}}</h2>
            <span location="results"/>

        {{else}}

            <h2>{{#if resultsMsg}}
                    {{{resultsMsg}}}
                {{else}}
                    {{#if companyName}}
                        No results for "{{companyName}}" on {{source}}. Please search again and/or try a different source.
                    {{else}}

                    {{/if}}
                {{/if}}
            </h2>
        {{/if}}
    </section>
    </div>


    <span _template_="_template_" data-selector-format="name"/>
</script>
<script type="template" id="tac-search-fs">
    <span location="company"/>
    <span location="source"/>
   <span _template_="_template_" data-selector-format="name"/>
</script>
<script type="template" id="tac-search-field-company">
    <input name="company" class="span11" type="text" value="{{value}}" placeholder="{{placeholder}}">
    <button class="btn btn-lg submit" style="float:right">search</button>
</script>
<script type="template" id="tac-search-field-source">
    <h4>Source:</h4>
    <div class="source_options">
        {{#each list}}
        <label class="radio">
          <input type="radio" name="source" id="{{value}}" value="{{value}}" {{#if selected}}checked{{/if}}>
          <img src="/tac/img/{{value}}.png">
        </label>
        {{/each}}
      </div>
</script>

<script type="template" id="tac-company-details">
    <div class="search-wrapper">
       <div class="container">
        <span location="alert"/>
       	<div class="company-header">
            <div class="row-fluid">
                <div class="span6">
                    <div class="company-title">
                    <span>{{{logo}}}</span>
                    <h2>{{{companyName}}}</h2>
                    </div>
                </div>
                <div class="span3">
                    {{{trackButton}}}
                </div>
                <div class="span3">
                <span location="selectCompany"/>
                {{#if newLink}}
                    <a href="/">Track a new company</a>
                {{/if}}
                </div>
            </div>
        </div>
        <div class="row-fluid">
          <div class="span3">
            <span location="feed"/>
            <span location="list"/>
          </div>
          <div class="span9">
              <span location="view"/>
          </div>
        </div>
       </div>
    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>
<script type="template" id="tac-company-source">
    <div class="content right-content track-content">
        <div class="track-content-title">
        <h3>{{{sourceName}}}</h3>
        {{{buttons}}}
        </div>
        <span location="items"/>
        <!--<div class="track-alert">-->
        <!--<h5>Not Linked Yet!</h5>-->
        <!--<h6><a href="#">Link or match the company with the right source</a></h6>-->
        <!--</div>-->
    </div>
    <span _template_="_template_" data-selector-format="location" data-selector-location="items"/>
</script>
<script type="template" id="tac-plans">
    <div class="pricing-wrapper" style="padding:0;">
      <div class="container">
        <div class="pricing pricing-left">
          <h3>{{{leftTitle}}}</h3>
          <h2>{{{leftPrice}}}</h2>
          <ul>
              {{#each leftFeatures}}
                <li>{{{this}}}</li>
              {{/each}}
          </ul>
          {{#if leftAction}}
          <a href="#refresh" data-server="{{leftAction}}" class="btn btn-org btn-signup">{{{leftSelect}}}</a>
          {{/if}}
        </div>
        <div class="pricing pricing-middle">
            <h3>{{{middleTitle}}}</h3>
            <h2>{{{middlePrice}}}</h2>
            <ul>
                {{#each middleFeatures}}
                  <li>{{{this}}}</li>
                {{/each}}
            </ul>
            {{#if middleAction}}
            <a href="#refresh" data-server="{{middleAction}}" class="btn btn-grn btn-buy">{{{middleSelect}}}</a>
            {{/if}}
        </div>
        <div class="pricing pricing-right">
            <h3>{{{rightTitle}}}</h3>
            <h2>{{{rightPrice}}}</h2>
            <ul>
                {{#each rightFeatures}}
                  <li>{{{this}}}</li>
                {{/each}}
            </ul>
            {{#if rightAction}}
            <a href="#refresh" data-server="{{rightAction}}" class="btn btn-org btn-signup">{{{rightSelect}}}</a>
            {{/if}}

        </div>
      </div>
    </div>

</script>

<script id="tac-search-results-page" type="template">
    <ul class="list">
        {{#each results}}
          <li>
            <header>
              <h4>{{name}}</h4>
              {{{select}}}
            </header>
            {{#if img}}<img src="{{img}}">{{/if}}
            <span>
              {{#if url}}<a href="{{url}}" target='_blank'>{{url}}</a>{{/if}}
              <p>{{location}}</p>
            </span>
            <p>{{{description}}}</p>
          </li>
       {{/each}}
    </ul>
</script>
<script id="tac-dashboard" type="template">
    <div class="container">
    <section class="companies span12">
        <div class="row-fluid">
        <span class="span12" location="list"/>
        </div>
    </section>
    </div>
    <span _template_="_template_" data-selector-format="location" data-selector-location="list"/>
</script>
<script id="tac-dashboard-page" type="template">
    <ul class="companies-list">
        {{#each companies}}
            <li>
            <a href="{{link}}">
              <h5>{{name}}</h5>
              <div class="logo-holder">
                <img src="{{logo}}">
              </div>
              <p> {{{competitors}}}</p>
            </a>
          </li>
       {{/each}}
    </ul>
    {{{addCompany}}}
</script>

<script type="template" id="tac-profile-cc">
    <div class="tab-pane billing-tab-content" style="min-height:200px">
      {{#if cc}}
        <h3>Current Card</h3>
        <h4> <span>{{{nameOnCard}}}</span><small>Name on Card</small> </h4>
        <h4> <span>{{{cc}}}</span><small>Card No</small> </h4>
        <h4> <span>{{{expires}}}</span><small>Expires</small></h4>
       {{else}}
        {{{msg}}}
      {{/if}}
        <h3>{{{updateCard}}}</h3>
    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>
<script type="template" id="tac-profile-plans">
    <div class="pricing">
        {{#each plans}}
        <div class="plan">
            <header>
                {{{name}}}
            </header>
            <div class="orange">
                <p>starts at</p>
                <h3>${{{monthly}}}/month</h3>
            </div>
            <article>
                <h4>Track {{companies}} {{#ifEq companies 1}}company{{else}}companies{{/ifEq}}</h4>
                <h5>+ competitors</h5>
                <p>{{{users}}}</p>
                <p>{{{additional}}}</p>
            </article>
            {{{choose}}}
        </div>
        {{/each}}
    </div>
</script>
