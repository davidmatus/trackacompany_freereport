(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-profile-main'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function";


  buffer += "      <div class=\"tab-pane billing-tab-content\" style=\"min-height:200px\">          <h4>              <span>";
  if (stack1 = helpers.userName) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.userName; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  <small>";
  if (stack1 = helpers.changeName) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.changeName; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</small></span>              <small>Name</small>          </h4>          <h4>              <span>";
  if (stack1 = helpers.email) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.email; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  <small>";
  if (stack1 = helpers.changeEmail) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.changeEmail; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</small></span>              <small>Email</small>          </h4>          <h4><span>";
  if (stack1 = helpers.changePassword) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.changePassword; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span></h4>      </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-profile-main'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-profile-plan'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          <h3>Plan Details</h3>          <div class=\"row-fluid\">            <div class=\"span4\"><h4>Plan: ";
  if (stack1 = helpers.plan) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.plan; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ";
  if (stack1 = helpers.upgrade) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.upgrade; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h4>";
  if (stack1 = helpers.base) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.base; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>            <div class=\"span4\"><h4>Companies Tracked: ";
  if (stack1 = helpers.totalCompanies) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.totalCompanies; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h4></div>            <div class=\"span4\"><h4>";
  if (stack1 = helpers.newCompany) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.newCompany; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h4></div>          </div>          <span location=\"list\"/>          <span location=\"summary\"/>         ";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          ";
  if (stack1 = helpers.msg) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.msg; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          <h3>";
  if (stack1 = helpers.upgrade) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.upgrade; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>        ";
  return buffer;
  }

  buffer += "      <div class=\"tab-pane billing-tab-content\" style=\"min-height:200px\">        ";
  stack1 = helpers['if'].call(depth0, depth0.plan, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "        </div>  <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-profile-plan'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-profile-history'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "      <div class=\"tab-pane billing-tab-content\" style=\"min-height:200px\">          <span location=\"history\"/>      </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-profile-history'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-profile-contact'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "      <div class=\"tab-pane billing-tab-content\" style=\"overflow:hidden; \">          <span location=\"contactForm\"/>      </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-profile-contact'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-profile-sources'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2;
  buffer += "          <li>              <label>                  <input name=\""
    + escapeExpression(((stack1 = depth1.name),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" ";
  stack2 = helpers['if'].call(depth0, depth1.disabled, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " type=\"checkbox\" value=\"";
  if (stack2 = helpers.value) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.value; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" ";
  stack2 = helpers['if'].call(depth0, depth0.selected, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  if (stack2 = helpers.attributes) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.attributes; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  stack2 = helpers['if'].call(depth0, depth0.tabindex, {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " />                  <img src=\"";
  if (stack2 = helpers.icon) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.icon; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">                  ";
  if (stack2 = helpers.label) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.label; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "              </label>          </li>          ";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "disabled";
  }

function program4(depth0,data) {
  
  
  return "checked=\"checked\"";
  }

function program6(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " tabindex=\"";
  if (stack1 = helpers.tabindex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.tabindex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ";
  return buffer;
  }

  buffer += "      <ul class=\"selection\">          ";
  stack1 = helpers.each.call(depth0, depth0.list, {hash:{},inverse:self.noop,fn:self.programWithDepth(1, program1, data, depth0),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      </ul>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-profile-sources'] = {"selectorFormat":"name"};
})();
