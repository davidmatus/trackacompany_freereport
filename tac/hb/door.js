(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-login'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  
  return "Or use your email";
  }

function program3(depth0,data) {
  
  
  return "Login";
  }

function program5(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "?redirect=";
  if (stack1 = helpers.redirect) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.redirect; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1);
  return buffer;
  }

  buffer += "      <div class=\"signup-login\" style=\"margin-top: 0\">          ";
  stack1 = self.invokePartial(partials['tac-socials'], 'tac-socials', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          <div class=\"row-fluid\">              <h3>";
  stack1 = helpers['if'].call(depth0, depth0.socials, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <div class=\"span3 first\">              </div>              <span location=\"login\"/>          </div>          <div class=\"row-fluid\">              <div class=\"span3 offset3\" ><a href=\"/forgot\" style=\"float:right\"> Forgot Password?</a></div>              <div class=\"span3\" ><a href=\"/register";
  stack1 = helpers['if'].call(depth0, depth0.redirect, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" style=\"float:right\">Not Registered?</a></div>          </div>        </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-login'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-door-form'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "      <form>          <span location=\"fs\"/>      </form>      <span _template_=\"_template_\" data-selector-format=\"location\" data-selector-location=\"fs\"/>  ";
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-door-form'] = {"selectorFormat":"location","selectorLocation":"fs"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-login-input'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " tabindex=\"";
  if (stack1 = helpers.tabindex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.tabindex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ";
  return buffer;
  }

  buffer += "      <input name=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" type=\"";
  if (stack1 = helpers.inputType) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.inputType; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" placeholder=\"";
  if (stack1 = helpers.placeholder) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.placeholder; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"span12\" ";
  stack1 = helpers['if'].call(depth0, depth0.tabindex, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " value=\"";
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" >          ";
  stack1 = self.invokePartial(partials['o-form-errors'], 'o-form-errors', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-door-form-fs'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "tabindex=\"";
  if (stack1 = helpers.submitButtonTabIndex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.submitButtonTabIndex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"";
  return buffer;
  }

  buffer += "      <div class=\"span3\">          <span location=\"email\"/>      </div>      <div class=\"span3\">          <span location=\"password\"/>      </div>      <div class=\"span3\">          <button type=\"submit\" class=\"btn btn-block btn-primary submit\" ";
  stack1 = helpers['if'].call(depth0, depth0.submitButtonTabIndex, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " style=\"float:right;\">";
  if (stack1 = helpers.submitButtonText) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.submitButtonText; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</button>      </div>      <span _template_=\"_template_\" data-selector-format=\"name\" />  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-door-form-fs'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-door-form-forgot-fs'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "tabindex=\"";
  if (stack1 = helpers.submitButtonTabIndex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.submitButtonTabIndex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"";
  return buffer;
  }

  buffer += "      <div class=\"span6\">          <span location=\"email\"/>      </div>      <div class=\"span3\">          <button type=\"submit\" class=\"btn btn-block btn-primary submit\" ";
  stack1 = helpers['if'].call(depth0, depth0.submitButtonTabIndex, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " style=\"float:right;\">";
  if (stack1 = helpers.submitButtonText) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.submitButtonText; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</button>      </div>      <span _template_=\"_template_\" data-selector-format=\"name\" />  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-door-form-forgot-fs'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-door-form-register-fs'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "tabindex=\"";
  if (stack1 = helpers.submitButtonTabIndex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.submitButtonTabIndex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"";
  return buffer;
  }

  buffer += "      <div class=\"span2\">          <span location=\"name\"/>      </div>      <div class=\"span2\">          <span location=\"email\"/>      </div>      <div class=\"span2\">          <span location=\"password\"/>      </div>      <div class=\"span2\">          <span location=\"password_confirm\"/>      </div>      <div class=\"span2\">          <button type=\"submit\" class=\"btn btn-block btn-primary submit\" ";
  stack1 = helpers['if'].call(depth0, depth0.submitButtonTabIndex, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " style=\"float:right;\">";
  if (stack1 = helpers.submitButtonText) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.submitButtonText; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</button>      </div>        <span _template_=\"_template_\" data-selector-format=\"name\" />  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-door-form-register-fs'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-register'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  
  return "Or use your email";
  }

function program3(depth0,data) {
  
  
  return "Register";
  }

function program5(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "?redirect=";
  if (stack1 = helpers.redirect) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.redirect; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1);
  return buffer;
  }

  buffer += "      <div class=\"signup-login\" style=\"margin-top: 0\">          ";
  stack1 = self.invokePartial(partials['tac-socials'], 'tac-socials', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          <div class=\"row-fluid\">              <h3>";
  stack1 = helpers['if'].call(depth0, depth0.socials, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <div class=\"span2 first\">              </div>              <span location=\"register\"/>          </div>          <div class=\"row-fluid\">              <div class=\"span2 offset8\" ><a href=\"/login";
  stack1 = helpers['if'].call(depth0, depth0.redirect, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" style=\"float:right\">Already Registered?</a></div>          </div>      </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-register'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-forgot'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "      <div class=\"signup-login\" style=\"margin-top: 0\">          <div class=\"row-fluid\">              <h3>Enter your email to reset your password</h3>              <div class=\"span3 first\">              </div>              <span location=\"forgot\"/>          </div>      </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-forgot'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-socials'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          <div class=\"row-fluid\">              <h2>";
  stack1 = helpers['if'].call(depth0, depth0.registration, {hash:{},inverse:self.program(4, program4, data),fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h2>              <div class=\"span2 first ";
  if (stack1 = helpers.socialOffset) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.socialOffset; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">              </div>              ";
  stack1 = helpers.each.call(depth0, depth0.socials, {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          </div>      ";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "Create account using your social networks";
  }

function program4(depth0,data) {
  
  
  return "Login with";
  }

function program6(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              <div class=\"span2\">                  <button class=\"btn btn-block ";
  if (stack1 = helpers.icon) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.icon; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" onclick=\"location.href='";
  if (stack1 = helpers.url) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.url; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "';\" >                      ";
  if (stack1 = helpers.label) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.label; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "                  </button>              </div>              ";
  return buffer;
  }

  buffer += "      ";
  stack1 = helpers['if'].call(depth0, depth0.socials, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
	Handlebars.registerPartial('tac-socials', Handlebars.templates['tac-socials']); 
})();
