<script id="tac-company-report" type="template">
<div class="container">
<section class="single_company report">
<div class="report-header">
    {{#if return}}<h4>{{{return}}}</h4>{{/if}}
    <h1>{{#if prev}}{{{prev}}}{{/if}}{{{title}}}{{#if next}}{{{next}}}{{/if}}</h1>
    <ul class="options">
        <li>{{{downloadLink}}}</li>
        <li>{{{printLink}}}</li>
        <li>{{{shareLink}}}</li>
    </ul>
</div>
<header>
    <div class="company_title">
        <div class="img-content">
            <div class="img">
                <img class="" src="{{logo}}">
            </div>
            <div class="content">
                <h3>{{{companyName}}}</h3>
                            <span><a href="{{url}}" target="_blank">{{#if urlDisplay}}{{urlDisplay}}{{else}}{{url}}{{/if}}</a>
                                <p>{{{location}}}</p>
                            </span>
                <span>
                    <ul class="socials">
                        {{#each socials}}
                        <li>
                            <a href="{{link}}">
                                <img src="{{icon}}">
                            </a>
                        </li>
                        {{/each}}
                    </ul>
                </span>
            </div>
        </div>
    </div>
</header>
<article class="base_data_report">

    <span location="content"/>


</article>
</section>
</div>
<span _template_="_template_" data-selector-format="location" data-selector-location="content"/>
</script>




