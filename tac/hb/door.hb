<script id="tac-login" type="template">
    <div class="signup-login" style="margin-top: 0">
        {{> tac-socials}}
        <div class="row-fluid">
            <h3>{{#if socials}}Or use your email{{else}}Login{{/if}}</h3>
            <div class="span3 first">
            </div>
            <span location="login"/>
        </div>
        <div class="row-fluid">
            <div class="span3 offset3" ><a href="/forgot" style="float:right"> Forgot Password?</a></div>
            <div class="span3" ><a href="/register{{#if redirect}}?redirect={{redirect}}{{/if}}" style="float:right">Not Registered?</a></div>
        </div>

    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>
<script id="tac-door-form" type="template">
    <form>
        <span location="fs"/>
    </form>
    <span _template_="_template_" data-selector-format="location" data-selector-location="fs"/>
</script>
<script id="tac-login-input" type="template">
    <input name="{{name}}" type="{{inputType}}" placeholder="{{placeholder}}" class="span12" {{#if tabindex}} tabindex="{{tabindex}}" {{/if}} value="{{value}}" >
        {{> o-form-errors}}


</script>
<script id="tac-door-form-fs" type="template">
    <div class="span3">
        <span location="email"/>
    </div>
    <div class="span3">
        <span location="password"/>
    </div>
    <div class="span3">
        <button type="submit" class="btn btn-block btn-primary submit" {{#if submitButtonTabIndex}}tabindex="{{submitButtonTabIndex}}"{{/if}} style="float:right;">{{submitButtonText}}</button>
    </div>
    <span _template_="_template_" data-selector-format="name" />
</script>
<script id="tac-door-form-forgot-fs" type="template">
    <div class="span6">
        <span location="email"/>
    </div>
    <div class="span3">
        <button type="submit" class="btn btn-block btn-primary submit" {{#if submitButtonTabIndex}}tabindex="{{submitButtonTabIndex}}"{{/if}} style="float:right;">{{submitButtonText}}</button>
    </div>
    <span _template_="_template_" data-selector-format="name" />
</script>
<script id="tac-door-form-register-fs" type="template">
    <div class="span2">
        <span location="name"/>
    </div>
    <div class="span2">
        <span location="email"/>
    </div>
    <div class="span2">
        <span location="password"/>
    </div>
    <div class="span2">
        <span location="password_confirm"/>
    </div>
    <div class="span2">
        <button type="submit" class="btn btn-block btn-primary submit" {{#if submitButtonTabIndex}}tabindex="{{submitButtonTabIndex}}"{{/if}} style="float:right;">{{submitButtonText}}</button>
    </div>

    <span _template_="_template_" data-selector-format="name" />
</script>
<script id="tac-register" type="template">
    <div class="signup-login" style="margin-top: 0">
        {{> tac-socials}}
        <div class="row-fluid">
            <h3>{{#if socials}}Or use your email{{else}}Register{{/if}}</h3>
            <div class="span2 first">
            </div>
            <span location="register"/>
        </div>
        <div class="row-fluid">
            <div class="span2 offset8" ><a href="/login{{#if redirect}}?redirect={{redirect}}{{/if}}" style="float:right">Already Registered?</a></div>
        </div>
    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>
<script id="tac-forgot" type="template">
    <div class="signup-login" style="margin-top: 0">
        <div class="row-fluid">
            <h3>Enter your email to reset your password</h3>
            <div class="span3 first">
            </div>
            <span location="forgot"/>
        </div>
    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>
<script id="tac-socials" type="partial">
    {{#if socials}}
        <div class="row-fluid">
            <h2>{{#if registration}}Create account using your social networks{{else}}Login with{{/if}}</h2>
            <div class="span2 first {{socialOffset}}">
            </div>
            {{#each socials}}
            <div class="span2">
                <button class="btn btn-block {{icon}}" onclick="location.href='{{url}}';" >
                    {{{label}}}
                </button>
            </div>
            {{/each}}
        </div>
    {{/if}}
</script>