<script id="tac-company" type="template">
<div class="container">
        {{{returnSearch}}}
        <section class="single_company">
         <header>
            <div class="company_title">
                <div class="img-content">
                    <div class="img">
                        <img src="{{logo}}" class="">
                    </div>
                    <div class="content">
                        <h3>{{companyName}} <span style="font-size:12px; float:none"> <a style="float:none" target="_blank" href="{{url}}">{{#if urlDisplay}}{{urlDisplay}}{{else}}{{url}}{{/if}}</a></span></h3>
                        <span>

                            <p>{{location}}</p>
                            <ul class="socials">
                                {{#each socials}}
                                <li>
                                    <a href="{{link}}">
                                        <img src="{{icon}}">
                                    </a>
                                </li>
                                {{/each}}
                            </ul>
                        </span>
                    </div>
                </div>
                {{{trackCompany}}}
            </div>
        <span location="company_menu"/>

            </header>
            <article class="base_data">
                <span location="company_content"/>
            </article>
     </section>
    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>

<script id="tac-block-none" type="template">
    <li class="">
      <p>This company was not found on this source.</p>
    </li>
</script>
<script id="tac-block-wait" type="template">
    <div class="">
      <p>We are currently looking into linking this source with the company.  Please try back later.</p>
    </div>
</script>
<script id="tac-block-all" type="template">
    <ul class="source">
        {{#each _hbSubViewCount}}
        <li location="{{_hbParent.cid}}_{{_hbIndex}}"></li>
        {{/each}}
    </ul>
    <span _template_="_template_" data-selector-format="index"/>
</script>
<script id="tac-block-source-empty" type="template">
    <div class="window">
    {{#if icon}}
    <div class="source_logo">
        <img src="/tac/img/icons/{{icon}}.png">{{{source}}}
    </div>
     {{/if}}
        <div class="two-row">
            {{{msg}}}
        </div>
    </div>
</script>
<script id="tac-block-crunchbase" type="template">
    <div class="top-white_bottom-gray">
        <img src="/tac/img/crunchbase.png" class="source_logo">
        <div class="inner">
            <ul class="top">
                {{#each items}}
                <li class="{{class}}">({{date}}) {{{text}}}</li>
                {{/each}}
            </ul>
        </div>
        <div class="bottom">
            <div class="col-two">
                <h6>Total Amount Raised</h6>
                <h4>{{{totalAmountRaised}}}</h4>

            </div>
            <div class="col-two">
                <h6>Last Round</h6>
                <h5>{{{lastRound}}}</h5>
                <span class="">{{{lastRoundDate}}}</span>
            </div>
        </div>
    </div>
</script>
<script id="tac-block-facebook" type="template">
    <div class="window">
        <div class="source_logo">
            <img src="/tac/img/icons/facebook.png">Facebook
        </div>
        <div class="trebol">
            <div class="leaf likes">
                <h1>{{likes}}</h1>
                <p>Likes</p>
            </div>
            <div class="leaf talking">
                <h1>{{talkingAboutThis}}</h1>
                <p>Talking about this</p>
            </div>
            <div class="leaf count">
                <h1>{{wereHereCount}}</h1>
                <p>Were Here Count</p>
            </div>
            <div class="leaf checkins">
                <h1>{{checkins}}</h1>
                <p>Checkins </p>
            </div>
        </div>
    </div>
</script>

<script id="tac-block-twitter" type="template">
    <div class="four-rows">
        <div class="source_logo">
            <img src="/tac/img/icons/twitter.png">Twitter
        </div>
        <div class="stats">
            <div class="four_rows">
                <div>
                    <p>Followers</p>
                    <span>{{followers}}</span>
                </div>
            </div>
            <div class="four_rows">
                <div>
                    <p>Following</p>
                    <span>{{following}}</span>
                </div>
            </div>
            <div class="four_rows">
                <div>
                    <p>Tweets</p>
                    <span>{{tweets}}</span>
                </div>
            </div>
            <div class="four_rows">
                <div>
                    <p>Favs</p>
                    <span>{{favourites}}</span>
                </div>
            </div>
        </div>
    </div>
</script>
<script id="tac-block-linked_in" type="template">
    <div class="two-row">
        <div class="source_logo">
            <img src="/tac/img/icons/linked_in.png">LinkedIn</div>
        <div class="domino">
            <div class="piece employees">
                <h1>{{employees}}</h1>
                <p>Employees</p>
            </div>
            <div class="piece followers">
                <h1>{{followers}}</h1>
                <p>Followers</p>
            </div>
        </div>
    </div>
</script>
<script id="tac-block-google_plus" type="template">
    <div class="two-row">
        <div class="source_logo">
            <img src="/tac/img/icons/google_plus.png">Google Plus
        </div>
        <div class="domino">
            <div class="piece plus">
                <h1>{{plusOneCount}}</h1>
                <p>Plus One Count</p>
            </div>
            <div class="piece followers">
                <h1>{{circleByCount}}</h1>
                <p>Circled By Count</p>
            </div>
        </div>
    </div>
</script>
<script id="tac-block-blog" type="template">
    <div class="two-row">
        <div class="source_logo">
            <img src="/tac/img/icons/blog.png">Blog</div>
        <div class="domino">
            <div class="piece post">
                <h1>{{newBlogPosts}}</h1>
                <p>New Blog Posts</p>
            </div>
            <div class="piece comments">
                <h1>{{totalBlogPosts}}</h1>
                <p>Total Blog Posts</p>
            </div>
        </div>
    </div>
</script>
<script id="tac-company-report" type="template">
    <div class="container">
					<section class="single_company report">
						<div class="report-header">
							<h1>{{{report_title}}}</h1>
							<ul class="options">
								<li>
									<a href="/tac/main/download">
										<span class="icon-download"></span>Download report
									</a>
								</li>
								<li>
									<a href="/tac/main/print">
										<span class="icon-print"></span>Print report
									</a>
								</li>
								<li>
									<a href="/tac/main/share">
										<span class="icon-email"></span>Email report
									</a>
								</li>
							</ul>
						</div>

						<header>
							<div class="company_title">
 							<div class="img-content">
 								<div class="img">
 									<img src="{{img}}" class="">
 								</div>
						<div class="content">
							<h3>{{companyName}}</h3>
  								<span>
    				<a target="_blank" href="{{url}}">{{#if url_display}}{{url_display}}{{else}}{{url}}{{/if}}</a>
    				<p>{{{location}}}</p>
   				</span>
   				<ul class="socials">
                    {{#each socials}}
   					<li>
   						<a href="{{link}}">
   							<img src="{{img}}">
   						</a>
   					</li>
                    {{/each}}
   				</ul>
			</div>
			</div>
			</div>
   				</header>
     			<article class="base_data_report">
                    <span location="items"></span>
                </article>
            </section>
        </div>
    <span _template_="_template_" data-selector-format="location" data-selector-location="items"/>
</script>
<script id="tac-company-settings-general" type="template">
    <div class="fluid-row">
        <div class="span6">
            <div class="stat">
            <section>
                <header>
                    <h3>Sources to track</h3>
                </header>
                <article>
                    <div class="tab-content-holder">
                        <span location="sources"/>
                    </div>
                </article>
            </section>
        </div>
        </div>
        <div class="span6">
            <div class="stat">
                            <section>
                                <header>
                                    <h3>Reports</h3>
                                </header>
                                <article>
                                    <div class="tab-content-holder generate-report">
                                        <span location="reports"/>

                                    </div>
                                </article>
                            </section>
                        </div>
        </div>
    </div>
    <div class="fluid-row">
        <div class="span12">
            <div class="stat stop">
                <section>
                    <header>
                        <h3>{{{stopTracking}}}</h3>
                    </header>

                </section>
            </div>
        </div>
    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>

<script id="tac-block-crunchbase-overview" type="template">
    <ul class="simple two-col">
        <li>
            <h3>{{{location}}}</h3>
            <span>Location</span>
        </li>
        <li>
            <a href="#">{{{website}}}</a>
            <span>Website</span>
        </li>
        <li>
            <h3>{{{category}}}</h3>
            <span>Category</span>
        </li>
        <li>
            <h3>{{{phone}}}</h3>
            <span>Phone</span>
        </li>
        <li>
            <h3>{{{founded}}}</h3>
            <span>Founded</span>
        </li>
        <li>
            <a href="#">{{{blog}}}</a>
            <span>Blog</span>
        </li>
        <li>
            <h3>{{{employees}}}</h3>
            <span>Employees</span>
        </li>
        <li>
            {{#if twitter}}<a href="https://twitter.com/{{twitter}}">@{{twitter}}</a>{{/if}}
            <span>Twitter</span>
        </li>
        <li>
            <h3>{{{totalAmountRaised}}}</h3>
            <span>Amount Raised</span>
        </li>
        <li>
            <h3 class="tags">{{{tags}}}</h3>
            <span>Tags</span>
        </li>
    </ul>
    <footer>
        <h4>Description</h4>
        <p>{{{description}}}</p>
    </footer>
    <span _template_="_template_" data-selector-format="name"/>
</script>

<script id="tac-block-crunchbase-people" type="template">
    <div class="tab-pane" id="people">
        <ul class="simple three-col">
            {{#each people}}
            <li>
                <div class="img-content">
                    <div class="img">
                        {{#if image}}<img src="//www.crunchbase.com/{{{image}}}">{{/if}}
                    </div>
                    <div class="content">
                        <h3>{{name}}</h3>
                        <span>{{title}}</span>
                    </div>
                </div>
            </li>
            {{/each}}
        </ul>
    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>

<script id="tac-block-crunchbase-products" type="template">
    <div class="tab-pane" id="products">
        <ul class="simple three-col">
            {{#each products}}
            <li>
                <a href="//www.crunchbase.com/product/{{{permalink}}}" target="_blank">{{{name}}}</a>
            </li>
            {{/each}}
        </ul>
    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>

<script id="tac-block-crunchbase-media" type="template">
    <ul class="simple free">
        {{#each videos}}
            <li>
                {{{embed}}}
            </li>
        {{/each}}
        {{#each images}}
        <li>
            {{#if image}}<img src="//www.crunchbase.com/{{{image}}}">{{/if}}
        </li>
        {{/each}}
    </ul>
    <span _template_="_template_" data-selector-format="name"/>
</script>

<script id="tac-block-crunchbase-milestones" type="template">
    <div class="tab-content-holder">
        {{#if milestones}}
        <table class="table">
            <thead>
            <tr>
                <th>Date</th>
                <th>Milestone</th>
            </tr>
            </thead>
            <tbody>
            {{#each milestones}}
            <tr>
                <td>{{{date}}}</td>
                <td>{{{text}}}</td>
            </tr>
            {{/each}}
            </tbody>
        </table>
        {{else}}
        No milestones
        {{/if}}
    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>

<script id="tac-block-crunchbase-funding" type="template">
    <div class="tab-content-holder">
        {{#if fundings}}
        <table class="table">
            <thead>
            <tr>
                <th>Investor</th>
                <th>Round</th>
                <th>Amount</th>
                <th>Date</th>
            </tr>
            </thead>
            <tbody>
            {{#each fundings}}
            <tr>
                <td>{{{investor}}}</td>
                <td>{{{round}}}</td>
                <td>{{{amount}}}</td>
                <td>{{{date}}}</td>
            </tr>
            {{/each}}
            </tbody>
        </table>
        {{else}}
        No fundings
        {{/if}}
    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>

<script id="tac-block-crunchbase-competitors" type="template">
    <div class="tab-pane" id="competitors">
        <ul class="simple three-col">
            {{#each competitors}}
            <li>
                <a href="//www.crunchbase.com/company/{{{permalink}}}" target="_blank">{{{name}}}</a>
            </li>
            {{/each}}
        </ul>
    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>

<script id="tac-block-crunchbase-acquisitions" type="template">

        <div class="tab-content-holder">
            {{#if acquisitions}}
            <table class="table">
                <thead>
                <tr>
                    <th>Company</th>
                    <th>Source</th>
                    <th>Amount</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                {{#each acquisitions}}
                <tr>
                    <td>{{{company}}}</td>
                    <td>{{{source}}}</td>
                    <td>{{{amount}}}</td>
                    <td>{{{date}}}</td>
                </tr>
                {{/each}}
                </tbody>
            </table>
            {{else}}
            No acquisitions
            {{/if}}
        </div>

    <span _template_="_template_" data-selector-format="name"/>
</script>

<script id="tac-block-crunchbase-investments" type="template">

        <div class="tab-content-holder">
            {{#if investments}}
            <table class="table">
                <thead>
                <tr>
                    <th>Company</th>
                    <th>Round</th>
                    <th>Amount</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                {{#each investments}}
                <tr>
                    <td>{{{company}}}</td>
                    <td>{{{round}}}</td>
                    <td>{{{amount}}}</td>
                    <td>{{{date}}}</td>
                </tr>
                {{/each}}
                </tbody>
            </table>
            {{else}}
            No investments
            {{/if}}

        </div>

    <span _template_="_template_" data-selector-format="name"/>
</script>

<script id="tac-settings-sources" type="template">
    <ul class="selection">
        {{#each list}}
        <li>
            <label>
                <input name="{{../name}}" {{#if ../disabled}}disabled{{/if}} type="checkbox" value="{{value}}" {{#if selected}}checked="checked"{{/if}} {{{attributes}}} {{#if tabindex}} tabindex="{{tabindex}}" {{/if}} />
                <img src="{{icon}}">
                {{{label}}}
            </label>
        </li>
        {{/each}}
    </ul>
    <span _template_="_template_" data-selector-format="name"/>
</script>
<script id="tac-block-settings-general" type="template">
    <div class="fluid-row">
        <div class="span6">
            <div class="stat">
                <section>
                    <header>
                        <h3>Sources being tracked</h3>

                    </header>
                    <article>
                        <div class="tab-content-holder">
                            <span location="settingsSources"/>
                        </div>
                    </article>
                </section>
            </div>
        </div>
        <div class="span6">
            <div class="stat">
                <section>
                    <header>
                        <h3>Reports</h3>

                    </header>
                    <article>
                        <div class="tab-content-holder generate-report">
                            <span location="report"/>
                        </div>
                    </article>
                </section>
            </div>
        </div>
    </div>
    <div class="fluid-row">
        <div class="span12">
            <div class="stat stop">
                <section>
                    <header>
                        <h3>{{{trackCompany}}}</h3>
                    </header>

                </section>
            </div>
        </div>
    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>
<script id="tac-block-settings-competitors" type="template">
    <div class="fluid-row">
        <ul class="companies-list">
            <h3>{{totalCompetitors}} Competitor{{#ifNotEq totalCompetitors 1}}s{{/ifNotEq}}{{{manageCompetitors}}}</h3>
            {{#each competitors}}
            <li>
                <a href="#">
                    <h5>{{name}}</h5>
                    <div class="logo-holder">
                        <img src="{{logo}}">
                    </div>
                    {{{remove}}}
                </a>
            </li>
            {{/each}}
        </ul>
        {{{addCompetitor}}}
    </div>
    <span _template_="_template_" data-selector-format="name"/>
</script>



<script id="tac-block-facebook-profile" type="template">
    <ul class="simple two-col">
        <li>
            <h3>{{{address}}}</h3>
            <span>Address</span>
        </li>
        <li>
            <h3>{{{phone}}}</h3>
            <span>Phone</span>
        </li>
        <li>
            <h3>{{{founded}}}</h3>
            <span>Founded</span>
        </li>
        <li>
            <h3>{{{checkins}}}</h3>
            <span>Checkings</span>
        </li>
        <li>
            <h3>{{{likes}}}</h3>
            <span>Likes</span>
        </li>
        <li>
            <h3>{{{talkingAboutCount}}}</h3>
            <span>Talking About Count</span>
        </li>
        <li>
            <h3>{{{wereHereCount}}}</h3>
            <span>Were Here Count</span>
        </li>
        <li>
            <img src="{{{cover}}}">
            <span>Cover</span>
        </li>
    </ul>
    <footer>
        <div>
            <h4>Products</h4>
            <p>{{{products}}}</p>
        </div>
        <div>
            <h4>Mission</h4>
            <p>{{{mission}}}</p>
        </div>
    </footer>
    <span _template_="_template_" data-selector-format="name"/>
</script>

<script id="tac-block-twitter-profile" type="template">
    <ul class="simple two-col">
        <li>
            <h3>{{{location}}}</h3>
            <span>Location</span>
        </li>
        <li>
            <h3>{{{followers}}}</h3>
            <span>Followers</span>
        </li>
        <li>
            <h3>{{{friends}}}</h3>
            <span>Friends</span>
        </li>
        <li>
            <h3>{{{listed}}}</h3>
            <span>Listed</span>
        </li>
        <li>
            <h3>{{{favorites}}}</h3>
            <span>Favorites</span>
        </li>
    </ul>
    <footer>
        <div>
            <h4>Description</h4>
            <p>{{{description}}}</p>
        </div>
    </footer>
    <span _template_="_template_" data-selector-format="name"/>
</script>

<script id="tac-block-stats" type="template">
    <ul class="simple two-col">
        <span location="stats"/>
    </ul>
    <span _template_="_template_" data-selector-format="location" data-selector-location="stats"/>
</script>

<script id="tac-block-linked_in-profile" type="template">
    <ul class="simple two-col">
        <li>
            <h3>{{{location}}}</h3>
            <span>Location</span>
        </li>
        <li>
            <h3>{{{phone}}}</h3>
            <span>Phone</span>
        </li>
        <li>
            <h3>{{{founded}}}</h3>
            <span>Founded</span>
        </li>
        <li>
            <h3>{{{employees}}}</h3>
            <span>Employees</span>
        </li>
        <li>
            <h3>{{{followers}}}</h3>
            <span>Followers</span>
        </li>
    </ul>
    <footer>
        <div>
            <h4>Specialties</h4>
            <p>{{{specialties}}}</p>
        </div>
        <div>
            <h4>Description</h4>
            <p>{{{description}}}</p>
        </div>
    </footer>
    <span _template_="_template_" data-selector-format="name"/>
</script>
<script id="tac-block-feed" type="template">
    <div class="tab-content-holder">
        {{#if feeds}}
        <span location="feed"/>
        {{else}}
        No feeds
        {{/if}}
    </div>
    <span _template_="_template_" data-selector-format="location" data-selector-location="feed"/>
</script>
<script id="tac-block-updates" type="template">
    <div class="tab-content-holder">
        {{#if update}}
        <span location="update"/>
        {{else}}
        No Updates
        {{/if}}
    </div>
    <span _template_="_template_" data-selector-format="location" data-selector-location="update"/>
</script>
<script id="tac-block-google_plus-profile" type="template">
    <ul class="simple two-col">
        <li>
            <h3>{{{plusOne}}}</h3>
            <span>Plus One Count</span>
        </li>
        <li>
            <h3>{{{circle}}}</h3>
            <span>Circled By Count</span>
        </li>
        <li>
            <img src="{{{cover}}}">
            <span>Cover</span>
        </li>
    </ul>
    <footer>
        <div>
            <h4>Description</h4>
            <p>{{{description}}}</p>
        </div>
    </footer>
    <span _template_="_template_" data-selector-format="name"/>
</script>
<script id="tac-block-blog-profile" type="template">
    <ul class="simple two-col">
        <li>
            <h3>{{{newBlogPosts}}}</h3>
            <span>New Blog Posts</span>
        </li>
        <li>
            <h3>{{{comments}}}</h3>
            <span>Comments</span>
        </li>
    </ul>
    <span _template_="_template_" data-selector-format="name"/>
</script>