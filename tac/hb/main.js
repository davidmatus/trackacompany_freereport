(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-search-results'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "           <h2>";
  stack1 = helpers['if'].call(depth0, depth0.resultsMsg, {hash:{},inverse:self.program(4, program4, data),fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h2>              <span location=\"results\"/>            ";
  return buffer;
  }
function program2(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.resultsMsg) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.resultsMsg; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }

function program4(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  if (stack1 = helpers.numCompanies) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.numCompanies; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " result";
  options = {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data};
  stack2 = ((stack1 = helpers.ifNotEq || depth0.ifNotEq),stack1 ? stack1.call(depth0, depth0.numCompanies, 1, options) : helperMissing.call(depth0, "ifNotEq", depth0.numCompanies, 1, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " for \"";
  if (stack2 = helpers.companyName) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.companyName; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" on ";
  if (stack2 = helpers.source) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.source; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2);
  return buffer;
  }
function program5(depth0,data) {
  
  
  return "s";
  }

function program7(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "                <h2>";
  stack1 = helpers['if'].call(depth0, depth0.resultsMsg, {hash:{},inverse:self.program(10, program10, data),fn:self.program(8, program8, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              </h2>          ";
  return buffer;
  }
function program8(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "                      ";
  if (stack1 = helpers.resultsMsg) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.resultsMsg; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "                  ";
  return buffer;
  }

function program10(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "                      ";
  stack1 = helpers['if'].call(depth0, depth0.companyName, {hash:{},inverse:self.program(13, program13, data),fn:self.program(11, program11, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "                  ";
  return buffer;
  }
function program11(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "                          No results for \"";
  if (stack1 = helpers.companyName) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.companyName; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" on ";
  if (stack1 = helpers.source) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.source; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + ". Please search again and/or try a different source.                      ";
  return buffer;
  }

function program13(depth0,data) {
  
  
  return "                        ";
  }

  buffer += "      <div class=\"container\" style=\"min-height:400px\">              <section class=\"search_company_results span12\">              <article>                <span location=\"search\"/>              </article>            </section>          <section class=\"results span12\">          ";
  stack1 = helpers['if'].call(depth0, depth0.numCompanies, {hash:{},inverse:self.program(7, program7, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      </section>      </div>          <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-search-results'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-search-fs'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "      <span location=\"company\"/>      <span location=\"source\"/>     <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-search-fs'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-search-field-company'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "      <input name=\"company\" class=\"span11\" type=\"text\" value=\"";
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" placeholder=\"";
  if (stack1 = helpers.placeholder) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.placeholder; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">      <button class=\"btn btn-lg submit\" style=\"float:right\">search</button>  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-search-field-source'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          <label class=\"radio\">            <input type=\"radio\" name=\"source\" id=\"";
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" value=\"";
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ";
  stack1 = helpers['if'].call(depth0, depth0.selected, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">            <img src=\"/tac/img/";
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + ".png\">          </label>          ";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "checked";
  }

  buffer += "      <h4>Source:</h4>      <div class=\"source_options\">          ";
  stack1 = helpers.each.call(depth0, depth0.list, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "        </div>  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-company-details'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", self=this;

function program1(depth0,data) {
  
  
  return "                      <a href=\"/\">Track a new company</a>                  ";
  }

  buffer += "      <div class=\"search-wrapper\">         <div class=\"container\">          <span location=\"alert\"/>         	<div class=\"company-header\">              <div class=\"row-fluid\">                  <div class=\"span6\">                      <div class=\"company-title\">                      <span>";
  if (stack1 = helpers.logo) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.logo; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span>                      <h2>";
  if (stack1 = helpers.companyName) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.companyName; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h2>                      </div>                  </div>                  <div class=\"span3\">                      ";
  if (stack1 = helpers.trackButton) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.trackButton; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "                  </div>                  <div class=\"span3\">                  <span location=\"selectCompany\"/>                  ";
  stack1 = helpers['if'].call(depth0, depth0.newLink, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "                  </div>              </div>          </div>          <div class=\"row-fluid\">            <div class=\"span3\">              <span location=\"feed\"/>              <span location=\"list\"/>            </div>            <div class=\"span9\">                <span location=\"view\"/>            </div>          </div>         </div>      </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-company-details'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-company-source'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function";


  buffer += "      <div class=\"content right-content track-content\">          <div class=\"track-content-title\">          <h3>";
  if (stack1 = helpers.sourceName) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.sourceName; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>          ";
  if (stack1 = helpers.buttons) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.buttons; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          </div>          <span location=\"items\"/>          <!--<div class=\"track-alert\">-->          <!--<h5>Not Linked Yet!</h5>-->          <!--<h6><a href=\"#\">Link or match the company with the right source</a></h6>-->          <!--</div>-->      </div>      <span _template_=\"_template_\" data-selector-format=\"location\" data-selector-location=\"items\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-company-source'] = {"selectorFormat":"location","selectorLocation":"items"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-plans'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "                  <li>";
  stack1 = (typeof depth0 === functionType ? depth0.apply(depth0) : depth0);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</li>                ";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "            <a href=\"#refresh\" data-server=\"";
  if (stack1 = helpers.leftAction) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.leftAction; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"btn btn-org btn-signup\">";
  if (stack1 = helpers.leftSelect) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.leftSelect; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a>            ";
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "                    <li>";
  stack1 = (typeof depth0 === functionType ? depth0.apply(depth0) : depth0);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</li>                  ";
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              <a href=\"#refresh\" data-server=\"";
  if (stack1 = helpers.middleAction) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.middleAction; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"btn btn-grn btn-buy\">";
  if (stack1 = helpers.middleSelect) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.middleSelect; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a>              ";
  return buffer;
  }

function program9(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              <a href=\"#refresh\" data-server=\"";
  if (stack1 = helpers.rightAction) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.rightAction; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"btn btn-org btn-signup\">";
  if (stack1 = helpers.rightSelect) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.rightSelect; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a>              ";
  return buffer;
  }

  buffer += "      <div class=\"pricing-wrapper\" style=\"padding:0;\">        <div class=\"container\">          <div class=\"pricing pricing-left\">            <h3>";
  if (stack1 = helpers.leftTitle) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.leftTitle; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>            <h2>";
  if (stack1 = helpers.leftPrice) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.leftPrice; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h2>            <ul>                ";
  stack1 = helpers.each.call(depth0, depth0.leftFeatures, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "            </ul>            ";
  stack1 = helpers['if'].call(depth0, depth0.leftAction, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          </div>          <div class=\"pricing pricing-middle\">              <h3>";
  if (stack1 = helpers.middleTitle) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.middleTitle; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <h2>";
  if (stack1 = helpers.middlePrice) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.middlePrice; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h2>              <ul>                  ";
  stack1 = helpers.each.call(depth0, depth0.middleFeatures, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              </ul>              ";
  stack1 = helpers['if'].call(depth0, depth0.middleAction, {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          </div>          <div class=\"pricing pricing-right\">              <h3>";
  if (stack1 = helpers.rightTitle) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.rightTitle; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <h2>";
  if (stack1 = helpers.rightPrice) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.rightPrice; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h2>              <ul>                  ";
  stack1 = helpers.each.call(depth0, depth0.rightFeatures, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              </ul>              ";
  stack1 = helpers['if'].call(depth0, depth0.rightAction, {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "            </div>        </div>      </div>    ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-search-results-page'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "            <li>              <header>                <h4>";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h4>                ";
  if (stack1 = helpers.select) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.select; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              </header>              ";
  stack1 = helpers['if'].call(depth0, depth0.img, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              <span>                ";
  stack1 = helpers['if'].call(depth0, depth0.url, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "                <p>";
  if (stack1 = helpers.location) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.location; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</p>              </span>              <p>";
  if (stack1 = helpers.description) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.description; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</p>            </li>         ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<img src=\"";
  if (stack1 = helpers.img) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.img; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  return buffer;
  }

function program4(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<a href=\"";
  if (stack1 = helpers.url) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.url; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" target='_blank'>";
  if (stack1 = helpers.url) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.url; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</a>";
  return buffer;
  }

  buffer += "      <ul class=\"list\">          ";
  stack1 = helpers.each.call(depth0, depth0.results, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      </ul>  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-dashboard'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "      <div class=\"container\">      <section class=\"companies span12\">          <div class=\"row-fluid\">          <span class=\"span12\" location=\"list\"/>          </div>      </section>      </div>      <span _template_=\"_template_\" data-selector-format=\"location\" data-selector-location=\"list\"/>  ";
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-dashboard'] = {"selectorFormat":"location","selectorLocation":"list"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-dashboard-page'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              <li>              <a href=\"";
  if (stack1 = helpers.link) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.link; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">                <h5>";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h5>                <div class=\"logo-holder\">                  <img src=\"";
  if (stack1 = helpers.logo) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.logo; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">                </div>                <p> ";
  if (stack1 = helpers.competitors) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.competitors; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</p>              </a>            </li>         ";
  return buffer;
  }

  buffer += "      <ul class=\"companies-list\">          ";
  stack1 = helpers.each.call(depth0, depth0.companies, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      </ul>      ";
  if (stack1 = helpers.addCompany) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.addCompany; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-profile-cc'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          <h3>Current Card</h3>          <h4> <span>";
  if (stack1 = helpers.nameOnCard) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.nameOnCard; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span><small>Name on Card</small> </h4>          <h4> <span>";
  if (stack1 = helpers.cc) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.cc; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span><small>Card No</small> </h4>          <h4> <span>";
  if (stack1 = helpers.expires) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.expires; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span><small>Expires</small></h4>         ";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          ";
  if (stack1 = helpers.msg) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.msg; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "        ";
  return buffer;
  }

  buffer += "      <div class=\"tab-pane billing-tab-content\" style=\"min-height:200px\">        ";
  stack1 = helpers['if'].call(depth0, depth0.cc, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          <h3>";
  if (stack1 = helpers.updateCard) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.updateCard; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>      </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-profile-cc'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-profile-plans'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "          <div class=\"plan\">              <header>                  ";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              </header>              <div class=\"orange\">                  <p>starts at</p>                  <h3>$";
  if (stack1 = helpers.monthly) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.monthly; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "/month</h3>              </div>              <article>                  <h4>Track ";
  if (stack1 = helpers.companies) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.companies; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " ";
  options = {hash:{},inverse:self.program(4, program4, data),fn:self.program(2, program2, data),data:data};
  stack2 = ((stack1 = helpers.ifEq || depth0.ifEq),stack1 ? stack1.call(depth0, depth0.companies, 1, options) : helperMissing.call(depth0, "ifEq", depth0.companies, 1, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</h4>                  <h5>+ competitors</h5>                  <p>";
  if (stack2 = helpers.users) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.users; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</p>                  <p>";
  if (stack2 = helpers.additional) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.additional; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</p>              </article>              ";
  if (stack2 = helpers.choose) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.choose; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "          </div>          ";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "company";
  }

function program4(depth0,data) {
  
  
  return "companies";
  }

  buffer += "      <div class=\"pricing\">          ";
  stack1 = helpers.each.call(depth0, depth0.plans, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      </div>  ";
  return buffer;
  });
})();
