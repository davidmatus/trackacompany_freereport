(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-email'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "  					<tr>  						<td bgcolor=\"#ffffff\" style=\"padding: 20px 30px 40px 30px;\">  							<h3 style=\"margin:0 0 10px 0;\">Stats</h3>                              ";
  if (stack1 = helpers.stats) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.stats; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "                          </td>     					</tr>                      <tr>                      	<td bgcolor=\"#ffffff\" style=\"padding: 20px 30px 40px 30px;\">                              <a href=\"";
  if (stack1 = helpers.reportLink) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.reportLink; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" style=\"padding: 18px 30px; border-color: #00A4C7; box-shadow: none; background: #00A4C7; font-size: 20px; line-height: 28px; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; color: white; text-decoration: none; margin-top: 10px; display: block; text-align:center;\">View full report</a>  						</td>  					</tr>                      ";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "  					<tr>  						<td bgcolor=\"#ffffff\" style=\"padding: 20px 30px 40px 30px;\">  							<h3 style=\"margin:0 0 10px 0;\">Feeds</h3>                              ";
  if (stack1 = helpers.feed) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.feed; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "                          </td>     					</tr>                      <tr>                      	<td bgcolor=\"#ffffff\" style=\"padding: 20px 30px 40px 30px;\">                              <a href=\"";
  if (stack1 = helpers.reportLink) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.reportLink; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" style=\"padding: 18px 30px; border-color: #00A4C7; box-shadow: none; background: #00A4C7; font-size: 20px; line-height: 28px; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; color: white; text-decoration: none; margin-top: 10px; display: block; text-align:center;\">View full report</a>  						</td>  					</tr>                      ";
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "  					<tr>  						<td bgcolor=\"#ffffff\" style=\"padding: 20px 30px 40px 30px;\">  							<h3 style=\"margin:0 0 10px 0;\">Updates</h3>                              ";
  if (stack1 = helpers.updates) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.updates; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "                          </td>     					</tr>                      <tr>                      	<td bgcolor=\"#ffffff\" style=\"padding: 20px 30px 40px 30px;\">                              <a href=\"";
  if (stack1 = helpers.reportLink) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.reportLink; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" style=\"padding: 18px 30px; border-color: #00A4C7; box-shadow: none; background: #00A4C7; font-size: 20px; line-height: 28px; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; color: white; text-decoration: none; margin-top: 10px; display: block; text-align:center;\">View full report</a>  						</td>  					</tr>                      ";
  return buffer;
  }

  buffer += "  <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">  <html xmlns=\"http://www.w3.org/1999/xhtml\">  <head>  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />  <title></title>  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>  <style type=\"text/css\">  	body {  		font-family: Arial;  	}  	.clearfix {    *zoom: 1;  	}  	.clearfix:before,  	.clearfix:after {  	  display: table;  	  content: \"\";  	  line-height: 0;  	}  	.clearfix:after {  	  clear: both;  	}  	.hide-text {  	  font: 0/0 a;  	  color: transparent;  	  text-shadow: none;  	  background-color: transparent;  	  border: 0;  	}  	table {  	  max-width: 100%;  	  background-color: transparent;  	  border-collapse: collapse;  	  border-spacing: 0;  	}  	.table {  	  width: 100%;  	  margin-bottom: 20px;  	}  	.table th,  	.table td {  	  padding: 8px;  	  line-height: 20px;  	  text-align: left;  	  vertical-align: top;  	  border-top: 1px solid #dddddd;  	}  	.table th {  	  font-weight: bold;  	}  	.table thead th {  	  vertical-align: bottom;  	}  	.table caption + thead tr:first-child th,  	.table caption + thead tr:first-child td,  	.table colgroup + thead tr:first-child th,  	.table colgroup + thead tr:first-child td,  	.table thead:first-child tr:first-child th,  	.table thead:first-child tr:first-child td {  	  border-top: 0;  	}  	.table tbody + tbody {  	  border-top: 2px solid #dddddd;  	}  	.table .table {  	  background-color: #ffffff;  	}  	.table-condensed th,  	.table-condensed td {  	  padding: 4px 5px;  	}  	.table-bordered {  	  border: 1px solid #dddddd;  	  border-collapse: separate;  	  *border-collapse: collapse;  	  border-left: 0;  	  -webkit-border-radius: 4px;  	  -moz-border-radius: 4px;  	  border-radius: 4px;  	}  	.table-bordered th,  	.table-bordered td {  	  border-left: 1px solid #dddddd;  	}  	.table-bordered caption + thead tr:first-child th,  	.table-bordered caption + tbody tr:first-child th,  	.table-bordered caption + tbody tr:first-child td,  	.table-bordered colgroup + thead tr:first-child th,  	.table-bordered colgroup + tbody tr:first-child th,  	.table-bordered colgroup + tbody tr:first-child td,  	.table-bordered thead:first-child tr:first-child th,  	.table-bordered tbody:first-child tr:first-child th,  	.table-bordered tbody:first-child tr:first-child td {  	  border-top: 0;  	}  	.table-bordered thead:first-child tr:first-child > th:first-child,  	.table-bordered tbody:first-child tr:first-child > td:first-child,  	.table-bordered tbody:first-child tr:first-child > th:first-child {  	  -webkit-border-top-left-radius: 4px;  	  -moz-border-radius-topleft: 4px;  	  border-top-left-radius: 4px;  	}  	.table-bordered thead:first-child tr:first-child > th:last-child,  	.table-bordered tbody:first-child tr:first-child > td:last-child,  	.table-bordered tbody:first-child tr:first-child > th:last-child {  	  -webkit-border-top-right-radius: 4px;  	  -moz-border-radius-topright: 4px;  	  border-top-right-radius: 4px;  	}  	.table-bordered thead:last-child tr:last-child > th:first-child,  	.table-bordered tbody:last-child tr:last-child > td:first-child,  	.table-bordered tbody:last-child tr:last-child > th:first-child,  	.table-bordered tfoot:last-child tr:last-child > td:first-child,  	.table-bordered tfoot:last-child tr:last-child > th:first-child {  	  -webkit-border-bottom-left-radius: 4px;  	  -moz-border-radius-bottomleft: 4px;  	  border-bottom-left-radius: 4px;  	}  	.table-bordered thead:last-child tr:last-child > th:last-child,  	.table-bordered tbody:last-child tr:last-child > td:last-child,  	.table-bordered tbody:last-child tr:last-child > th:last-child,  	.table-bordered tfoot:last-child tr:last-child > td:last-child,  	.table-bordered tfoot:last-child tr:last-child > th:last-child {  	  -webkit-border-bottom-right-radius: 4px;  	  -moz-border-radius-bottomright: 4px;  	  border-bottom-right-radius: 4px;  	}  	.table-bordered tfoot + tbody:last-child tr:last-child td:first-child {  	  -webkit-border-bottom-left-radius: 0;  	  -moz-border-radius-bottomleft: 0;  	  border-bottom-left-radius: 0;  	}  	.table-bordered tfoot + tbody:last-child tr:last-child td:last-child {  	  -webkit-border-bottom-right-radius: 0;  	  -moz-border-radius-bottomright: 0;  	  border-bottom-right-radius: 0;  	}  	.table-bordered caption + thead tr:first-child th:first-child,  	.table-bordered caption + tbody tr:first-child td:first-child,  	.table-bordered colgroup + thead tr:first-child th:first-child,  	.table-bordered colgroup + tbody tr:first-child td:first-child {  	  -webkit-border-top-left-radius: 4px;  	  -moz-border-radius-topleft: 4px;  	  border-top-left-radius: 4px;  	}  	.table-bordered caption + thead tr:first-child th:last-child,  	.table-bordered caption + tbody tr:first-child td:last-child,  	.table-bordered colgroup + thead tr:first-child th:last-child,  	.table-bordered colgroup + tbody tr:first-child td:last-child {  	  -webkit-border-top-right-radius: 4px;  	  -moz-border-radius-topright: 4px;  	  border-top-right-radius: 4px;  	}  	.table-striped tbody > tr:nth-child(odd) > td,  	.table-striped tbody > tr:nth-child(odd) > th {  	  background-color: #f9f9f9;  	}  	.table-hover tbody tr:hover > td,  	.table-hover tbody tr:hover > th {  	  background-color: #f5f5f5;  	}      </style>  </head>  <body style=\"margin: 0; padding: 0;\">  	<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">  		<tr>  			<td bgcolor=\"#ffffff\" style=\"padding: 10px 0 30px 0; background: #F3F3F3;\">  				<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border: 1px solid #cccccc; border-collapse: collapse;\">  					<tr>  						<td bgcolor=\"#ffffff\" style=\"padding: 40px 40px 20px 40px; border-bottom: 1px solid #ddd;\">  							<img src=\"";
  if (stack1 = helpers.tacLogo) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.tacLogo; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">  						</td>  					</tr>  					<tr>  						<td bgcolor=\"#ffffff\" style=\"padding: 40px 30px 0 30px;\">  							<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">  								<tr>  									<td style=\"color: #9EA5B2; font-family: Arial, sans-serif; font-size: 32px;\">  										<span>";
  if (stack1 = helpers.title) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.title; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span>  									</td>  								</tr>  								<tr>  									<td style=\"padding-top: 20px;\">  										<img src=\"";
  if (stack1 = helpers.logo) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.logo; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" alt=\"\" width=\"auto\" max-height=\"140\" style=\"display: block;\" />  									</td>  								</tr>  							</table>  						</td>  					</tr>  					<tr>  						<td bgcolor=\"#ffffff\" style=\"padding: 20px 40px 20px 30px; border-bottom: 1px solid #ddd;\">  							<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">  								<tr>  									<td style=\"color: #192A39; font-size: 24px;\" width=\"40%\"><b>";
  if (stack1 = helpers.companyName) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.companyName; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</b></td>  									<td style=\"text-align:right;\"><a href=\"";
  if (stack1 = helpers.companyUrl) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.companyUrl; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" style=\"color: #F16639; text-align:right;\"  width=\"30%\">";
  if (stack1 = helpers.companyUrl) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.companyUrl; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</a></td>  									<td style=\"color:#9EA5B2; text-align:right;\" width=\"30%\">";
  if (stack1 = helpers.companyLocation) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.companyLocation; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>  								</tr>  							</table>  						</td>  					</tr>                      ";
  stack1 = helpers['if'].call(depth0, depth0.stats, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "                      ";
  stack1 = helpers['if'].call(depth0, depth0.feed, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "                      ";
  stack1 = helpers['if'].call(depth0, depth0.updates, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  					<tr>  						<td bgcolor=\"#ffffff\" style=\"padding: 0 40px 20px 30px;\">  							<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">  								<tr>  									<td style=\"color:#9EA5B2\">";
  if (stack1 = helpers.signature) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.signature; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</td>  								</tr>  							</table>  						</td>  					</tr>  					<tr>  						<td bgcolor=\"#ffffff\" style=\"padding: 20px 40px 20px 30px; border-top:1px solid #ddd;\">  							<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">  								<tr>  									<td style=\"color:#9EA5B2; font-size: 12px; line-height: 20px;\">";
  if (stack1 = helpers.copy) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.copy; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "   									</td>   									<td style=\"color:#9EA5B2; font-size: 12px; line-height: 20px; text-align:right;\">     										<span><a href=\"trackacompany.com\" style=\"color:#F16639; font-size: 12px;\">trackacompany.com</a><br/><a href=\"trackacompany.com\" style=\"color:#F16639; font-size: 12px;\">Privacy Policy</a> and <a href=\"trackacompany.com\" style=\"color:#F16639; font-size: 12px;\">Terms and Conditions</a></span>   									</td>  								</tr>  							</table>  						</td>  					</tr>  				</table>  			</td>  		</tr>  	</table>  </body>  </html>  ";
  return buffer;
  });
})();
