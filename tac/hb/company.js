(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-company'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.urlDisplay) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.urlDisplay; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program3(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.url) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.url; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program5(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "                                  <li>                                      <a href=\"";
  if (stack1 = helpers.link) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.link; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">                                          <img src=\"";
  if (stack1 = helpers.icon) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.icon; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">                                      </a>                                  </li>                                  ";
  return buffer;
  }

  buffer += "  <div class=\"container\">          ";
  if (stack1 = helpers.returnSearch) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.returnSearch; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          <section class=\"single_company\">           <header>              <div class=\"company_title\">                  <div class=\"img-content\">                      <div class=\"img\">                          <img src=\"";
  if (stack1 = helpers.logo) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.logo; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"\">                      </div>                      <div class=\"content\">                          <h3>";
  if (stack1 = helpers.companyName) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.companyName; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " <span style=\"font-size:12px; float:none\"> <a style=\"float:none\" target=\"_blank\" href=\"";
  if (stack1 = helpers.url) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.url; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  stack1 = helpers['if'].call(depth0, depth0.urlDisplay, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a></span></h3>                          <span>                                <p>";
  if (stack1 = helpers.location) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.location; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</p>                              <ul class=\"socials\">                                  ";
  stack1 = helpers.each.call(depth0, depth0.socials, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "                              </ul>                          </span>                      </div>                  </div>                  ";
  if (stack1 = helpers.trackCompany) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.trackCompany; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              </div>          <span location=\"company_menu\"/>                </header>              <article class=\"base_data\">                  <span location=\"company_content\"/>              </article>       </section>      </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-company'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-none'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "      <li class=\"\">        <p>This company was not found on this source.</p>      </li>  ";
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-wait'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "      <div class=\"\">        <p>We are currently looking into linking this source with the company.  Please try back later.</p>      </div>  ";
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-all'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "          <li location=\""
    + escapeExpression(((stack1 = ((stack1 = depth0._hbParent),stack1 == null || stack1 === false ? stack1 : stack1.cid)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "_";
  if (stack2 = helpers._hbIndex) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0._hbIndex; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"></li>          ";
  return buffer;
  }

  buffer += "      <ul class=\"source\">          ";
  stack1 = helpers.each.call(depth0, depth0._hbSubViewCount, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      </ul>      <span _template_=\"_template_\" data-selector-format=\"index\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-all'] = {"selectorFormat":"index"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-source-empty'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "      <div class=\"source_logo\">          <img src=\"/tac/img/icons/";
  if (stack1 = helpers.icon) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.icon; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + ".png\">";
  if (stack1 = helpers.source) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.source; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      </div>       ";
  return buffer;
  }

  buffer += "      <div class=\"window\">      ";
  stack1 = helpers['if'].call(depth0, depth0.icon, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          <div class=\"two-row\">              ";
  if (stack1 = helpers.msg) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.msg; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          </div>      </div>  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-crunchbase'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "                  <li class=\"";
  if (stack1 = helpers['class']) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0['class']; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">(";
  if (stack1 = helpers.date) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.date; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + ") ";
  if (stack1 = helpers.text) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.text; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</li>                  ";
  return buffer;
  }

  buffer += "      <div class=\"top-white_bottom-gray\">          <img src=\"/tac/img/crunchbase.png\" class=\"source_logo\">          <div class=\"inner\">              <ul class=\"top\">                  ";
  stack1 = helpers.each.call(depth0, depth0.items, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              </ul>          </div>          <div class=\"bottom\">              <div class=\"col-two\">                  <h6>Total Amount Raised</h6>                  <h4>";
  if (stack1 = helpers.totalAmountRaised) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.totalAmountRaised; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h4>                </div>              <div class=\"col-two\">                  <h6>Last Round</h6>                  <h5>";
  if (stack1 = helpers.lastRound) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.lastRound; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h5>                  <span class=\"\">";
  if (stack1 = helpers.lastRoundDate) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.lastRoundDate; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span>              </div>          </div>      </div>  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-facebook'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "      <div class=\"window\">          <div class=\"source_logo\">              <img src=\"/tac/img/icons/facebook.png\">Facebook          </div>          <div class=\"trebol\">              <div class=\"leaf likes\">                  <h1>";
  if (stack1 = helpers.likes) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.likes; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h1>                  <p>Likes</p>              </div>              <div class=\"leaf talking\">                  <h1>";
  if (stack1 = helpers.talkingAboutThis) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.talkingAboutThis; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h1>                  <p>Talking about this</p>              </div>              <div class=\"leaf count\">                  <h1>";
  if (stack1 = helpers.wereHereCount) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.wereHereCount; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h1>                  <p>Were Here Count</p>              </div>              <div class=\"leaf checkins\">                  <h1>";
  if (stack1 = helpers.checkins) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.checkins; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h1>                  <p>Checkins </p>              </div>          </div>      </div>  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-twitter'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "      <div class=\"four-rows\">          <div class=\"source_logo\">              <img src=\"/tac/img/icons/twitter.png\">Twitter          </div>          <div class=\"stats\">              <div class=\"four_rows\">                  <div>                      <p>Followers</p>                      <span>";
  if (stack1 = helpers.followers) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.followers; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span>                  </div>              </div>              <div class=\"four_rows\">                  <div>                      <p>Following</p>                      <span>";
  if (stack1 = helpers.following) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.following; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span>                  </div>              </div>              <div class=\"four_rows\">                  <div>                      <p>Tweets</p>                      <span>";
  if (stack1 = helpers.tweets) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.tweets; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span>                  </div>              </div>              <div class=\"four_rows\">                  <div>                      <p>Favs</p>                      <span>";
  if (stack1 = helpers.favourites) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.favourites; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span>                  </div>              </div>          </div>      </div>  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-linked_in'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "      <div class=\"two-row\">          <div class=\"source_logo\">              <img src=\"/tac/img/icons/linked_in.png\">LinkedIn</div>          <div class=\"domino\">              <div class=\"piece employees\">                  <h1>";
  if (stack1 = helpers.employees) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.employees; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h1>                  <p>Employees</p>              </div>              <div class=\"piece followers\">                  <h1>";
  if (stack1 = helpers.followers) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.followers; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h1>                  <p>Followers</p>              </div>          </div>      </div>  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-google_plus'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "      <div class=\"two-row\">          <div class=\"source_logo\">              <img src=\"/tac/img/icons/google_plus.png\">Google Plus          </div>          <div class=\"domino\">              <div class=\"piece plus\">                  <h1>";
  if (stack1 = helpers.plusOneCount) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.plusOneCount; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h1>                  <p>Plus One Count</p>              </div>              <div class=\"piece followers\">                  <h1>";
  if (stack1 = helpers.circleByCount) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.circleByCount; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h1>                  <p>Circled By Count</p>              </div>          </div>      </div>  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-blog'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "      <div class=\"two-row\">          <div class=\"source_logo\">              <img src=\"/tac/img/icons/blog.png\">Blog</div>          <div class=\"domino\">              <div class=\"piece post\">                  <h1>";
  if (stack1 = helpers.newBlogPosts) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.newBlogPosts; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h1>                  <p>New Blog Posts</p>              </div>              <div class=\"piece comments\">                  <h1>";
  if (stack1 = helpers.totalBlogPosts) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.totalBlogPosts; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h1>                  <p>Total Blog Posts</p>              </div>          </div>      </div>  ";
  return buffer;
  });
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-company-report'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.url_display) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.url_display; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program3(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.url) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.url; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program5(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "     					<li>     						<a href=\"";
  if (stack1 = helpers.link) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.link; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">     							<img src=\"";
  if (stack1 = helpers.img) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.img; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">     						</a>     					</li>                      ";
  return buffer;
  }

  buffer += "      <div class=\"container\">  					<section class=\"single_company report\">  						<div class=\"report-header\">  							<h1>";
  if (stack1 = helpers.report_title) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.report_title; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h1>  							<ul class=\"options\">  								<li>  									<a href=\"/tac/main/download\">  										<span class=\"icon-download\"></span>Download report  									</a>  								</li>  								<li>  									<a href=\"/tac/main/print\">  										<span class=\"icon-print\"></span>Print report  									</a>  								</li>  								<li>  									<a href=\"/tac/main/share\">  										<span class=\"icon-email\"></span>Email report  									</a>  								</li>  							</ul>  						</div>    						<header>  							<div class=\"company_title\">   							<div class=\"img-content\">   								<div class=\"img\">   									<img src=\"";
  if (stack1 = helpers.img) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.img; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"\">   								</div>  						<div class=\"content\">  							<h3>";
  if (stack1 = helpers.companyName) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.companyName; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h3>    								<span>      				<a target=\"_blank\" href=\"";
  if (stack1 = helpers.url) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.url; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  stack1 = helpers['if'].call(depth0, depth0.url_display, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a>      				<p>";
  if (stack1 = helpers.location) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.location; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</p>     				</span>     				<ul class=\"socials\">                      ";
  stack1 = helpers.each.call(depth0, depth0.socials, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "     				</ul>  			</div>  			</div>  			</div>     				</header>       			<article class=\"base_data_report\">                      <span location=\"items\"></span>                  </article>              </section>          </div>      <span _template_=\"_template_\" data-selector-format=\"location\" data-selector-location=\"items\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-company-report'] = {"selectorFormat":"location","selectorLocation":"items"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-company-settings-general'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function";


  buffer += "      <div class=\"fluid-row\">          <div class=\"span6\">              <div class=\"stat\">              <section>                  <header>                      <h3>Sources to track</h3>                  </header>                  <article>                      <div class=\"tab-content-holder\">                          <span location=\"sources\"/>                      </div>                  </article>              </section>          </div>          </div>          <div class=\"span6\">              <div class=\"stat\">                              <section>                                  <header>                                      <h3>Reports</h3>                                  </header>                                  <article>                                      <div class=\"tab-content-holder generate-report\">                                          <span location=\"reports\"/>                                        </div>                                  </article>                              </section>                          </div>          </div>      </div>      <div class=\"fluid-row\">          <div class=\"span12\">              <div class=\"stat stop\">                  <section>                      <header>                          <h3>";
  if (stack1 = helpers.stopTracking) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.stopTracking; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>                      </header>                    </section>              </div>          </div>      </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-company-settings-general'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-crunchbase-overview'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<a href=\"https://twitter.com/";
  if (stack1 = helpers.twitter) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.twitter; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">@";
  if (stack1 = helpers.twitter) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.twitter; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</a>";
  return buffer;
  }

  buffer += "      <ul class=\"simple two-col\">          <li>              <h3>";
  if (stack1 = helpers.location) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.location; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Location</span>          </li>          <li>              <a href=\"#\">";
  if (stack1 = helpers.website) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.website; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a>              <span>Website</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.category) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.category; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Category</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.phone) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.phone; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Phone</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.founded) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.founded; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Founded</span>          </li>          <li>              <a href=\"#\">";
  if (stack1 = helpers.blog) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.blog; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a>              <span>Blog</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.employees) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.employees; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Employees</span>          </li>          <li>              ";
  stack1 = helpers['if'].call(depth0, depth0.twitter, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              <span>Twitter</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.totalAmountRaised) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.totalAmountRaised; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Amount Raised</span>          </li>          <li>              <h3 class=\"tags\">";
  if (stack1 = helpers.tags) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.tags; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Tags</span>          </li>      </ul>      <footer>          <h4>Description</h4>          <p>";
  if (stack1 = helpers.description) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.description; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</p>      </footer>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-crunchbase-overview'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-crunchbase-people'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", self=this, escapeExpression=this.escapeExpression;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              <li>                  <div class=\"img-content\">                      <div class=\"img\">                          ";
  stack1 = helpers['if'].call(depth0, depth0.image, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "                      </div>                      <div class=\"content\">                          <h3>";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h3>                          <span>";
  if (stack1 = helpers.title) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.title; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span>                      </div>                  </div>              </li>              ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<img src=\"//www.crunchbase.com/";
  if (stack1 = helpers.image) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.image; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">";
  return buffer;
  }

  buffer += "      <div class=\"tab-pane\" id=\"people\">          <ul class=\"simple three-col\">              ";
  stack1 = helpers.each.call(depth0, depth0.people, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          </ul>      </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-crunchbase-people'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-crunchbase-products'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              <li>                  <a href=\"//www.crunchbase.com/product/";
  if (stack1 = helpers.permalink) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.permalink; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" target=\"_blank\">";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a>              </li>              ";
  return buffer;
  }

  buffer += "      <div class=\"tab-pane\" id=\"products\">          <ul class=\"simple three-col\">              ";
  stack1 = helpers.each.call(depth0, depth0.products, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          </ul>      </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-crunchbase-products'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-crunchbase-media'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              <li>                  ";
  if (stack1 = helpers.embed) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.embed; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              </li>          ";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          <li>              ";
  stack1 = helpers['if'].call(depth0, depth0.image, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          </li>          ";
  return buffer;
  }
function program4(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<img src=\"//www.crunchbase.com/";
  if (stack1 = helpers.image) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.image; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">";
  return buffer;
  }

  buffer += "      <ul class=\"simple free\">          ";
  stack1 = helpers.each.call(depth0, depth0.videos, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          ";
  stack1 = helpers.each.call(depth0, depth0.images, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      </ul>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-crunchbase-media'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-crunchbase-milestones'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          <table class=\"table\">              <thead>              <tr>                  <th>Date</th>                  <th>Milestone</th>              </tr>              </thead>              <tbody>              ";
  stack1 = helpers.each.call(depth0, depth0.milestones, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              </tbody>          </table>          ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              <tr>                  <td>";
  if (stack1 = helpers.date) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.date; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</td>                  <td>";
  if (stack1 = helpers.text) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.text; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</td>              </tr>              ";
  return buffer;
  }

function program4(depth0,data) {
  
  
  return "          No milestones          ";
  }

  buffer += "      <div class=\"tab-content-holder\">          ";
  stack1 = helpers['if'].call(depth0, depth0.milestones, {hash:{},inverse:self.program(4, program4, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-crunchbase-milestones'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-crunchbase-funding'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "          <table class=\"table\">              <thead>              <tr>                  <th>Investor</th>                  <th>Round</th>                  <th>Amount</th>                  <th>Date</th>              </tr>              </thead>              <tbody>              ";
  stack1 = helpers.each.call(depth0, depth0.fundings, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "              </tbody>          </table>          ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              <tr>                  <td>";
  if (stack1 = helpers.investor) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.investor; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</td>                  <td>";
  if (stack1 = helpers.round) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.round; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</td>                  <td>";
  if (stack1 = helpers.amount) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.amount; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</td>                  <td>";
  if (stack1 = helpers.date) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.date; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</td>              </tr>              ";
  return buffer;
  }

function program4(depth0,data) {
  
  
  return "          No fundings          ";
  }

  buffer += "      <div class=\"tab-content-holder\">          ";
  stack1 = helpers['if'].call(depth0, depth0.fundings, {hash:{},inverse:self.program(4, program4, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-crunchbase-funding'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-crunchbase-competitors'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              <li>                  <a href=\"//www.crunchbase.com/company/";
  if (stack1 = helpers.permalink) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.permalink; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" target=\"_blank\">";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a>              </li>              ";
  return buffer;
  }

  buffer += "      <div class=\"tab-pane\" id=\"competitors\">          <ul class=\"simple three-col\">              ";
  stack1 = helpers.each.call(depth0, depth0.competitors, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          </ul>      </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-crunchbase-competitors'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-crunchbase-acquisitions'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              <table class=\"table\">                  <thead>                  <tr>                      <th>Company</th>                      <th>Source</th>                      <th>Amount</th>                      <th>Date</th>                  </tr>                  </thead>                  <tbody>                  ";
  stack1 = helpers.each.call(depth0, depth0.acquisitions, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "                  </tbody>              </table>              ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "                  <tr>                      <td>";
  if (stack1 = helpers.company) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.company; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</td>                      <td>";
  if (stack1 = helpers.source) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.source; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</td>                      <td>";
  if (stack1 = helpers.amount) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.amount; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</td>                      <td>";
  if (stack1 = helpers.date) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.date; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</td>                  </tr>                  ";
  return buffer;
  }

function program4(depth0,data) {
  
  
  return "              No acquisitions              ";
  }

  buffer += "            <div class=\"tab-content-holder\">              ";
  stack1 = helpers['if'].call(depth0, depth0.acquisitions, {hash:{},inverse:self.program(4, program4, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "          </div>        <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-crunchbase-acquisitions'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-crunchbase-investments'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              <table class=\"table\">                  <thead>                  <tr>                      <th>Company</th>                      <th>Round</th>                      <th>Amount</th>                      <th>Date</th>                  </tr>                  </thead>                  <tbody>                  ";
  stack1 = helpers.each.call(depth0, depth0.investments, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "                  </tbody>              </table>              ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "                  <tr>                      <td>";
  if (stack1 = helpers.company) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.company; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</td>                      <td>";
  if (stack1 = helpers.round) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.round; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</td>                      <td>";
  if (stack1 = helpers.amount) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.amount; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</td>                      <td>";
  if (stack1 = helpers.date) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.date; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</td>                  </tr>                  ";
  return buffer;
  }

function program4(depth0,data) {
  
  
  return "              No investments              ";
  }

  buffer += "            <div class=\"tab-content-holder\">              ";
  stack1 = helpers['if'].call(depth0, depth0.investments, {hash:{},inverse:self.program(4, program4, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "            </div>        <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-crunchbase-investments'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-settings-sources'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2;
  buffer += "          <li>              <label>                  <input name=\""
    + escapeExpression(((stack1 = depth1.name),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" ";
  stack2 = helpers['if'].call(depth0, depth1.disabled, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " type=\"checkbox\" value=\"";
  if (stack2 = helpers.value) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.value; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" ";
  stack2 = helpers['if'].call(depth0, depth0.selected, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  if (stack2 = helpers.attributes) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.attributes; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  stack2 = helpers['if'].call(depth0, depth0.tabindex, {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " />                  <img src=\"";
  if (stack2 = helpers.icon) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.icon; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">                  ";
  if (stack2 = helpers.label) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.label; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "              </label>          </li>          ";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "disabled";
  }

function program4(depth0,data) {
  
  
  return "checked=\"checked\"";
  }

function program6(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " tabindex=\"";
  if (stack1 = helpers.tabindex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.tabindex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" ";
  return buffer;
  }

  buffer += "      <ul class=\"selection\">          ";
  stack1 = helpers.each.call(depth0, depth0.list, {hash:{},inverse:self.noop,fn:self.programWithDepth(1, program1, data, depth0),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      </ul>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-settings-sources'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-settings-general'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function";


  buffer += "      <div class=\"fluid-row\">          <div class=\"span6\">              <div class=\"stat\">                  <section>                      <header>                          <h3>Sources being tracked</h3>                        </header>                      <article>                          <div class=\"tab-content-holder\">                              <span location=\"settingsSources\"/>                          </div>                      </article>                  </section>              </div>          </div>          <div class=\"span6\">              <div class=\"stat\">                  <section>                      <header>                          <h3>Reports</h3>                        </header>                      <article>                          <div class=\"tab-content-holder generate-report\">                              <span location=\"report\"/>                          </div>                      </article>                  </section>              </div>          </div>      </div>      <div class=\"fluid-row\">          <div class=\"span12\">              <div class=\"stat stop\">                  <section>                      <header>                          <h3>";
  if (stack1 = helpers.trackCompany) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.trackCompany; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>                      </header>                    </section>              </div>          </div>      </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-settings-general'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-settings-competitors'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  
  return "s";
  }

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "              <li>                  <a href=\"#\">                      <h5>";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h5>                      <div class=\"logo-holder\">                          <img src=\"";
  if (stack1 = helpers.logo) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.logo; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">                      </div>                      ";
  if (stack1 = helpers.remove) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.remove; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "                  </a>              </li>              ";
  return buffer;
  }

  buffer += "      <div class=\"fluid-row\">          <ul class=\"companies-list\">              <h3>";
  if (stack1 = helpers.totalCompetitors) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.totalCompetitors; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " Competitor";
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data};
  stack2 = ((stack1 = helpers.ifNotEq || depth0.ifNotEq),stack1 ? stack1.call(depth0, depth0.totalCompetitors, 1, options) : helperMissing.call(depth0, "ifNotEq", depth0.totalCompetitors, 1, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  if (stack2 = helpers.manageCompetitors) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.manageCompetitors; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</h3>              ";
  stack2 = helpers.each.call(depth0, depth0.competitors, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "          </ul>          ";
  if (stack2 = helpers.addCompetitor) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.addCompetitor; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "      </div>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-settings-competitors'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-facebook-profile'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function";


  buffer += "      <ul class=\"simple two-col\">          <li>              <h3>";
  if (stack1 = helpers.address) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.address; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Address</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.phone) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.phone; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Phone</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.founded) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.founded; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Founded</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.checkins) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.checkins; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Checkings</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.likes) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.likes; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Likes</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.talkingAboutCount) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.talkingAboutCount; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Talking About Count</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.wereHereCount) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.wereHereCount; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Were Here Count</span>          </li>          <li>              <img src=\"";
  if (stack1 = helpers.cover) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.cover; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">              <span>Cover</span>          </li>      </ul>      <footer>          <div>              <h4>Products</h4>              <p>";
  if (stack1 = helpers.products) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.products; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</p>          </div>          <div>              <h4>Mission</h4>              <p>";
  if (stack1 = helpers.mission) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.mission; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</p>          </div>      </footer>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-facebook-profile'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-twitter-profile'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function";


  buffer += "      <ul class=\"simple two-col\">          <li>              <h3>";
  if (stack1 = helpers.location) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.location; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Location</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.followers) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.followers; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Followers</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.friends) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.friends; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Friends</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.listed) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.listed; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Listed</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.favorites) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.favorites; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Favorites</span>          </li>      </ul>      <footer>          <div>              <h4>Description</h4>              <p>";
  if (stack1 = helpers.description) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.description; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</p>          </div>      </footer>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-twitter-profile'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-stats'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "      <ul class=\"simple two-col\">          <span location=\"stats\"/>      </ul>      <span _template_=\"_template_\" data-selector-format=\"location\" data-selector-location=\"stats\"/>  ";
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-stats'] = {"selectorFormat":"location","selectorLocation":"stats"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-linked_in-profile'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function";


  buffer += "      <ul class=\"simple two-col\">          <li>              <h3>";
  if (stack1 = helpers.location) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.location; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Location</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.phone) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.phone; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Phone</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.founded) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.founded; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Founded</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.employees) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.employees; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Employees</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.followers) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.followers; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Followers</span>          </li>      </ul>      <footer>          <div>              <h4>Specialties</h4>              <p>";
  if (stack1 = helpers.specialties) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.specialties; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</p>          </div>          <div>              <h4>Description</h4>              <p>";
  if (stack1 = helpers.description) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.description; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</p>          </div>      </footer>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-linked_in-profile'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-feed'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, self=this;

function program1(depth0,data) {
  
  
  return "          <span location=\"feed\"/>          ";
  }

function program3(depth0,data) {
  
  
  return "          No feeds          ";
  }

  buffer += "      <div class=\"tab-content-holder\">          ";
  stack1 = helpers['if'].call(depth0, depth0.feeds, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      </div>      <span _template_=\"_template_\" data-selector-format=\"location\" data-selector-location=\"feed\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-feed'] = {"selectorFormat":"location","selectorLocation":"feed"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-updates'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, self=this;

function program1(depth0,data) {
  
  
  return "          <span location=\"update\"/>          ";
  }

function program3(depth0,data) {
  
  
  return "          No Updates          ";
  }

  buffer += "      <div class=\"tab-content-holder\">          ";
  stack1 = helpers['if'].call(depth0, depth0.update, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "      </div>      <span _template_=\"_template_\" data-selector-format=\"location\" data-selector-location=\"update\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-updates'] = {"selectorFormat":"location","selectorLocation":"update"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-google_plus-profile'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function";


  buffer += "      <ul class=\"simple two-col\">          <li>              <h3>";
  if (stack1 = helpers.plusOne) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.plusOne; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Plus One Count</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.circle) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.circle; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Circled By Count</span>          </li>          <li>              <img src=\"";
  if (stack1 = helpers.cover) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.cover; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">              <span>Cover</span>          </li>      </ul>      <footer>          <div>              <h4>Description</h4>              <p>";
  if (stack1 = helpers.description) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.description; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</p>          </div>      </footer>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-google_plus-profile'] = {"selectorFormat":"name"};
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tac-block-blog-profile'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function";


  buffer += "      <ul class=\"simple two-col\">          <li>              <h3>";
  if (stack1 = helpers.newBlogPosts) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.newBlogPosts; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>New Blog Posts</span>          </li>          <li>              <h3>";
  if (stack1 = helpers.comments) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.comments; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h3>              <span>Comments</span>          </li>      </ul>      <span _template_=\"_template_\" data-selector-format=\"name\"/>  ";
  return buffer;
  });
})();
(function() {
	if (!Handlebars.templateData) Handlebars.templateData = {};
Handlebars.templateData['tac-block-blog-profile'] = {"selectorFormat":"name"};
})();
